dotnet add package --prerelease Avalonia
dotnet add package --prerelease Avalonia.Controls.DataGrid
dotnet add package --prerelease Avalonia.Desktop 
dotnet add package --prerelease Avalonia.Diagnostics 
dotnet add package --prerelease Avalonia.ReactiveUI 
dotnet add package --prerelease Avalonia.Svg.Skia
dotnet add package --prerelease Avalonia.Themes.Fluent
dotnet add package --prerelease LiveChartsCore.SkiaSharpView.Avalonia
dotnet add package --prerelease system.io.ports
dotnet add package --prerelease Avalonia.Themes.Fluent
dotnet add package --prerelease Markdown.Avalonia
