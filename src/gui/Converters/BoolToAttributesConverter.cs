using System;
using System.Globalization;
using Avalonia;
using Avalonia.Media;
using Avalonia.Data.Converters;
using Avalonia.Controls.Shapes;

namespace dToFGUI.Converters
{
    public class BoolToAttributesConverter : IValueConverter
    {
        public static BoolToAttributesConverter Instance = new BoolToAttributesConverter();
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (targetType == typeof(FontWeight))
            {
                if (value is bool)
                {
                    if ((bool)value)
                        return FontWeight.Normal;
                    else 
                        return FontWeight.Bold;
                }
                return FontWeight.Normal;
            }
            else if (targetType == typeof(Thickness))
            {
                if (value is bool)
                {
                    if ((bool)value)
                        return new Thickness(5,2,5,4);
                    else 
                        return new Thickness(12,9,12,12);
                }
                return new Thickness(12,9,12,12);
            }
            else if (targetType == typeof(IBrush))
            {
                if (value is bool)
                {
                    if ((bool)value)
                        return Brushes.Green;
                    else 
                        return Brushes.DimGray;
                }
                return Brushes.DimGray;
            }
            return false;
        }

        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}