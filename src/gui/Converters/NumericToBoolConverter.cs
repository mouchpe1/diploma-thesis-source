using System;
using System.Globalization;
using Avalonia.Data.Converters;

namespace dToFGUI.Converters
{
    public class NumericToBoolConverter : IValueConverter
    {
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (targetType == typeof(bool))
            {
                if (value is double)
                {
                    if ((double)value > 0)
                        return true;
                    else 
                        return false;
                }
            }
            return false;
        }

        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}