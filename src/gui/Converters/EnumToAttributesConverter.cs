using System;
using System.Globalization;
using Avalonia;
using Avalonia.Media;
using Avalonia.Data.Converters;
using Avalonia.Controls.Shapes;
using dToFGUI.Models;

namespace dToFGUI.Converters
{
    public class EnumToAttributesConverter : IValueConverter
    {
        public static EnumToAttributesConverter Instance = new EnumToAttributesConverter();
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (targetType == typeof(IBrush))
            {
                if (value is SensorStatus)
                {
                    switch ((SensorStatus)value)
                    {
                        case SensorStatus.Inactive:
                            return Brushes.DimGray;
                        case SensorStatus.Active:
                            return Brushes.Green;
                        case SensorStatus.Paused:
                            return Brushes.Yellow;
                    }
                }
                return Brushes.Black;
            }
            return false;
        }

        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}