### Basic workflow

1. Connect the board via USB-C cable to the PC
2. Select the correct serial port (press *Refresh* to update list of devices)
3. For a correct detection, it might be necessary to press *Set Reference* when there are no objects in the area and background is static
4. Evaluation status is visible in the top right, for algorithm details go to *Measurement *