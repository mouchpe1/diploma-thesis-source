using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using System.Reactive.Linq;
using ReactiveUI;

namespace dToFGUI.Models
{
    public class Device : ReactiveObject
    {
        public static MessageInterface? Interface
        {
            get; set;
        }

        private ObservableCollection<Sensor> _sensors;
        public ObservableCollection<Sensor> Sensors 
        {
            get => _sensors; 
            set => this.RaiseAndSetIfChanged(ref _sensors, value); 
        }

        private Measurement _measurement;
        public Measurement Measurement
        {
            get => _measurement;
        }

        private DeviceSettings _settings;
        public DeviceSettings Settings
        {
            get => _settings;
            set => this.RaiseAndSetIfChanged(ref _settings, value);
        }


        public Device ()
        {
            int sensor_cnt = 4;
            _measurement = new Measurement();
            _settings = new DeviceSettings();
            _sensors = new ObservableCollection<Sensor>();
            for (int i = 0; i < sensor_cnt; i++)
            {
                _sensors.Add(new Sensor(i));
                _sensors[i].WhenAnyValue(x => x.Status).Subscribe((a) => _measurement.AssignSensors(Sensors));
            }
        }

        public void SetEvalMode (byte val)
        {
            if (MessageInterface.Instance.IsConnected)
                MessageInterface.Instance.Get(CommandType.EvalMode, val);
        }

        public void SetEvalResult (byte val)
        {
            if (MessageInterface.Instance.IsConnected)
                MessageInterface.Instance.Get(CommandType.EvalResult, val);
        }

        public void GetSettings ()
        {
            Span<byte> data = null;
            if (MessageInterface.Instance.IsConnected)
                data = MessageInterface.Instance.Get(CommandType.Settings, 0);
            else
                return;
            
            Settings.DistanceTreshold = BitConverter.ToUInt16(data.ToArray(), 0);  
        }

        public void ApplySettings ()
        {
            List<byte> data = new List<byte>();
            data.Add(1);
            data.AddRange(BitConverter.GetBytes(Settings.DistanceTreshold));
            if (MessageInterface.Instance.IsConnected)
                MessageInterface.Instance.Get(CommandType.Settings, data.ToArray());
        }

        public void EqualizeHistograms ()
        {
            int ymax = 0;
            foreach (Sensor s in Sensors)
            {
                int ycur = s.GetHistogramYMax();
                if (ycur > ymax)
                    ymax = ycur;
            }

            foreach (Sensor s in Sensors)
                if (s.Status == SensorStatus.Active)
                    s.SetHistogramYMax(ymax);
        }
    }
}