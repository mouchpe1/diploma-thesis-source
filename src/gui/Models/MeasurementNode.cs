using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using ReactiveUI;
using SkiaSharp;

namespace dToFGUI.Models
{
    public class MeasurementNode : ReactiveObject
    {
        public Sensor Source { get; private set; }

        private ObservableCollection<int> _ref;
        private ObservableCollection<float> _refRatio;
        private ObservableCollection<float> _diffRatio;
        private ObservableCollection<float> _diffSignal;
        private ObservableCollection<float> _actRatio;
        private List<float> _binWeights;

        private float _meanRatio = 0;
        public float MeanRatio
        {
            get => _meanRatio;
            set => this.RaiseAndSetIfChanged(ref _meanRatio, value);
        }

        private Tuple<float,float> _ratioSignature = new Tuple<float, float>(0,0);
        public Tuple<float,float> RatioSignature
        {
            get => _ratioSignature;
            set => this.RaiseAndSetIfChanged(ref _ratioSignature, value);
        }

        private Tuple<float,float> _signalSignature = new Tuple<float, float>(0,0);
        public Tuple<float,float> SignalSignature
        {
            get => _signalSignature;
            set => this.RaiseAndSetIfChanged(ref _signalSignature, value);
        }

        private float _meanAreaSignal = 0;
        public float MeanAreaSignal
        {
            get => _meanAreaSignal;
            set => this.RaiseAndSetIfChanged(ref _meanAreaSignal, value);
        }
        private float _meanSignal = 0;
        public float MeanSignal
        {
            get => _meanSignal;
            set => this.RaiseAndSetIfChanged(ref _meanSignal, value);
        }
        private float _sumSignal = 0;
        public float SumSignal
        {
            get => _sumSignal;
            set => this.RaiseAndSetIfChanged(ref _sumSignal, value);
        }
        private int _areaBins = 4;

        public int AreaBins
        {
            get => _areaBins;
            set => this.RaiseAndSetIfChanged(ref _areaBins, value);
        }

        private float _sumAreaSignal = 0;
        public float SumAreaSignal
        {
            get => _sumAreaSignal;
            set => this.RaiseAndSetIfChanged(ref _sumAreaSignal, value);
        }

        private ISeries[] _data;
        public ISeries[] Data
        {
            get => _data;
            set => this.RaiseAndSetIfChanged(ref _data, value);
        }

        public List<ISeries[]> _singleData;
        public List<ISeries[]> SingleData 
        {
            get => _singleData;
            private set => this.RaiseAndSetIfChanged(ref _singleData, value);
        }

        private List<Axis[]> _dataYAxes;
        public List<Axis[]> DataYAxes 
        {
            get => _dataYAxes;
            set => this.RaiseAndSetIfChanged(ref _dataYAxes, value);
        }

        public MeasurementNode (Sensor source)
        {
            Source = source;
            _ref = new ObservableCollection<int>();
            _diffRatio = new ObservableCollection<float>();
            _diffSignal = new ObservableCollection<float>();
            _refRatio = new ObservableCollection<float>();
            _actRatio = new ObservableCollection<float>();
            _binWeights = new List<float>();
            
            _data = new ISeries[]
                {
                    new ColumnSeries<float>
                    {
                        Values = _diffSignal,
                        Name = "Signal",
                        Fill = new SolidColorPaint(SKColors.Yellow){ StrokeThickness = 15 },
                        Stroke = null
                    },
                    new ColumnSeries<float>
                    {
                        Values = _diffRatio,
                        Name = "Ratio",
                        Fill = new SolidColorPaint(SKColors.LawnGreen){ StrokeThickness = 15 },
                        Stroke = null
                    }
                };
            
            _singleData = new List<ISeries[]>();
            foreach (ISeries series in _data)
                _singleData.Add(new ISeries[] {series});

            _dataYAxes = new List<Axis[]>();
            foreach (ISeries series in _data)
                _dataYAxes.Add(new Axis[] {new Axis() {LabelsRotation = 90}});
        }

        private void AdjustYAxes()
        {
            DataYAxes[0][0].MaxLimit = 2000;
            DataYAxes[0][0].MinLimit = -2000;
            DataYAxes[1][0].MaxLimit = 5;  
        }
        private void UpdateBinWeights ()
        {
            /*****Use proportion of signal in reference distribution*****

            Common.InitCollection(_binWeights, _ref.Count);
            int total = _ref.Sum();
            int area = _ref.Take(AreaBins).Sum();
            for(int i = 0; i < AreaBins; i++)
                _binWeights[i] = 1 + (1 - area/total);
            for(int i = AreaBins; i < _binWeights.Count; i++)
                _binWeights[i] = 1 - (1 - area/total);
            */

            Common.InitCollection(_binWeights, _diffRatio.Count);
            float total = _diffRatio.Sum();
            for(int i = 0; i < _binWeights.Count; i++)
                _binWeights[i] = (float)1 - (_diffRatio[i]/total);
        }
        public void SetNewReference ()
        {
            Common.InitCollection(_ref, Source.HistogramData.Count);
            Common.InitCollection(_refRatio, Source.HistogramData.Count);
            Common.InitCollection(_actRatio, Source.HistogramData.Count);
            Common.InitCollection(_diffRatio, Source.HistogramData.Count);
            Common.InitCollection(_diffSignal, Source.HistogramData.Count);

            for (int i = 0; i < Source.HistogramData.Count; i++)
            {
                _ref[i] = Source.HistogramData[i];
            }
            //calculate reference bin ratio
            for(int i = 0; i < _ref.Count - 1; i++)
                _refRatio[i] = (float)_ref[i] / (float)_ref[i + 1];

            UpdateBinWeights();
        }

        public Tuple<float,float> GetPeakSignature(ObservableCollection<float> data)
        {
            //find peaks
            float min = data.Min();
            min = min < 0 ? Math.Abs(min) : 0;
            List<float> areaPeaks = Common.FindMaxPeaks(data, 0, AreaBins);
            List<float> bgPeaks = Common.FindMaxPeaks(data, AreaBins);
            float bgNonPeakAvg = 0;
            float areaNonPeakAvg = 0;
            for (int i = 0; i < _ref.Count; i++)
            {
                if (i < AreaBins && !areaPeaks.Contains(data[i]))
                    areaNonPeakAvg += min + data[i];
                else if (!bgPeaks.Contains(data[i]))
                    bgNonPeakAvg += min +data[i];
            }
            areaNonPeakAvg /= AreaBins - areaPeaks.Count;
            bgNonPeakAvg /= _ref.Count - AreaBins - bgPeaks.Count;

            float areaSig = 0, bgSig = 0;
            if (areaPeaks.Count > 0)
                areaSig = areaPeaks.Select((peak) => (peak + min) - areaNonPeakAvg).Max();
            if (bgPeaks.Count > 0)
                bgSig = bgPeaks.Select((peak) => (peak + min) - bgNonPeakAvg).Max();
            
            return new Tuple<float, float>(areaSig, bgSig);
        }

        public void Update ()
        {
            if (_ref.Count < _areaBins + 1 || Source.HistogramData.Count < _ref.Count)
                return;

            //calculate current bin ratio
            for(int i = 0; i < _ref.Count - 1; i++)
                _actRatio[i] = (float)Source.HistogramData[i] / (float)Source.HistogramData[i + 1];

            //calculate ratio and signal difference
            for(int i = 0; i < _ref.Count; i++)
            {
                _diffRatio[i] = Math.Abs(_actRatio[i] / _refRatio[i]);
                _diffSignal[i] = Source.HistogramData[i] - _ref[i];
            }



            //sum is taken with original negative values so there are no scaling issues
            SumSignal = _diffSignal.Sum();
            SumAreaSignal = _diffSignal.Take(AreaBins).Sum();

            SignalSignature = GetPeakSignature(_diffSignal);
            RatioSignature = new Tuple<float, float>(_diffRatio.Take(AreaBins).Max(), _diffRatio.TakeLast(_diffRatio.Count - AreaBins).Max());

            //offset to eliminate negative values
            //for (int i = 0; i < _diffSignal.Count; i++)
            //    _diffSignal[i] += Math.Abs(minDiffSignal);
            
            UpdateBinWeights();
            MeanRatio = Common.GetMean(_diffRatio);
            MeanSignal = Common.GetMean(_diffSignal);
            MeanAreaSignal = Common.GetMean(_diffSignal, AreaBins);

            AdjustYAxes();
        }

        public float GetRatio (int bin)
        {
            if (bin > 0 && bin < _diffRatio.Count)
                return _diffRatio[bin];
            else
                return float.NaN;
        }

        public float GetSignal (int bin)
        {
            if (bin > 0 && bin < _diffSignal.Count)
                return _diffSignal[bin];
            else
                return float.NaN;
        }
    }
}