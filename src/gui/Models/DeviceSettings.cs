using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using System.Reactive.Linq;
using ReactiveUI;

namespace dToFGUI.Models
{
    public class DeviceSettings : ReactiveObject
    {
        private ushort _distanceTreshold = 0;
        public ushort DistanceTreshold
        {
            get => _distanceTreshold;
            set => this.RaiseAndSetIfChanged(ref _distanceTreshold, value);
        }
    }
}