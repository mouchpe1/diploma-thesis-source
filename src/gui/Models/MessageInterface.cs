using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using ReactiveUI;

namespace dToFGUI.Models
{
    public enum CommandType : byte {
        Status = 0x41,
        EvalMode = 0x44,
        EvalResult = 0x45,
        Histogram = 0x48,
        Settings = 0x53
    }
    public class MessageInterface : ReactiveObject
    {

        private static readonly MessageInterface _instance = new MessageInterface();

        static MessageInterface()
        {
        }

        public static MessageInterface Instance
        {
            get
            {
                return _instance;
            }
        }

        private const byte FrameStartHead = 0x2A;
        private const byte FrameStartData = 0x2B;
        private const byte FrameEnd = 0x2D;
        private const byte FrameAckOK = 0x41;
        private const byte FrameAckErr = 0x45;
        private SerialPort _serialPort;
        private byte[] buffer;
        private int dataSize;
        private bool _isConnected;
        public bool IsConnected
        {
            get => _isConnected;
            set => this.RaiseAndSetIfChanged(ref _isConnected, value);
        }

        private string? _port;
        public string? Port
        {
            get => _port;
            set
            {
                this.RaiseAndSetIfChanged(ref _port, value);
            }
        }

        public MessageInterface()
        {
            //Init buffer
            buffer = new byte[byte.MaxValue + 4];
            //Set serial port params
            _serialPort = new SerialPort();
        }

        public void Connect ()
        {
            if (IsConnected == true || Port == null)
                return;
            
            if (_serialPort != null)
                _serialPort.Dispose();
            
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 38400;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Handshake = Handshake.None;
            _serialPort.PortName = Port;
            // Set the read/write timeouts
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            _serialPort.Open();
            IsConnected = true;
        }

        public void Disconnect()
        {
            if (_serialPort != null)
                _serialPort.Close();
            IsConnected = false;
        }

        private void ReadBytes(int offset, int cnt)
        {
            int bytesRead = 0;
            while (bytesRead < cnt)
            {
                int bytes=_serialPort.Read(buffer, offset, cnt - bytesRead);
                bytesRead += bytes;
                offset += bytes;
            }
        }

        private void SendBytes(params byte[] data)
        {
            _serialPort.Write(data, 0, data.Length);
        }

        private bool SendCommand(CommandType cmd, params byte[] data)
        {
            int data_off;
            int i = 0;
            byte size = data.Length < 256 ? Convert.ToByte(data.Length) : (byte)255;
            buffer[i++] = FrameStartHead;
            buffer[i++] = (byte)cmd;
            buffer[i++] = size;
            buffer[i++] = FrameEnd;
            data_off = i;
            buffer[i++] = FrameStartData;
            int data_idx = 0;
            while (data_idx < size)
                buffer[i++] = data[data_idx++];
            buffer[i++] = FrameEnd;

            _serialPort.DiscardInBuffer(); //clears previous responses or boot messages
            _serialPort.Write(buffer, 0, data_off);
            byte tmp = (byte)_serialPort.ReadByte();
            if (tmp != FrameAckOK)
                return false;
            _serialPort.Write(buffer, data_off, size + 2);
            tmp = (byte)_serialPort.ReadByte();
            if (tmp != FrameAckOK)
                return false;
            return true;
        }

        private bool GetResponse()
        {
            bool ret = true;
            int sent_op = buffer[1];
            //header check
            ReadBytes(0, 4);
            if (buffer[0] == FrameStartHead
                && buffer[1] == sent_op
                && buffer[3] == FrameEnd)
            {
                dataSize = buffer[2];
                //SendBytes(FrameAckOK);
            }
            else
            {
                //SendBytes(FrameAckErr);
                return false;
            }
            if (dataSize == 0)
                return true;
            //data read
            byte data_start = (byte)_serialPort.ReadByte();
            ReadBytes(0, dataSize + 1);
            if (data_start == FrameStartData && buffer[dataSize] == FrameEnd)
            {
                //SendBytes(FrameAckOK);
            }
            else
            {
                //SendBytes(FrameAckErr);
                return false;
            }
            return ret;
        }
        public Span<byte> Get (CommandType cmd, params byte[] data)
        {
            try 
            {
                if (!SendCommand(cmd, data))
                    return null;   
                if (!GetResponse())
                    return null;

                return buffer.AsSpan<byte>(0, dataSize);
            }
            catch
            {
                Disconnect();
            }
            return null;
        }
    }
}