using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using ReactiveUI;
using SkiaSharp;

namespace dToFGUI.Models
{
    public enum ObstructionType
    {
        None = 0,
        Object = 1,
        FakeObject = 2
    }
    public class Measurement : ReactiveObject
    {

        private ObservableCollection<MeasurementNode> _nodes;
        public ObservableCollection<MeasurementNode> Nodes
        {
            get => _nodes;
            set => this.RaiseAndSetIfChanged(ref _nodes, value);
        }

        public bool DetectsNone {get => _result == ObstructionType.None; }
        public bool DetectsObject {get => _result == ObstructionType.Object; }
        public bool DetectsFakeObject {get => _result == ObstructionType.FakeObject; }
        private ObstructionType _result;
        public ObstructionType Result
        {
            get => _result;
            set
            {
                this.RaiseAndSetIfChanged(ref _result, value);
                this.RaisePropertyChanged(nameof(DetectsNone));
                this.RaisePropertyChanged(nameof(DetectsObject));
                this.RaisePropertyChanged(nameof(DetectsFakeObject));
            } 
        }


        public Measurement ()
        {
            _nodes = new ObservableCollection<MeasurementNode>();
        }
        public void AssignSensors (Collection<Sensor> sensors)
        {
            if (sensors.Count < 4)
                return;
            Nodes.Clear();
            foreach(Sensor s in sensors)
            {
                if (s.Status == SensorStatus.Active)
                    Nodes.Add(new MeasurementNode(s));
            }
        }

        private int updateCtr = 0;
        private const int updateTres = 3;
        public void Update ()
        {
            if (Nodes.Count < 2)
                return;

            //update sensor node metrics
            foreach (MeasurementNode node in Nodes)
                node.Update();
            
            //evaluate object
            ObstructionType obj = ObstructionType.None;
            const float trSignal = 400;
            const float trRatio = (float)2;
            const float trNewRef = 8;
            //const float trEdgeBin = 4;

            foreach (MeasurementNode node in Nodes)
            {
                if (obj == ObstructionType.Object)
                    break;
                
                if (node.RatioSignature.Item1 > trRatio)
                    obj = ObstructionType.Object;
                else if (node.RatioSignature.Item1 <= trRatio && node.SignalSignature.Item1 > trSignal)
                {
                    if (node.SumAreaSignal/(node.SumSignal < node.SumAreaSignal ? node.SumAreaSignal : node.SumSignal)> 0.5) //&& node.RatioSignature.Item2 < trNewRef)
                        obj = ObstructionType.Object;
                    else
                        obj = ObstructionType.FakeObject;
                }

                /* Auto-update reference
                if (node.RatioSignature.Item2 > trNewRef)
                    node.SetNewReference();
                */
            }

            if (Result == obj)
                updateCtr = 0;
            else
                updateCtr++;

            if (obj == ObstructionType.None || updateCtr >= updateTres)
            {
                Result = obj;
                updateCtr = 0;
            }
                
        }
    }
}