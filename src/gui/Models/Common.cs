using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using System.Reactive.Linq;
using System.Linq;
using ReactiveUI;

static class Common
{
    public static void InitCollection<T> (ICollection<T> a, int count) where T : new()
    {
        if (a.Count == count)
            return;
        else if (a.Count > count)
            a.Clear();
        for (int i = a.Count; i < count; i++)
            a.Add(new T());
    }

    public static float GetMean (ICollection<float> data, int count = -1)
    {
        if (count == -1)
            count = data.Count;
        float sum = data.Select(x => Math.Abs(x)).Sum();
        float mean = 0;
        for (int i = 0; i < count; i++)
            mean += Math.Abs(data.ElementAt(i))/sum * i;
        return mean;
    }

    public static float GetWeightedMean (Collection<float> data, ICollection<float> weights, int count = -1)
    {
        if (count == -1 || count > Math.Min(data.Count, weights.Count))
            count = Math.Min(data.Count, weights.Count);
        float sum = data.Select((x, i) => Math.Abs(x) * weights.ElementAt(i)).Sum();
        float mean = 0;
        for (int i = 0; i < count; i++)
            mean += ((Math.Abs(data.ElementAt(i)) * weights.ElementAt(i))/sum) * i;
        return mean;
    }

    public static List<float> FindMaxPeaks (Collection<float> data, int start = 0, int len = -1)
    {
        List<float> peaks = new List<float>();
        len = len < 0 ? data.Count - start : len;
        for (int i = start; i < len; i++)
        {
            float left = i == 0 ? float.MinValue : data.ElementAt(i - 1);
            float right = i == len - 1 ? float.MinValue : data.ElementAt(i + 1);
            if ((left < data.ElementAt(i) && data.ElementAt(i) > right))
                //|| (data.ElementAt(i) < 0 && left > data.ElementAt(i) && data.ElementAt(i) < right))
                peaks.Add(data.ElementAt(i));
        }
        return peaks;
    }
}