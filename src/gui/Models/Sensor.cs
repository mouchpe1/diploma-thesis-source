using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using dToFGUI.ViewModels;
using dToFGUI.Views;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using ReactiveUI;
using SkiaSharp;

namespace dToFGUI.Models
{
    public enum SensorStatus
    {
        Inactive,
        Active,
        Paused
    }
    public class Sensor : ReactiveObject
    {
        private byte _id;
        public string ID {get; set;}

        private SensorStatus _status;
        public SensorStatus Status
        {
            get => _status;
            set => this.RaiseAndSetIfChanged(ref _status, value);
        }

        private uint _distance;
        public string Distance 
        { 
            get => _distance.ToString() + " mm";
        } 

        public uint DistanceInt
        { 
            get => _distance;
        } 

        private uint _ambientRate;
        public string AmbientRate
        { 
            get => _ambientRate.ToString() + " kcp/s";
        } 

        private uint _signalRate;
        public string SignalRate
        { 
            get => _signalRate.ToString() + " kcp/s";
        } 

        private uint _numberOfSpad;
        public string NumberOfSpad
        { 
            get => _numberOfSpad.ToString();
        } 

        private uint _sigma;
        public string Sigma
        { 
            get => _sigma.ToString() + " mm";
        } 

        private ObservableCollection<int> _histogramData;
        public ObservableCollection<int> HistogramData
        {
            get => _histogramData;
            set => this.RaiseAndSetIfChanged(ref _histogramData, value);
        }
        
        private ISeries[] _histogram;
        public ISeries[] Histogram
        {
            get => _histogram;
            private set => this.RaiseAndSetIfChanged(ref _histogram, value);
        }

        public Sensor (int id)
        {
            _id = (byte)id;
            ID = id.ToString();
            Status = SensorStatus.Inactive;
            _histogramData = new ObservableCollection<int>();
            _histogram = new ISeries[]
                {
                    new ColumnSeries<int>
                    {
                        Values = HistogramData,
                        Name = "dToF_" + ID,
                        Fill = new SolidColorPaint(SKColors.Blue){ StrokeThickness = 15 },
                        Stroke = null
                    },
                    new ColumnSeries<int>
                    {
                        IsHoverable = false, // disables the series from the tooltips 
                        Values = new ObservableCollection<int>() { 0 },
                        Stroke = null,
                        Fill = null,
                        IgnoresBarPosition = true
                    }
                };
        }

        public void SetStatus (byte[] data, int len)
        {
            int offset = 0;
            if (len < 10)
                return;
            _distance = BitConverter.ToUInt32(data, offset + 0);  
            _ambientRate = BitConverter.ToUInt32(data, offset + 4);  
            _signalRate = BitConverter.ToUInt32(data, offset + 8);
            _numberOfSpad = BitConverter.ToUInt32(data, offset + 12);
            _sigma = BitConverter.ToUInt32(data, offset + 16);
            this.RaisePropertyChanged(nameof(Distance));
            this.RaisePropertyChanged(nameof(AmbientRate));
            this.RaisePropertyChanged(nameof(SignalRate));
            this.RaisePropertyChanged(nameof(NumberOfSpad));
            this.RaisePropertyChanged(nameof(Sigma));
        }

        public int GetHistogramYMax ()
        {
            return ((Histogram[1].Values as ObservableCollection<int>) ?? new ObservableCollection<int>() {0})[0];
        }

        public void SetHistogramYMax (int max)
        {
            ObservableCollection<int> max_val = (Histogram[1].Values as ObservableCollection<int>) ?? new ObservableCollection<int>();
            int rounding = 2500;
            max_val[0] = max + (rounding - max%rounding);
        }
        public void SetHistogram (byte[] data, int len)
        {
            const int max_bins = 12;
            int bins = max_bins < len/4 ? max_bins : len/4;
            SetHistogramYMax(0);
            Common.InitCollection(HistogramData, bins);
            for (int i = 0; i < bins; i++)
            {
                HistogramData[i] = BitConverter.ToInt32(data, i*4);
                if (HistogramData[i] > GetHistogramYMax())
                {
                    SetHistogramYMax(HistogramData[i]);
                }
            }
        }

        public void UpdateStatus ()
        {
            Span<byte> data = MessageInterface.Instance.Get(CommandType.Status, _id);
            SetStatus(data.ToArray(), data.Length);
            if (data.Length != 0)      
                Status = SensorStatus.Active;
            else
                Status = SensorStatus.Inactive;
        }

        public void UpdateHistogram ()
        {
            //highest value
            if (Status != SensorStatus.Active)
                return; 
            Span<byte> data = MessageInterface.Instance.Get(CommandType.Histogram, _id);
            SetHistogram(data.ToArray(), data.Length);
        }
    }
}