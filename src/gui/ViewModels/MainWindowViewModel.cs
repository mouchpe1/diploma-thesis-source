﻿using System.Collections.Generic;
using System.Windows.Input;
using System.IO.Ports;
using System.Threading;
using System.Reactive;
using ReactiveUI;
using DynamicData;
using DynamicData.Binding;
using Avalonia.Threading;
using System;
using dToFGUI.Models;

namespace dToFGUI.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        //Commands
        public ICommand RefreshDevices { get; }
        public ICommand ConnectDevice { get; }
        public ICommand OpenMenu { get; }
        public ICommand OpenView { get; }
        public ICommand SetMeasurementReference { get; }
        public ICommand ApplyDeviceSettings { get; }
        
        //Private Properties
        private DispatcherTimer Timer { get; }

        //Public UI Properties
        private bool _isMenuOpen;
        public bool IsMenuOpen
        {
            get => _isMenuOpen;
            set
            {
                this.RaiseAndSetIfChanged(ref _isMenuOpen, value);
            }
        }

        private int _currentView;
        public int CurrentView
        {
            get => _currentView;
            set
            {
                this.RaiseAndSetIfChanged(ref _currentView, value);
            }
        }

        //Public Properties
        public string[] AvailablePorts {
            get => SerialPort.GetPortNames();
        }

        public MessageInterface Msg {get; set;}
        public Device Dev {get; set;}

        //Public Settings
        private bool _syncMeasurementResult = true;
        public bool SyncMeasurementResult
        {
            get => _syncMeasurementResult;
            set
            {
                this.RaiseAndSetIfChanged(ref _syncMeasurementResult, value);
                Dev.SetEvalMode(Convert.ToByte(SyncMeasurementResult));
            }
        }

        
        //Constructor with command definition
        public MainWindowViewModel ()
        {
            Msg = MessageInterface.Instance;
            Dev = new Device();
            TimeSpan interval = new TimeSpan(0,0,0,0,400);
            Timer = new DispatcherTimer(interval, DispatcherPriority.MaxValue, TimerTick);

            RefreshDevices = ReactiveCommand.Create(() =>
            {
                this.RaisePropertyChanged(nameof(AvailablePorts));
            });

            ConnectDevice = ReactiveCommand.Create(() =>
            {
                Msg.Connect();
                Dev.SetEvalMode(Convert.ToByte(SyncMeasurementResult));
                Dev.GetSettings();
                Timer.Start();
            });

            OpenMenu = ReactiveCommand.Create(() =>
            {
                IsMenuOpen = !IsMenuOpen;
            });

            OpenView = ReactiveCommand.Create<string>((viewIndexStr) =>
            {
                int viewIndex;
                if (!int.TryParse(viewIndexStr, out viewIndex)) viewIndex = 0;
                CurrentView = viewIndex;
                IsMenuOpen = false;
            });

            SetMeasurementReference = ReactiveCommand.Create(() =>
            {
                foreach (MeasurementNode node in Dev.Measurement.Nodes)
                    node.SetNewReference();
            });

            ApplyDeviceSettings = ReactiveCommand.Create(() =>
            {
                Dev.ApplySettings();
            });
        }

        private void TimerTick (object? sender, EventArgs e)
        {
            foreach (Sensor s in Dev.Sensors)
            {
                s.UpdateStatus();
                s.UpdateHistogram();
                if (!Msg.IsConnected)
                    Timer.Stop();
            }
            Dev.EqualizeHistograms();
            Dev.Measurement.Update();

            Dev.SetEvalResult((byte)Dev.Measurement.Result);
        }
    }
}
