use <tools.scad>
use <MCAD/nuts_and_bolts.scad>
use <MCAD/regular_shapes.scad>

//Resolution of round surfaces
$fn = 100; //[0:1:100]
//Fit tolerance
tol = 0.15;
//Tight fit tolerance
ttol = 0.1;
//Loose fit tolerance
ltol = 0.22;
tol_vec = [tol, tol, tol];
ttol_vec = [ttol, ttol, ttol];
ltol_vec = [ltol, ltol, ltol];

//Part select
sel = 0; // [0:Full case, 1:Front, 2:Lid]

/*[Dimensions from FreeCAD]*/
case= [20, 17, 28];
fence_height = 15;
glass_offset = 8;
glass_h = 3;
case_angle = 20;
case_fillet = 5;
slot = 2 - tol;

/*[Case dimensions]*/
case_split = 6;
case_overlap = case.z - fence_height - glass_offset + tol;
lid_thicc = 2;
pcbhold_offset = 6;
pcbhold_h = 2;
pcbhold_w = 1;
pcb_rail = 0.2;

/*[Component dimensions]*/
//Dimensions of PCB
pcb = [12.5, 13.5, 1.6];
pcb_screw = 3.1;
sensor_pos = [6.25, 4.054];
sensor_tilt = 2;
sensor_voff = 7;
lens_h = 1.5;
lens_viewcoef = 2.8;
cable_d = 6;

/*[Hidden]*/
dt = [7,1.8,1.5];
extra = 0.01;
margin = 1;
angle_l = tan(sensor_tilt) * (case.z - glass_offset);
lid_bot_l = (case.y - case_split);
lid_top_l = (case.y - case_split) + angle_l;
lid_mid_l = case.y - (case_split+angle_l);
front_h = (case.z - glass_offset);
pcb_offset = (sensor_voff - sensor_pos.y)/cos(sensor_tilt);

cablehole_r = lid_mid_l - margin - dt.y;
cablehole_xoff = case.x/2 - cablehole_r - lid_thicc;
cablehole_yoff = lid_bot_l/2 - cablehole_r - dt.y - margin;


/////************************************************
/////************************************************
/////************************************************
module cable_hole(h = dt.z + margin + extra)
{
    hull() 
    {
        translate([cablehole_xoff,cablehole_yoff,0]) 
            cylinder(h=h, r=cablehole_r, center=true);
        translate([-cablehole_xoff,cablehole_yoff,0]) 
            cylinder(h=h, r=cablehole_r, center=true);
    }
}

module lid_shell ()
{
    translate([0,case.y/2 - (case.y - case_split - angle_l),case.z/2 - glass_offset]) 
    difference() 
    {
        cube(case, true);
        /*
        difference() 
        {
            fillet_xabs = (case.x/2 - case_fillet) + extra;
            fillet_yabs = (case.y/2 - case_fillet) + extra;
            translate([fillet_xabs,-fillet_yabs,0]) 
                rotate([0,0,-90]) 
                    fillet(r=case_fillet, w=case.z + extra);
            translate([-fillet_xabs,-fillet_yabs,0]) 
                rotate([0,0,180]) 
                    fillet(r=case_fillet, w=case.z + extra);
        }*/
        translate([0,(case.y - case_split)/2 + extra]) 
            hull()
        {
            angle_l = tan(sensor_tilt) * (case.z + 2*extra);
            translate([0,0,case.z/2]) 
                cube([case.x + extra, case_split, extra], true);
            translate([0,-angle_l/2,- (case.z/2 + extra)]) 
                cube([case.x + extra, case_split + angle_l, extra], true);
        }
    }
}

module lid ()
{
    difference()
    {
        union()
        {
            difference() 
            {   
                cut_ratio = [(case.x - 2*lid_thicc)/case.x, 
                            2, 
                            (case.z - 2*lid_thicc)/case.z];
                lid_shell();
                translate ([0,case.y/2,0]) scale(cut_ratio) lid_shell(); 
                //bottom cable passthrough
                passthrough = cable_d + 2*lid_thicc;
                translate([-passthrough/2,0,-glass_offset - tol + lid_thicc]) mirror([0,0,1]) rotate([90,0,0]) dovetail([passthrough,lid_thicc,case.y]);
            }

            //inside fill
            intersection()
            {
                lid_shell();
                union()
                {
                    translate([0,-lid_bot_l/2,(dt.z + margin)/2])
                    difference()
                    {
                        cube([case.x, lid_top_l, dt.z + margin], true);
                        //remove hole for cable
                        cable_hole();
                    }
                    //PCB rail
                    difference() 
                    {
                        union()
                        {
                            //PCB hold lip
                            translate([0,-lid_top_l/2 + angle_l,(sensor_voff - sensor_pos.y) + pcbhold_offset])
                            {
                                hold_w = (case.x - pcb.x)/2 + pcbhold_w;
                                translate([case.x/2 - hold_w/2, 0]) cube([hold_w, lid_top_l, pcbhold_h], true);
                                translate([-case.x/2 + hold_w/2, 0]) cube([hold_w, lid_top_l, pcbhold_h], true);
                            }
                            translate([0,-lid_top_l/2 + angle_l,(case.z - glass_offset)/2])
                            {
                                rail_w = (case.x - pcb.x)/2 + pcb_rail;
                                translate([case.x/2 - rail_w/2, 0]) cube([rail_w, lid_top_l, case.z - glass_offset], true);
                                translate([-case.x/2 + rail_w/2, 0]) cube([rail_w, lid_top_l, case.z - glass_offset], true);
                            }
                        }
                        translate([0,-lid_bot_l/2,front_h/2 - extra]) cable_hole(front_h);
                    }
                }
            }

            //positive dovetail
            translate([-dt.x/2,dt.y + angle_l,front_h - dt.z]) mirror([0,1,0]) dovetail(dt, lid_bot_l);
            
            //slots
            translate([case.x/2 + slot/2, -lid_mid_l + slot/2, (case.z - case_overlap)/2-glass_offset]) cube([slot, slot, case.z - case_overlap], true);
            translate([-(case.x/2 + slot/2), -lid_mid_l + slot/2, (case.z - case_overlap)/2-glass_offset]) cube([slot, slot, case.z - case_overlap], true);
            
            //top back cover
            translate([0, -lid_mid_l + slot/2, case.z - glass_offset - case_overlap/2]) cube([case.x, slot, case_overlap], true);
        }

        //back label
        label_size = 3.5;
        label_h = 0.4;
        translate([0,-lid_mid_l/2, case.z - glass_offset + extra]) mirror([0,0,1]) rotate([0,0,0]) union() 
        {
            translate([0,label_size/2]) linear_extrude(label_h) text(str(sensor_tilt,"° ", sensor_voff, "~"), 
                font="Noto Sans:Black", size = label_size, halign = "center", valign = "top");
        }

        //negative dovetail
        translate([-(dt.x + ttol)/2,-dt.y, -extra])  dovetail(dt + ttol_vec, lid_bot_l);
    }
}

module lens ()
{
    slot_r = 3.5 + tol;
    slot_l = 4.6 + tol;
    translate([-slot_l/2,0]) union()
    {
        circle(r=slot_r);
        translate([slot_l,0]) circle(r=slot_r);
        translate([0,-slot_r]) square([slot_l, slot_r*2]);
    }
}

module front () 
{
    front_y = angle_l + case_split;

    difference() 
    {
        //main front body
        union()
        {
            //positive dovetail
            translate([-dt.x/2,-dt.y,0]) dovetail(dt, front_y);
            translate([0,case_split/2 + angle_l, front_h/2]) 
                hull()
                {
                    translate([0,0,front_h/2]) 
                        cube([case.x, case_split, extra], true);
                    translate([0,-angle_l/2,- (front_h/2 + extra)]) 
                        cube([case.x, front_y, extra], true);
                }
        }
        //PCB, lens view cutout and upper edge chamfer
        rotate([-sensor_tilt]) 
            translate([-pcb.x/2,pcb.z + ltol - extra,pcb_offset]) 
                rotate([90,0,0]) 
            union()
            {
                difference() 
                {
                    cube(pcb + ltol_vec);
                    translate([0,0,-extra/2]) cylinder(h=pcb.z + ltol + extra, d=pcb_screw);
                    translate([pcb.x,0,-extra/2]) cylinder(h=pcb.z + ltol + extra, d=pcb_screw);
                }
                //lens
                translate ([pcb.x/2,sensor_pos.y, extra]) mirror([0,0,1])  union()
                {
                    translate([0,0,]) linear_extrude(height=lens_h + extra) lens();
                    translate([0,0,lens_h]) linear_extrude(height=2*case_split, scale = lens_viewcoef) lens();
                }
                //chamfer
                translate ([0,pcb.y + ltol - extra,pcb.z + ltol + extra]) rotate([0,90,0]) chamfer(a = pcb.z, h=pcb.x +ltol);
            }
        //negative dovetail
        translate([-(dt.x + ttol)/2,dt.y + angle_l,front_h - (dt.z + ttol) + extra]) 
            mirror([0,1,0]) dovetail(dt + ttol_vec, front_y);
    }
}

/////************************************************
/////************************************************
/////************************************************

//parts
if (sel == 1)
{
    front();
}
else if (sel == 2)
{
    translate([0,0,lid_mid_l]) rotate([90,0,0]) lid();
}
else
{
    front();
    lid();
}

echo(str("Cable hole size is ", cablehole_r, " mm"));
echo(str("Case overlap is ", case_overlap - tol, " mm"));