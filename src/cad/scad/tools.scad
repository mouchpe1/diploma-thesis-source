module clip_m (stem_h, stem_w, beak_h, beak_w, h)
{
    linear_extrude(height=h)
        polygon(points=[[0,0], [stem_w, 0], [stem_w, stem_h - beak_h], [stem_w + beak_w, stem_h - beak_h], [stem_w, stem_h], [0, stem_h]]);
}

module clip_f (stem_h, stem_w, beak_h, beak_w, h)
{
    union()
    {
        translate ([-beak_w,0,0]) cube ([stem_w + beak_w, stem_h, h]);
        translate ([0,stem_h-beak_h,0]) cube ([stem_w + 100, beak_h, h]);
    }
}

module fillet(r = 5, w = 5, center=true)
{
    difference()
    {
        translate([r/2,r/2,0]) cube([r,r,w], center=center);
        cylinder(r=r, h=2*w+0.1, center=center);
    }
}

module chamfer (a = 5, h= 2, left = true)
{
    mul = left ? 1 : -1;
    linear_extrude(height=h)
        polygon(points=[[0,0], [mul*a, 0], [0, a]]);
}

module dovetail (size = [5,1,1], tail = 0)
{
    tail_x = size.x - 2* size.y;
    linear_extrude(height=size.z)
        polygon(points=[[0,0], [size.x, 0], [size.x - size.y, size.y], [size.y, size.y]]);
    translate([size.x/2 - tail_x/2, size.y - 0.01, 0]) cube([tail_x, tail + 0.01, size.z]);
}
//clip_m (stem_h=15, stem_w=3, beak_h = 10, beak_w=3, h=5);
//%clip_f (stem_h=15, stem_w=3, beak_h = 10, beak_w=3, h=5);