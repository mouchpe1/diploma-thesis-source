
#include "app.h"

/*
 * Application entry point.
 */
int main(void) {
  //!App and component init
  platform_init();
  //Start application
  app_run();
}
