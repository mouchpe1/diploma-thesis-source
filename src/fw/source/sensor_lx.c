#include "include/sensor.h"

#ifdef SENSOR_USE_LXDRIVER

void sensor_start_ranging ( Sensor_t* sensor )
{
	if (sensor->state != SENSOR_STATE_RANGING)
		assert(VL53LX_StartMeasurement(&sensor->dev.lx) == VL53LX_ERROR_NONE);
	sensor->state = SENSOR_STATE_RANGING;
}

void sensor_stop_ranging ( Sensor_t* sensor )
{
	if (sensor->state != SENSOR_STATE_IDLE)
		assert(VL53LX_StopMeasurement(&sensor->dev.lx) == VL53LX_ERROR_NONE);
	sensor->state = SENSOR_STATE_IDLE;
}

void sensor_connect( Sensor_t* sensor_array, const int cnt )
{
	VL53LX_Error status = VL53LX_ERROR_NONE;
	//initialize sensor

	for (Sensor_t* i = sensor_array; i < sensor_array + cnt; i++)
	{
		if (!sensor_isconnected(i))
		{
			for (Sensor_t* j = i + 1; j < sensor_array + cnt; j++)
				sensor_disable(j);

			//check presence on a bus
			sensor_enable(i);
			uint16_t id = 0;

			i->dev.lx.i2c_slave_address = SENSOR_I2C_ADDRESS;
			if (VL53LX_RdWord(&i->dev.lx, 0x010F, &id) != VL53LX_ERROR_NONE)
				continue;

			assert(VL53LX_WaitDeviceBooted(&i->dev.lx) == VL53LX_ERROR_NONE);
			assert(VL53LX_SetDeviceAddress(&i->dev.lx, i->dev.addr) == VL53LX_ERROR_NONE);
			i->dev.lx.i2c_slave_address = i->dev.addr;
			status = VL53LX_RdWord(&i->dev.lx, 0x010F, &id);

			//init
			assert(VL53LX_DataInit(&i->dev.lx) == VL53LX_ERROR_NONE);
			//set additional measurement parameters (diasable histogram postprocessing)
			status |= VL53LX_SetDistanceMode(&i->dev.lx, i->params.mode);
			status |= VL53LX_SetTuningParameter(&i->dev.lx, VL53LX_TUNINGPARM_HIST_MERGE, 0);
			status |= VL53LX_SetTuningParameter(&i->dev.lx, VL53LX_TUNINGPARM_UWR_ENABLE, 0);
			status |= VL53LX_SetMeasurementTimingBudgetMicroSeconds(&i->dev.lx, i->params.ranging_ms*1000);
			//status |= VL53LX_PerformRefSpadManagement(&i->dev.lx);
			//status |= VL53LX_PerformOffsetPerVcselCalibration(&i->dev.lx, 290);
			assert(status == VL53LX_ERROR_NONE);
			i->state = SENSOR_STATE_IDLE;
		}
	}
}

uint8_t sensor_lx_data_ready;

SensorRet_t sensor_read_result ( Sensor_t* sensor, SensorResult_t* result)
{
	VL53LX_MultiRangingData_t tmp;
	VL53LX_AdditionalData_t hist;
	assert(VL53LX_GetMeasurementDataReady(&sensor->dev.lx, &sensor_lx_data_ready) == VL53LX_ERROR_NONE);
	//Get ranging data
	if (!sensor_lx_data_ready) //wait for valid data
		return SENSOR_RET_ERR;
	assert(VL53LX_GetMultiRangingData(&sensor->dev.lx, &tmp) == VL53LX_ERROR_NONE);
	if (tmp.RangeData->RangeStatus != 0)
		return SENSOR_RET_ERR;
	result->distance_mm = tmp.RangeData->RangeMilliMeter;
	//division by 65536 means that integer part of fix point data will be extracted
	result->signal_rate_kcps = ((float)tmp.RangeData->SignalRateRtnMegaCps / 65536.0)*1000;
	result->ambient_rate_kcps = ((float)tmp.RangeData->AmbientRateRtnMegaCps / 65536.0)*1000;
	result->sigma_mm = tmp.RangeData->SigmaMilliMeter / 65536.0;
	result->number_of_spad = tmp.EffectiveSpadRtnCount/256;
	//Get histogram
	assert(VL53LX_GetAdditionalData(&sensor->dev.lx, &hist) == VL53LX_ERROR_NONE);
	result->scene.max = hist.VL53LX_p_006.bin_data[0];
	result->scene.sum = 0;
	for (int i = 0; i < SENSOR_HIST_SIZE; i++)
	{
		result->scene.hist[i] = hist.VL53LX_p_006.bin_data[i];
		result->scene.sum += result->scene.hist[i];
		if (result->scene.hist[i] > result->scene.max)
			result->scene.max = result->scene.hist[i];
	}
	result->scene.bins = SENSOR_HIST_SIZE;

	return SENSOR_RET_OK;
}

void sensor_clear_interrupt	( Sensor_t* sensor )
{
	/* (Mandatory) Clear HW interrupt to restart measurements */
	if (sensor_lx_data_ready)
		VL53LX_ClearInterruptAndStartMeasurement(&sensor->dev.lx);
	sensor->state = SENSOR_STATE_RANGING;
}

uint16_t sensor_id ( Sensor_t* sensor )
{
	uint16_t id = 0xFFFF;
	VL53LX_RdWord(&sensor->dev.lx, 0x010F, &id);
	return id;
}
#endif
