#include "include/cmd.h"

#include "include/msg.h"

/*******************************************************************************
* Private command pointer table
*******************************************************************************/
CommandFunc_t cmd_links[] ={
		cmd_read_sensor,				//'A'
		NULL,							//'B'
		NULL,							//'C'
		cmd_set_evalmode,				//'D'
		cmd_set_eval,					//'E'
		NULL,							//'F'
		NULL,							//'G'
		cmd_read_hist,					//'H'
		NULL,							//'I'
		NULL,							//'J'
		NULL,							//'K'
		NULL,							//'L'
		NULL,							//'M'
		NULL,							//'N'
		NULL,							//'O'
		NULL,							//'P'
		NULL,							//'Q'
		NULL,							//'R'
		cmd_set_dist,					//'S'
		NULL,							//'T'
		NULL,							//'U'
		NULL,							//'V'
		NULL,							//'W'
		NULL,							//'X'
		NULL,							//'Y'
		NULL							//'Z'
};

/*******************************************************************************
* Private helper functions
*******************************************************************************/

uint8_t* _cmd_le_copy16 (uint8_t* const target, uint16_t data)
{
	uint8_t* ptr = target;
	*(ptr++) = data & 0x00FF;
	*(ptr++) = data >> 8;
	return ptr;
}

uint8_t* _cmd_le_copy32 (uint8_t* const target, uint32_t data)
{
	uint8_t* ptr = target;
	*(ptr++) = (data & 0x000000FF);
	*(ptr++) = (data & 0x0000FF00) >> 8;
	*(ptr++) = (data & 0x00FF0000) >> 16;
	*(ptr++) = (data & 0xFF000000) >> 24;
	return ptr;
}

uint8_t* _cmd_copy_result (uint8_t* const target, SensorResult_t* const result)
{
	uint8_t* ptr = target;
	ptr = _cmd_le_copy32(ptr, result->distance_mm);
	ptr = _cmd_le_copy32(ptr, result->ambient_rate_kcps);
	ptr = _cmd_le_copy32(ptr, result->signal_rate_kcps);
	ptr = _cmd_le_copy32(ptr, result->number_of_spad);
	ptr = _cmd_le_copy32(ptr, result->sigma_mm);
	return ptr;
}

SensorResult_t* _cmd_find_result (uint8_t id)
{
	if (id >= SENSOR_TOTAL_CNT)
		return NULL;
	SensorResult_t* requested = &meas.raw[0];

	uint8_t found = 0;
	for (int i = 0; i < board.assigned; i++)
		if (board.sensors_meas[i]->id == id)
		{
			requested = &meas.raw[i];
			found = 1;
			break;
		}

	if (!found)
		return NULL;

	return requested;
}

/*******************************************************************************
* Public commands
*******************************************************************************/

CommandFunc_t cmd_fetch (uint8_t id)
{
	uint8_t i = id - 65; //command ids are capital letters
	if (i < sizeof(cmd_links)/sizeof(CommandFunc_t))
	{
		return cmd_links[i];
	}
	else
		return 0;
}

uint8_t cmd_read_sensor(uint8_t* in, const uint8_t in_len, uint8_t* out)
{
	SensorResult_t* requested = _cmd_find_result(*in);
	if (in_len != 1 || requested == NULL)
		return 0;

	uint8_t* sensor_end =_cmd_copy_result (out, requested);
	return sensor_end - out;
}

uint8_t cmd_read_hist(uint8_t* in, const uint8_t in_len, uint8_t* out)
{
	SensorResult_t* requested = _cmd_find_result(*in);
	if (in_len != 1 || requested == NULL)
		return 0;

	uint8_t* ptr = out;
	for (int i = 0; i < SENSOR_HIST_SIZE; i++)
	{
		ptr =_cmd_le_copy32(ptr, requested->scene.hist[i]);
	}
	return (SENSOR_HIST_SIZE)*4;
}

uint8_t cmd_set_evalmode(uint8_t* in, const uint8_t in_len, uint8_t* out)
{
	if (in_len != 1)
		return 0;

	*out = 0;
	settings.remote_eval = *in;
	return 0;
}

uint8_t cmd_set_eval(uint8_t* in, const uint8_t in_len, uint8_t* out)
{
	if (in_len != 1)
		return 0;

	*out = 0;
	meas.object = *in;
	return 0;
}

uint8_t cmd_set_dist(uint8_t* in, const uint8_t in_len, uint8_t* out)
{
	*out = 0;

	if (in_len == 3 && *in == 0x1)
	{
		settings.eval_dist_treshold = (in[2] << 8) | in[1] ;
		eeprom_write(&EEPROMD, SETTINGS_ID, sizeof(settings), (uint32_t)(&settings));
	}
	else if ( in_len == 1 && *in == 0x0 )
	{
		_cmd_le_copy16(out, settings.eval_dist_treshold);
		return sizeof(settings.eval_dist_treshold);
	}
	return 0;
}
