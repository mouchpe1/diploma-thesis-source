#include "include/sensor.h"

#ifndef SENSOR_USE_LXDRIVER

void sensor_start_ranging ( Sensor_t* sensor )
{
	assert(VL53L4CD_StartRanging(sensor->dev.addr) == VL53L4CD_ERROR_NONE);
	sensor->state = SENSOR_STATE_RANGING;
}

void sensor_stop_ranging ( Sensor_t* sensor )
{
	assert(VL53L4CD_StopRanging(sensor->dev.addr) == VL53L4CD_ERROR_NONE);
	sensor->state = SENSOR_STATE_IDLE;
}

void sensor_connect( Sensor_t* sensor_array, const int cnt )
{
	VL53L4CD_Error status;
	//initialize sensor

	for (Sensor_t* i = sensor_array; i < sensor_array + cnt; i++)
	{
		if (!sensor_isconnected(i))
		{
			for (Sensor_t* j = i + 1; j < sensor_array + cnt; j++)
				sensor_disable(j);

			//check presence on a bus
			sensor_enable(i);
			uint16_t id = 0;
			if (VL53L4CD_GetSensorId(SENSOR_I2C_ADDRESS, &id) != VL53L4CD_ERROR_NONE)
				continue;

			assert(VL53L4CD_SetI2CAddress(SENSOR_I2C_ADDRESS, i->dev.addr) == VL53L4CD_ERROR_NONE);
			status = VL53L4CD_SensorInit(i->dev.addr);
			assert(status == VL53L4CD_ERROR_NONE);

			status = VL53L4CD_SetRangeTiming(i->dev.addr, i->params.ranging_ms, i->params.period_ms);
			assert(status == VL53L4CD_ERROR_NONE);
			i->state = SENSOR_STATE_IDLE;
		}
	}
}

SensorRet_t sensor_read_result ( Sensor_t* sensor, SensorResult_t* result)
{
	/* Read measured distance. RangeStatus = 0 means valid data */
	VL53L4CD_ResultsData_t tmp;
	VL53L4CD_GetResult(sensor->dev.addr, &tmp);
	if (tmp.range_status != 0)
			return SENSOR_RET_ERR;
	result->distance_mm = tmp.distance_mm;
	result->ambient_rate_kcps = tmp.ambient_per_spad_kcps;
	result->signal_rate_kcps = tmp.signal_per_spad_kcps;
	result->number_of_spad = tmp.number_of_spad;
	result->sigma_mm = tmp.sigma_mm;
	result->scene.bins = 0;
	result->scene.sum = 0;
	result->scene.max = 0;
	return SENSOR_RET_OK;
}

void sensor_clear_interrupt	( Sensor_t* sensor )
{
	/* (Mandatory) Clear HW interrupt to restart measurements */
	VL53L4CD_ClearInterrupt(sensor->dev.addr);
}

uint16_t sensor_id ( Sensor_t* sensor )
{
	uint16_t id = 0xFFFF;
	VL53L4CD_GetSensorId(sensor->dev.addr, &id);
	return id;
}

#endif
