#include "meas.h"
#include <math.h>

void meas_calc_scenediff (SceneData_t* a, SceneData_t* b, SceneData_t* out, int bins)
{
	out->max = a->hist[0] - b->hist[0];
	out->sum = 0;
	for (int i = 0; i < bins; i++)
	{
		out->hist[i] = a->hist[i] - b->hist[i];
		out->sum += out->hist[i];
		if (out->hist[i] > out->max)
			out->max = out->hist[i];
	}
	out->bins = bins;
}

float meas_calc_mean (SceneData_t* a)
{
	int total_abs = 0;
	float pos = 0;
	for (int i = 0; i < a->bins; i++)
	{
		total_abs += abs(a->hist[i]);
	}
	for (int i = 0; i < a->bins; i++)
	{
		pos += i * ((float)abs(a->hist[i]))/((float)total_abs);
	}
	return pos;
}

float meas_calc_wmean (SceneData_t* a)
{
	static float weights[] = {0.4, 1, 1.7, 1.7, 0.8};
	int total_abs = 0;
	float pos = 0;
	for (int i = 0; i < a->bins; i++)
	{
		total_abs += abs(a->hist[i]);
	}
	for (int i = 0; i < a->bins; i++)
	{
		pos += i * (weights[i] * (float)abs(a->hist[i]))/((float)total_abs);
	}
	return pos;
}

void meas_eval_obstacle	( void )
{
	float th = settings.eval_dist_treshold;
	meas.id++;
	meas.object = 0;

	for (int i = 0; i < board.assigned; i++)
	{
		//decide based on distance first - if it is low enough, object has to be in the scene
		if (settings.sensor_autoupdate[i] && (float)meas.raw[i].distance_mm < th)
		{
			meas.object = 1;
		}
		/*
		else
		{
			//get differential assessment
			SceneData_t object_area;
			SceneData_t total_area;

			meas_calc_scenediff(&meas.raw[i].scene, &meas.eval[i].empty_scene, &object_area, settings.eval_bins_object);
			meas_calc_scenediff(&meas.raw[i].scene, &meas.eval[i].empty_scene, &total_area, settings.eval_bins_max);

			float mean_total = meas_calc_wmean(&total_area);
			if (mean_total <= settings.eval_bins_object)
				if (abs(object_area.max) > 200 && abs(object_area.sum > 500))
				{
					//if (mean_total > settings.eval_bins_object - 1 &&
					//		meas.raw[i].scene.hist[settings.eval_bins_object - 2] - meas.raw[i].scene.hist[settings.eval_bins_object - 1] > 1500 )
					//	meas.object = 1;
					//else if (mean_total < settings.eval_bins_object - 1)
						meas.object = 1;
				}
		}*/

		//set current scene as empty reference if the current scene is lower
		//if (meas.id < 50 && meas.raw[i].scene.sum > 0 && meas.raw[i].scene.sum < meas.eval[i].empty_scene.sum)
		//	meas.eval[i].empty_scene = meas.raw[i].scene;
	}
}
