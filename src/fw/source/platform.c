#include "include/platform.h"

void stop_err(uint8_t led)
{
	while (1)
	{
		switch (led)
		{
		case 1:
			LEDB_TOGGLE();
			break;
		case 2:
			LEDG_TOGGLE();
			break;
		default:
			break;
		}
		osalThreadDelayMilliseconds(400);
	}
}

void print_err(const char * file, uint8_t line)
{
	printf("\nAssertion failed!\n\n");
	printf("File: %s!\nLine: %d\n\n", file, line);
	stop_err(2);
}

/*******************************************************************************
 * Static variables
*******************************************************************************/

//static uint8_t uart_rx_buf[1];

/*
 * Init implementation
 */

void platform_init( )
{
	  /* Initialization of all the imported components in the order specified in
	     the application wizard. The function is generated automatically.*/
	  componentsInit();

	  /* Start SPCStudio Drivers*/
	  sd_lld_start(&UART_DRV,&serial_config_pc);
	  i2c_lld_start(&I2C_DRV, &i2c_hw_config_master);
	  spi_lld_start(&RGB_DRV, &spi_config_argb);

	  /* dToF Power enable */
	  siul_lld_setpad(PORT_AVDD_EN, AVDD_EN);
	  siul_lld_setpad(PORT_IOVDD_EN, IOVDD_EN);

	  /* Start watchdog timer */
	  pit_lld_start(&PITD1, pit0_config);
	  pit_lld_channel_start(&PITD1,PIT0_CHANNEL_CH1);

	  /* Start button polling timer */
	  pit_lld_channel_start(&PITD1,PIT0_CHANNEL_CH2);

	  /* Enable Interrupts */
	  irqIsrEnable();
	  eirqInit();

	  uint32_t ret = eeprom_start(&EEPROMD, &eeprom_cfg);
	  if ((ret != EEPROM_OK) && (ret != EEPROM_FIRST_INIT_OK)) {
		  printf("Init EEPROM failed!");
	      stop_err(0);
	  }
}

void uart_write(uint8_t *buffer, uint16_t len)
{
	sd_lld_write(&UART_DRV, buffer, len);
	while (UART_DRV.tx_busy == SPC5_LIN_TX_BUSY) { ; }
}

void uart_read(uint8_t *buffer, uint16_t len)
{
	sd_lld_read(&UART_DRV, buffer, len);
}
