#include "include/app.h"

/*******************************************************************************
* Defines
*******************************************************************************/

#define RANGING_ACTIVE 150
#define RANGING_IDLE   100
#define BTN_PRESS	0b00000111

/*******************************************************************************
* Variables
*******************************************************************************/

volatile uint32_t wd_counter = 0;
volatile int     dtof_irq_id = -1;
volatile uint8_t wd_irq_calls = 0;
volatile uint8_t uart_irq_calls = 0;
volatile uint8_t btn[] = {0,0};

Board_t board;
Measurement_t meas;
Setting_t settings;

/*******************************************************************************
* Local functions
*******************************************************************************/

/* Interrupt handlers */

void wd_irq_handler( void )
{
	wd_irq_calls++;
}

void dtof0_irq_handler( void )
{
	dtof_irq_id = 0;
}

void dtof1_irq_handler( void )
{
	dtof_irq_id = 1;
}

void dtof2_irq_handler( void )
{
	dtof_irq_id = 2;
}

void dtof3_irq_handler( void )
{
	dtof_irq_id = 3;
}

void btn_irq_handler( void )
{
	 btn[0] = ( btn[0] << 1 ) | (siul_lld_readpad(PORT_BTN0, BTN0) & 0x1);
	 btn[1] = ( btn[1] << 1 ) | (siul_lld_readpad(PORT_BTN1, BTN1) & 0x1);
}

void uart_rx_handler ( SerialDriver *sdp )
{
	if (sdp == &UART_DRV)
		uart_irq_calls++;
}

/* Helper functions */

void toggle_sensor_autoupdate (int id)
{
	//check if no sensors are used for ranging
	int update = 0;
	for (int i = 0; i < board.assigned; i++)
	if (settings.sensor_autoupdate[i])
	{
		update = 1;
		break;
	}

	//clear button history and update value
	btn[id] = 0xFF;
	settings.sensor_autoupdate[id] = !settings.sensor_autoupdate[id];

	if (update == 0 && settings.sensor_autoupdate[id])
	{
		sensor_start_ranging(board.sensors_meas[id]);
		sensor_clear_interrupt(board.sensors_meas[id]);
	}

	if (settings.sensor_autoupdate[id])
		printf("Enabled capture from sensor %d \n", id);
	else
		printf("Disabled capture from sensor %d \n", id);
	delay_ms(200);
}
/*******************************************************************************
* Global functions
*******************************************************************************/

int app_sensor_next( int current )
{
	if (current < 0 || current > board.assigned)
		return -1;

	if (sensor_read_result(board.sensors_meas[current], &meas.raw[current]) == SENSOR_RET_OK)
	{
		sensor_stop_ranging(board.sensors_meas[current]);
		int tmp = current;
		do
		{
			current = (current + 1) % board.assigned;
		} while (!settings.sensor_autoupdate[current] && current != tmp);
		if (!settings.sensor_autoupdate[current])
			return -1;
		sensor_start_ranging(board.sensors_meas[current]);
		if (!settings.remote_eval)
		{
			meas_eval_obstacle();
		}
	}
	sensor_clear_interrupt(board.sensors_meas[current]);
	return current;
}

uint8_t app_sensor_assign( void )
{
	uint8_t assigned_cnt = 0;
	for (Sensor_t* i = board.sensors; i < board.sensors + SENSOR_TOTAL_CNT; i++)
	{
		if (sensor_isconnected(i))
		{
			if (assigned_cnt < SENSOR_MEAS_CNT)
			{
				board.sensors_meas[assigned_cnt++] = i;
			}
			else
				break;
		}
	}
	return assigned_cnt;
}

void app_settings_init ( void )
{
	meas.id = 0;
	settings.eval_dist_treshold = 270;
	settings.eval_bins_object = 5;
	settings.eval_bins_max = 10;
	settings.remote_eval = 0;

	for (int i = 0; i < SENSOR_MEAS_CNT; i++)
	{
		settings.sensor_autoupdate[i] = 1;
		meas.eval[i].empty_scene.sum = INT32_MAX;
		meas.eval[i].empty_scene.max = INT32_MAX;
	}
}

void app_settings_load (void)
{
	uint32_t sizeread = 0;
	uint32_t ret = eeprom_read(&EEPROMD, SETTINGS_ID, &sizeread, (uint32_t)(&settings));
	if (ret == EEPROM_ERROR_ID_NOT_FOUND || sizeread != sizeof(Setting_t))
	{
		app_settings_init();
		ret = eeprom_write(&EEPROMD, SETTINGS_ID, sizeof(settings), (uint32_t)(&settings));
		if (ret != EEPROM_OK)
		{
			printf("Settings save to EEPROM failed!");
			stop_err(0);
		}
	}
	else if (ret != EEPROM_OK) {
		printf("Read EEPROM failed!");
		stop_err(0);
	}
}

/*
 * Main app routine
 */

void app_run ( )
{
	SensorRangingParams_t sensor_params = {
		.ranging_ms = RANGING_ACTIVE,
		.period_ms = RANGING_IDLE,
		.mode = VL53LX_DISTANCEMODE_MEDIUM
	};
	sensor_init(&board.sensors[0], 0, 0x54, XSHUT0, PORT_XSHUT0, &sensor_params);
	sensor_init(&board.sensors[1], 1, 0x56, XSHUT1, PORT_XSHUT1, &sensor_params);
	sensor_init(&board.sensors[2], 2, 0x58, XSHUT2, PORT_XSHUT2, &sensor_params);
	sensor_init(&board.sensors[3], 3, 0x60, XSHUT3, PORT_XSHUT3, &sensor_params);
	app_settings_load();

	sensor_connect(board.sensors, SENSOR_TOTAL_CNT);
	msg_write_boot(board.sensors, SENSOR_TOTAL_CNT);

	board.assigned = app_sensor_assign();
	//start ranging on first assigned sensor
	if (board.assigned > 0)
		sensor_start_ranging(board.sensors_meas[0]);
	else
		stop_err(1);

	msg_start();


	/* Application main loop.*/
	while (1)
	{
		if (dtof_irq_id >= 0)
		{
			//find sensor slot that caused interrupt
			int current = 0;
			for (int i = 0; i < board.assigned; i++)
				if (board.sensors_meas[i]->id == dtof_irq_id)
				{
					current = i;
					break;
				}

			int next = app_sensor_next(current);
			if (next < 0)
				argb_set(0, COLOR_HOTPINK);
			else
			{
				if (meas.object == MEASRESULT_OBJECT)
					argb_set(0, COLOR_RED);
				else if (meas.object == MEASRESULT_FAKEOBJECT)
					argb_set(0, COLOR_YELLOW);
				else
					argb_set(0, COLOR_CHARTREUSE);
			}

			dtof_irq_id = -1;
			wd_counter = 0; //reset watchdog
		}
		if (wd_irq_calls > 0)
		{
			wd_irq_calls = 0;
			wd_counter++;

			if (wd_counter > 2)
			{
				int stuck = 0;
				for(int i = 0; i < SENSOR_TOTAL_CNT; i++)
					if (board.sensors[i].state == SENSOR_STATE_RANGING && settings.sensor_autoupdate[i])
						stuck++;
				if (stuck > 0)
				{
					sensor_connect(board.sensors, SENSOR_TOTAL_CNT);
					board.assigned = app_sensor_assign();
					if (board.assigned > 0)
						printf("Reconnected!");
				}
			}
		}
		if ((btn[0] & 0xF) == BTN_PRESS)
		{
			toggle_sensor_autoupdate (0);
		}
		if ((btn[1] & 0xF) == BTN_PRESS)
		{
			toggle_sensor_autoupdate (1);
		}
		if (uart_irq_calls > 0)
		{
			//argb_set(0, Pink);
			msg_read();
			uart_irq_calls = 0;
		}
	}
}
