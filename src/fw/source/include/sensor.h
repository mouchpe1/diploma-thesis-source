/**
 * @file sensor.h
 * @author Petr Moucha (petr.moucha@st.com)
 * @brief Sensor interface used in application
 * @version 1.0
 * @date 2022-11-09
 *
 * This interface abstracts access to both sigma-delta driver and histogram
 * driver. Former has less features and lacks the ability to provide 
 * histogram data, so if it's used, histogram size will be set to 0 to 
 * indicate that histogram isn't present.
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include "platform.h"
#include "vl53l4cd_api.h"
#include "vl53lx_api.h"

/*******************************************************************************
* Defines
*******************************************************************************/
#define SENSOR_I2C_ADDRESS 0x52 ///< Default I2C adress of sensor on startup
#define SENSOR_USE_LXDRIVER ///< Choose either LA or LX dToF driver

#ifdef SENSOR_USE_LXDRIVER
	#define SENSOR_HIST_SIZE VL53LX_HISTOGRAM_BUFFER_SIZE ///< Takes histogram size from LX driver
	#define SENSOR_HIST_AMBIENT 4
#else
	#define SENSOR_HIST_SIZE 0
	#define SENSOR_HIST_AMBIENT 0
#endif

/*******************************************************************************
* dToF Types
*******************************************************************************/

/**
 * @brief Return values of sensor functions
 */
typedef enum
{
	SENSOR_RET_OK = 0, ///< Function ended without error
	SENSOR_RET_ERR ///<Function did not run correctly
} SensorRet_t;

/**
 * @brief Sensor state values 
 */
typedef enum
{
	SENSOR_STATE_DISCONNECTED = 0, ///<Default state. Sensor is either not present or was not initialized yet.
	SENSOR_STATE_IDLE, ///< Sensor is connected but not used
	SENSOR_STATE_RANGING, /**< Sensor is active and is currently used in measurement. This value is set by
						   * sensor_start_ranging(Sensor_t* sensor) and persists until sensor_stop_ranging(Sensor_t* sensor) 
						   * is called, therefore it isn't affected by incoming interrupts.
						   */
	SENSOR_STATE_ERROR ///< Sensor is present but in invalid state
} SensorState_t;

/**
 * @brief Represents a histogram data of the scene
 * 
 * This struct also includes useful statistics that are used often
 * in evaluation and therefore don't have to be counted each time
 * the instance is accessed.
 */
typedef struct
{
	int32_t max; ///<Maximum value in histogram
	int32_t sum; ///<Sum of histogram values
	int32_t bins; ///<Number of used bins (can be lower than #SENSOR_HIST_SIZE)
	int32_t hist[SENSOR_HIST_SIZE]; ///<Histogram data
} SceneData_t;

/**
 * @brief Raw sensor results, calculated by sensor driver
 */
typedef struct
{
	uint32_t distance_mm; ///<Distance
	uint32_t signal_rate_kcps; ///<Signal rate
	uint32_t ambient_rate_kcps; ///<Ambient rate
	uint32_t number_of_spad; ///<Number of SPADs
	uint32_t sigma_mm; ///<Sigma (standard deviation)
	SceneData_t scene; ///<Histogram of the scene
} SensorResult_t;

/**
 * @brief Settings for a sensor driver
 */
typedef struct
{
	int	ranging_ms; ///<Active VCSEL pulse duration
	int	period_ms; ///<Ranging period
	VL53LX_DistanceModes mode; ///<Distance mode
} SensorRangingParams_t;

/**
 * @brief dToF device wrapper
 */
typedef struct
{
	uint8_t id;///<Internal ID of the sensor
	SensorState_t state; ///<Current sensor state

	struct
	{
		#ifdef SENSOR_USE_LXDRIVER
			VL53LX_Dev_t lx;
		#endif
		uint8_t addr; ///<Current I2C address
	} dev;

	uint16_t xshut_pin; ///<XSHUT pin number
	uint16_t xshut_port; ///<XSHUT port number
	SensorRangingParams_t params; ///<Ranging parameters
} Sensor_t;

/*******************************************************************************
* dToF Application API
*******************************************************************************/

/**
 * @brief Initializes sensor instance
 * 
 * @param sensor Sensor instance to be initialized
 * @param id Sensor ID
 * @param dev Sensor device I2C address
 * @param xshut_pin XSHUT pin
 * @param xshut_port XSHUT port
 * @param params Sensor ranging parameters
 */
void		sensor_init				( Sensor_t* sensor, uint8_t id, uint8_t dev, uint16_t xshut_pin, uint16_t xshut_port, SensorRangingParams_t* params);

/**
 * @brief Connects to an array of sensors.
 *
 * Sensor struct should be initialized with valid parameters before this function is called.
 * Sensors need to be connected as whole, because they all have same default I2C address upon
 * startup and only one sensor may be connected to a bus when the new I2C address is set.
 * System on each sensor will be initialize when this function ends.
 * 
 * @param sensor_array Array of sensor instances
 * @param cnt Number of sensors in the array
 */
void		sensor_connect 		    ( Sensor_t* sensor_array, const int cnt );

/**
 * @brief Disables sensor with XSHUT pin
 * @param sensor Sensor instance
 */
void		sensor_disable			( Sensor_t* sensor );

/**
 * @brief Enables sensor with XSHUT pin
 * @param sensor Sensor instance
 */
void		sensor_enable			( Sensor_t* sensor );

/**
 * @brief Starts ranging
 * @param sensor Sensor instance
 */
void		sensor_start_ranging   	( Sensor_t* sensor );

/**
 * @brief Stops ranging
 * @param sensor Sensor instance
 */
void 		sensor_stop_ranging   	( Sensor_t* sensor );

/**
 * @brief Toggles ranging
 * @param sensor Sensor instance
 */
void 		sensor_toggle_ranging   ( Sensor_t* sensor );

/**
 * @brief Disables and successively enables sensor
 * @param sensor Sensor instance
 */
void		sensor_reset 			( Sensor_t* sensor );

/**
 * @brief Reads ranging result from sensor
 * @param sensor Sensor instance
 * @param result Pointer to a result destination
 */
SensorRet_t	sensor_read_result		( Sensor_t* sensor, SensorResult_t* result);

/**
 * @brief Clears interrupt and starts next ranging period
 * @param sensor Sensor instance
 */
void		sensor_clear_interrupt	( Sensor_t* sensor );

/**
 * @brief Gets sensor ID
 * @param sensor Sensor instance
 */
uint16_t	sensor_id				( Sensor_t* sensor );

/**
 * @brief Checks if sensor is connected
 * @param sensor Sensor instance
 */
uint8_t		sensor_isconnected		( Sensor_t* sensor );


#endif /* SENSOR_H_ */
