#ifndef MSG_H_
#define MSG_H_

#include "../include/cmd.h"
#include "../include/platform.h"

/*******************************************************************************
* Defines
*******************************************************************************/

#define MSG_MAX_LEN 260
#define MSG_BYTE_HEAD 0x2A
#define MSG_BYTE_DATA 0x2B
#define MSG_BYTE_END 0x2D
#define MSG_RESPONSE_ACK 0x41
#define MSG_RESPONSE_ERR 0x45

/*******************************************************************************
* Types
*******************************************************************************/

typedef enum
{
	MSG_EXPECT_HEADER,
	MSG_EXPECT_DATA,
	MSG_RECEIVED_DATA
} MessageState_t;

/*******************************************************************************
* UART message interface
*******************************************************************************/
uint8_t msg_get_opcode( void );
MessageState_t msg_get_state ( void );

void msg_start( void );
void msg_read( void );
void msg_write_boot(Sensor_t* sensor_array, int cnt);


#endif /* MSG_H_ */
