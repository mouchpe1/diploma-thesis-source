/**
 * @file app.h
 * @author Petr Moucha (petr.moucha@st.com)
 * @brief Main application loop
 * @version 1.0
 * @date 2022-11-09
 */

#ifndef _DTOF_APP_H
#define _DTOF_APP_H
#pragma once

#include "platform.h"
#include "msg.h"
#include "cmd.h"
#include "app_types.h"
#include "argb.h"
#include "sensor.h"
#include "meas.h"


/*******************************************************************************
* Defines
*******************************************************************************/

//

/*******************************************************************************
* Interface
*******************************************************************************/

/**
 * @brief Assigns sensors to measurement slots
 * 
 * @return uint8_t : number of assigned sensors
 */
uint8_t app_sensor_assign( void );

/**
 * @brief Reads data from sensors used for measurements
 */
void app_sensor_read( void );

/**
 * @brief Initializes settings struct to default values
 */
void app_settings_init( void );

/**
 * @brief Recovers stored settings from eeprom
 */
void app_settings_load( void );

/**
 * @brief Main application loop
 */
void app_run ( void );

#endif
