/**
 * @file app_types.h
 * @author Petr Moucha (petr.moucha@st.com)
 * @brief Global types used by main application
 * @version 1.0
 * @date 2022-11-09
 */

#ifndef APP_TYPES_H_
#define APP_TYPES_H_

#include "platform.h"
#include "sensor.h"
#include "argb.h"

/*******************************************************************************
* Eval structs
*******************************************************************************/

/**
 * @brief Structure with additional measurement data for a single sensor
 * 
 * Structure that represents current post-processed measurement data for a single sensor.
 * It is used by meas functions as an additional storage of processed
 * raw sensor data.  
 */
typedef struct
{
	uint8_t object;
	SceneData_t last_scene;
	SceneData_t object_scene;
	SceneData_t empty_scene;
} EvalResult_t;

/*******************************************************************************
* Main App global types
*******************************************************************************/

/**
 * @brief Structure that represents board and its peripherals
 * 
 * This structure is used to store current status and configuration
 * of sensors and remaining peripherals.
 */
typedef struct
{
	Sensor_t sensors[SENSOR_TOTAL_CNT];
	Sensor_t* sensors_meas[SENSOR_MEAS_CNT];
	uint8_t assigned;
} Board_t;

/**
 * @brief Structure with firmware settings
 * 
 * This structure is used for data that should persist
 * after the system is shutdown. It is serialized as a whole into
 * data part of flash memory, using an EEPROM emulation.
 */
typedef struct
{
	//app settings
	uint8_t remote_eval;
	uint8_t sensor_autoupdate[SENSOR_MEAS_CNT];
	//eval settings
	int16_t eval_bins_max;
	int16_t eval_bins_object;
	int16_t eval_dist_treshold;
} Setting_t;

/**
 * @brief Structure with measurements for each sensor
 * 
 */
typedef struct
{
	unsigned long id;
	SensorResult_t raw[SENSOR_MEAS_CNT];
	EvalResult_t eval[SENSOR_MEAS_CNT];
	uint8_t object;
} Measurement_t;

#endif /* APP_TYPES_H_ */
