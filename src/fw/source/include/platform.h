/*******************************************************************************
* Platform specific includes
*******************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include <stddef.h>

#include "components.h"
#include "pit_lld_cfg.h"
#include "serial_lld_cfg.h"
#include "i2c_lld_cfg.h"
#include "spi_lld_cfg.h"

/*******************************************************************************
* Other includes
*******************************************************************************/
#include "printf.h"

/*******************************************************************************
* Public macro definitions
*******************************************************************************/
#define LEDB_TOGGLE()     siul_lld_togglepad(PORT_LED_B, LED_B)
#define LEDG_TOGGLE()     siul_lld_togglepad(PORT_LED_G, LED_G)
#define LEDB_SET()        siul_lld_setpad(PORT_LED_B, LED_B)
#define LEDG_SET()        siul_lld_setpad(PORT_LED_G, LED_G)
#define LEDB_CLEAR()        siul_lld_clearpad(PORT_LED_B, LED_B)
#define LEDG_CLEAR()        siul_lld_clearpad(PORT_LED_G, LED_G)

#define assert(err) ((err) ? (void)0 : print_err (__FILE__, __LINE__))
#define delay_ms(ms)	(osalThreadDelayMilliseconds(ms))
#define delay_us(us)	(osalThreadDelayMicroseconds(us))
#define time_ms()		(osalThreadGetMilliseconds())
#define time_us()		(osalThreadGetMicroseconds())

/*******************************************************************************
* Public value definitions
*******************************************************************************/
#define UART_DRV SD2
#define I2C_DRV I2CD1
#define RGB_DRV SPID1

#define SENSOR_TOTAL_CNT 4
#define SENSOR_MEAS_CNT 2
#define SETTINGS_ID 83

/*******************************************************************************
* Extern interrupt handlers
*******************************************************************************/

extern void uart_rx_handler ( SerialDriver *sdp );

/*******************************************************************************
* Platform interface functions
*******************************************************************************/
void uart_write(uint8_t *buffer, uint16_t len); //blocking tx write
void uart_read(uint8_t *buffer, uint16_t len); //async rx request

/*******************************************************************************
* Global help functions
*******************************************************************************/
/*
 * Board peripherals initialization
 */
void platform_init(void);

/*
 * Stay in error state
 */
void stop_err(uint8_t);

/*
 * Print failed assertion to UART
 */
void print_err(const char * file, uint8_t line);
