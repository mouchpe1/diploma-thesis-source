#ifndef INCLUDE_MEAS_H_
#define INCLUDE_MEAS_H_

#include "app_types.h"

extern Board_t board;
extern Measurement_t meas;
extern Setting_t settings;

typedef enum {
	MEASRESULT_EMPTY = 0,
	MEASRESULT_OBJECT = 1,
	MEASRESULT_FAKEOBJECT = 2
} MeasResult_t;

void meas_eval_obstacle	( void );
#endif /* INCLUDE_MEAS_H_ */
