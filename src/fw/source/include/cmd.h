#ifndef CMD_H_
#define CMD_H_

#include "../include/app_types.h"
#include "../include/platform.h"

/*******************************************************************************
* Defines
*******************************************************************************/
typedef uint8_t (*CommandFunc_t)(uint8_t*, const uint8_t, uint8_t*);

/*******************************************************************************
* Extern application variables
*******************************************************************************/
extern Board_t board;
extern Measurement_t meas;
extern Setting_t settings;

/*******************************************************************************
* Command interface
*******************************************************************************/
CommandFunc_t cmd_fetch (uint8_t id);

/*******************************************************************************
* List of commands
*******************************************************************************/
uint8_t cmd_read_sensor(uint8_t* in, const uint8_t in_len, uint8_t* out);
uint8_t cmd_read_hist(uint8_t* in, const uint8_t in_len, uint8_t* out);
uint8_t cmd_set_evalmode(uint8_t* in, const uint8_t in_len, uint8_t* out);
uint8_t cmd_set_eval(uint8_t* in, const uint8_t in_len, uint8_t* out);
uint8_t cmd_set_dist(uint8_t* in, const uint8_t in_len, uint8_t* out);

#endif /* MSG_H_ */
