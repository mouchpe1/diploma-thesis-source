#include "include/msg.h"

static uint8_t msg_in_buffer[MSG_MAX_LEN] = { 0x0 };
static uint8_t msg_out_buffer[MSG_MAX_LEN] = { 0x0 };

static uint8_t msg_header_len = 4;
static uint8_t msg_response_ack = MSG_RESPONSE_ACK;
static uint8_t msg_response_err = MSG_RESPONSE_ERR;

static MessageState_t state = MSG_EXPECT_HEADER;
static CommandFunc_t msg_cmd;
static uint8_t msg_op;
static uint8_t msg_data_len;

/*******************************************************************************
* Private helper functions
*******************************************************************************/

void _msg_abort ( void )
{
	state = MSG_EXPECT_HEADER;
	uart_read(msg_in_buffer, msg_header_len);
	uart_write(&msg_response_err, 1);
}

void _msg_ack_head ( void )
{
	msg_op = msg_in_buffer[1];
	msg_data_len = msg_in_buffer[2];
	msg_cmd = cmd_fetch(msg_op);
	if (msg_data_len > 0 && msg_cmd != 0)
	{
		state = MSG_EXPECT_DATA;
		//read size of data + framing
		uart_read(msg_in_buffer, msg_data_len + 2);
	}
	else
	{
		state = MSG_EXPECT_HEADER;
		uart_read(msg_in_buffer, msg_header_len);
	}
	uart_write(&msg_response_ack, 1);
}

void _msg_ack_data ( void )
{
	state = MSG_RECEIVED_DATA;
	uart_write(&msg_response_ack, 1);
}

void _msg_parse_uart( void )
{
	switch (msg_in_buffer[0])
	{
	case MSG_BYTE_HEAD:
		if (msg_in_buffer[msg_header_len - 1] == MSG_BYTE_END)
		{
			_msg_ack_head();
		}
		else
			_msg_abort();
		break;
	case MSG_BYTE_DATA:
		if (state == MSG_EXPECT_DATA && msg_in_buffer[msg_data_len + 1] == MSG_BYTE_END)
			_msg_ack_data();
		else
			_msg_abort();
		break;
	default:
		_msg_abort();
		break;
	}

}

/*******************************************************************************
* Getters and Setters
*******************************************************************************/

uint8_t msg_get_opcode( void )
{
	return msg_op;
}

MessageState_t msg_get_state ( void )
{
	return state;
}

/*******************************************************************************
* Public functions
*******************************************************************************/

void msg_start (void)
{
	state = MSG_EXPECT_HEADER;
	uart_read(msg_in_buffer, msg_header_len);
}

void msg_read( void )
{
	_msg_parse_uart();
	if (msg_get_state() != MSG_RECEIVED_DATA)
		return;

	//+1 because I want to skip start data byte in command interface
	uint8_t cmd_data_len = msg_cmd(msg_in_buffer+1, msg_data_len, msg_out_buffer + msg_header_len + 1);
	uint8_t* msg = msg_out_buffer;
	uint16_t msg_out_len = msg_header_len;
	*(msg++) = MSG_BYTE_HEAD;
	*(msg++) = msg_op;
	*(msg++) = cmd_data_len; //size of selected dtof result data
	*(msg++) = MSG_BYTE_END;
	if (cmd_data_len > 0)
	{
		*msg = MSG_BYTE_DATA;
		msg_out_len += cmd_data_len + 2;
		msg_out_buffer[msg_out_len - 1] = MSG_BYTE_END;
	}
	uart_write(msg_out_buffer, msg_out_len);

	//ready to recieve next command
	state = MSG_EXPECT_HEADER;
	uart_read(msg_in_buffer, msg_header_len);
}

void msg_write_boot(Sensor_t* sensor_array, int cnt)
{
	printf("\n\ndToF connection status:\n");
	for (int i = 0; i < cnt; i++)
	{
		if (!sensor_isconnected(&sensor_array[i]))
		{
			printf("%d: NOT PRESENT\n", i);
		}
		else
		{
			uint16_t id = sensor_id(&sensor_array[i]);
			printf("%d: CONNECTED (addr: 0x%x, id: 0x%x)\n", i, sensor_array[i].dev.addr, id);
		}

	}
}
