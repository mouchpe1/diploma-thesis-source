#include "include/sensor.h"

void sensor_init	( Sensor_t* sensor, uint8_t id, uint8_t dev, uint16_t xshut_pin, uint16_t xshut_port, SensorRangingParams_t* params)
{
	sensor->id = id;
	sensor->dev.addr = dev;
	sensor->xshut_pin = xshut_pin;
	sensor->xshut_port = xshut_port;
	sensor->params.ranging_ms = params->ranging_ms;
	sensor->params.period_ms = params->period_ms;
	sensor->params.mode = params->mode;

	sensor->state = SENSOR_STATE_DISCONNECTED;
}

void sensor_disable( Sensor_t* sensor )
{
  siul_lld_writepad(sensor->xshut_port, sensor->xshut_pin, PAL_LOW);
  delay_ms(50);
}

void sensor_enable( Sensor_t* sensor )
{
  siul_lld_writepad(sensor->xshut_port, sensor->xshut_pin, PAL_HIGH);
  delay_ms(100);
}

void sensor_reset( Sensor_t* sensor )
{
  sensor_disable( sensor );
  sensor_enable ( sensor );
}

void sensor_toggle_ranging ( Sensor_t* sensor )
{
	if (sensor->state == SENSOR_STATE_RANGING)
	{
		sensor_stop_ranging(sensor);
	}
	else
	{
		sensor_start_ranging(sensor);
	}
}

uint8_t sensor_isconnected ( Sensor_t* sensor )
{
	return sensor->state == SENSOR_STATE_IDLE || sensor->state == SENSOR_STATE_RANGING;
}
