/*
 * argb.h
 *
 *  Created on: 9. 9. 2022
 *      Author: Petr Moucha
 */

#ifndef ARGB_ARGB_H_
#define ARGB_ARGB_H_

#include <stdint.h>

/*******************************************************************************
* Predefined Colors
*******************************************************************************/
typedef enum {
	COLOR_BLACK = 0x000000,
	COLOR_RED = 0xFF0000,
	COLOR_GREEN = 0x00FF00,
	COLOR_BLUE = 0x0000FF,
	COLOR_PURPLE = 0x800080,
	COLOR_YELLOW = 0xFFFF00,
	COLOR_CHARTREUSE = 0x7FFF00,
	COLOR_PINK = 0xFFC0CB,
	COLOR_HOTPINK = 0xFF69B4,
	COLOR_AQUA = 0x00FFFF,
	COLOR_NAVY = 0x000080,
	COLOR_DARKCYAN = 0x008B8B,
	COLOR_DARKGREEN = 0x006400,
	COLOR_BLUEVIOLET = 0x8A2BE2,
	COLOR_AMETHYST = 0x9966CC,
	COLOR_WHITE = 0xFFFFFF
} Color_t;

/*******************************************************************************
* Platform specific functions
*******************************************************************************/
Color_t _argb_get (const uint8_t num);
void _argb_set (const uint8_t num, Color_t color);
void _argb_update (void);

/*******************************************************************************
* User API
*******************************************************************************/
void argb_set (int num, Color_t color);
void argb_toggle (int num, Color_t color);
void argb_toggle2 (int num, Color_t color_a, Color_t color_b);
void argb_clear (int num);

#endif /* ARGB_ARGB_H_ */
