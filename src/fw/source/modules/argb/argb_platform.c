#include "../../include/platform.h"
#include "include/argb.h"

#define ARGB_NUM_LEDS 1
#define ARGB_FRAME_SIZE 24
#define ARGB_RESET_SIZE 25
#define ARGB_LED_SIZE ARGB_FRAME_SIZE * ARGB_NUM_LEDS

#define ARGB_USE_COLORBUFFER
#define ARGB_INVERTED //comment out if output pin values should not be inverted
#ifdef ARGB_INVERTED
	#define ARGB_HIGH 	0x07
	#define ARGB_LOW 	0x3f
#else
	#define ARGB_HIGH 	0xf8
	#define ARGB_LOW 	0xc0
#endif

						   //{R,G,B}//
const uint8_t argb_order[] = {1,0,2}; //Specify position of each color according to LED chip ordering.

static uint8_t argb_led[ARGB_NUM_LEDS * 24] = { ARGB_LOW };
static Color_t argb_colors[ARGB_NUM_LEDS] = { COLOR_BLACK };

void _argb_update (void)
{
	spi_lld_send(&SPID1, ARGB_LED_SIZE, argb_led);
	spi_lld_ignore(&SPID1, ARGB_RESET_SIZE);
}

void _argb_set (const uint8_t num, const Color_t color)
{
	unsigned long color_cp = color;
	int idx = (ARGB_NUM_LEDS - num - 1) * ARGB_FRAME_SIZE + ARGB_FRAME_SIZE - 1;

	for (int color = 2; color >= 0; color--) //Starts from LSB
	{
		int color_offset = (2 - argb_order[color]) * 8;
		for (int i = color_offset; i < color_offset + 8; i++)
		{
			//These values work if pad is inverted
			if (color_cp & 1)
				argb_led[idx - i] = ARGB_HIGH;
			else
				argb_led[idx - i] = ARGB_LOW;
			color_cp >>= 1;
		}
	}
#ifdef ARGB_USE_COLORBUFFER
	argb_colors[num] = color;
#endif
}

Color_t _argb_get (const uint8_t num)
{
#ifdef ARGB_USE_COLORBUFFER
	return argb_colors[num];
#else
	int idx = (ARGB_NUM_LEDS - num - 1) * ARGB_FRAME_SIZE;
	unsigned long color_tmp = 0;
	for (int i = idx; i < idx + ARGB_FRAME_SIZE; i++) //Starts from LSB
	{
		if (argb_led[idx + i] == ARGB_HIGH)
			color_tmp = (color_tmp << 1) | 1;
		else
			color_tmp <<= 1;
	}
	Color_t color_out = 0;
	for (int color = 0; color < 3; color++)
	{
		int color_offset = (2 - argb_order[color]) * 8;
		color_out |= ((color_tmp >> color_offset) & 0xFF) << ((2 - color)*8);
	}
	return color_out;
#endif
}

