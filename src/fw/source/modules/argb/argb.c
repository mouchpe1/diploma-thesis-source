#include "include/argb.h"

void argb_set(int num, Color_t color)
{
	if (_argb_get(num) != color)
	{
		_argb_set(num, color);
		_argb_update();
	}
}

void argb_toggle2 (int num, Color_t color_a, Color_t color_b)
{
	Color_t current = _argb_get(num);
	if (current == color_b || current != color_a)
		_argb_set(num, color_a);
	else
		_argb_set(num, color_b);
	_argb_update();
}

void argb_toggle (int num, Color_t color)
{
	argb_toggle2(num, color, COLOR_BLACK);
}

void argb_clear (int num)
{
	argb_set(num, COLOR_BLACK);
}
