/*
 Copyright (c) 2021, STMicroelectronics - All Rights Reserved

 This file : part of VL53L4CD Ultra Lite Driver and : dual licensed, either
 'STMicroelectronics Proprietary license'
 or 'BSD 3-clause "New" or "Revised" License' , at your option.

*******************************************************************************

 'STMicroelectronics Proprietary license'

*******************************************************************************

 License terms: STMicroelectronics Proprietary in accordance with licensing
 terms at www.st.com/sla0081

 STMicroelectronics confidential
 Reproduction and Communication of this document : strictly prohibited unless
 specifically authorized in writing by STMicroelectronics.


*******************************************************************************

 Alternatively, VL53L4CD Ultra Lite Driver may be distributed under the terms of
 'BSD 3-clause "New" or "Revised" License', in which case the following
 provisions apply instead of the ones mentioned above :

*******************************************************************************

 License terms: BSD 3-clause "New" or "Revised" License.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************
*/

#include "../vl53l4cd/include/vl53l4cd_platform.h"
#include "../vl53l4cd/include/vl53l4cd_api.h"


/*******************************************************************************
* Local functions
*******************************************************************************/
VL53L4CD_Error VL53L4CD_ResolveError (i2c_result_t i2c_result)
{
	switch(i2c_result)
	{
		case I2C_ERROR_INVALID_ARGUMENT:
			return VL53L4CD_ERROR_INVALID_ARGUMENT;
		case I2C_NO_ERROR:
			return VL53L4CD_ERROR_NONE;
		default:
			return VL53L4CD_ERROR_TIMEOUT;
	}
}

/*******************************************************************************
* Global functions
*******************************************************************************/

uint8_t VL53L4CD_RdDWord(uint16_t dev, uint16_t RegisterAdress, uint32_t *value)
{
	uint8_t status = 0;
	uint8_t data_read[4];

	/* Uncomment if SPC55Studio libraries v1.17 and lower are used (to bypass the I2C bug)
	uint8_t data_write[2];
	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	status = i2c_lld_write(&I2C_DRV, dev, data_write[0], &data_write[1], 1U);*/
	status = i2c_lld_read16(&I2C_DRV, dev, RegisterAdress, data_read, 4U);

	*value =  ((data_read[0] << 24) | (data_read[1]<<16) |
			(data_read[2]<<8)| (data_read[3]));

	return VL53L4CD_ResolveError(status);
}

uint8_t VL53L4CD_RdWord(uint16_t dev, uint16_t RegisterAdress, uint16_t *value)
{
	uint8_t status = 0;
	uint8_t data_read[2];

	/* Uncomment if SPC55Studio libraries v1.17 and lower are used (to bypass the I2C bug)
	uint8_t data_write[2];
	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	status = i2c_lld_write(&I2C_DRV, dev, data_write[0], &data_write[1], 1U);
	*/
	status = i2c_lld_read16(&I2C_DRV, dev, RegisterAdress, data_read, 2U);

	*value = (data_read[0] << 8) | (data_read[1]);

	return VL53L4CD_ResolveError(status);
}

uint8_t VL53L4CD_RdByte(uint16_t dev, uint16_t RegisterAdress, uint8_t *value)
{
	uint8_t status = 0;
	uint8_t data_read[1];

	/* Uncomment if SPC55Studio libraries v1.17 and lower are used (to bypass the I2C bug)
	uint8_t data_write[2];
	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	status = i2c_lld_write(&I2C_DRV, dev, data_write[0], &data_write[1], 1U);
	*/
	status = i2c_lld_read16(&I2C_DRV, dev, RegisterAdress, data_read, 1U);

	*value = data_read[0];

	return VL53L4CD_ResolveError(status);
}

uint8_t VL53L4CD_WrByte(uint16_t dev, uint16_t RegisterAdress, uint8_t value)
{
	uint8_t data_write[3];
	uint8_t status = 0;

	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	data_write[2] = value & 0xFF;

	status = i2c_lld_write16(&I2C_DRV, dev, RegisterAdress, &data_write[2], 1U);
	
	return VL53L4CD_ResolveError(status);
}

uint8_t VL53L4CD_WrWord(uint16_t dev, uint16_t RegisterAdress, uint16_t value)
{
	uint8_t data_write[4];
	uint8_t status = 0;

	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	data_write[2] = (value >> 8) & 0xFF;
	data_write[3] = value & 0xFF;

	status = i2c_lld_write16(&I2C_DRV, dev, RegisterAdress, &data_write[2], 2U);

	return VL53L4CD_ResolveError(status);
}

uint8_t VL53L4CD_WrDWord(uint16_t dev, uint16_t RegisterAdress, uint32_t value)
{
	uint8_t data_write[6];
	uint8_t status;

	data_write[0] = (RegisterAdress >> 8) & 0xFF;
	data_write[1] = RegisterAdress & 0xFF;
	data_write[2] = (value >> 24) & 0xFF;
	data_write[3] = (value >> 16) & 0xFF;
	data_write[4] = (value >> 8) & 0xFF;
	data_write[5] = value & 0xFF;

	status = i2c_lld_write16(&I2C_DRV, dev, RegisterAdress, &data_write[2], 4U);

	return VL53L4CD_ResolveError(status);
}

uint8_t WaitMs(Dev_t dev, uint32_t TimeMs)
{
	(void) (dev);
	/* To be filled by customer's platform implementation. */
	osalThreadDelayMilliseconds(TimeMs);
	return VL53L4CD_ERROR_NONE;
}
