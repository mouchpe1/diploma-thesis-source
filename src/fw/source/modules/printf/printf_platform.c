#include "printf.h"


void _putchar(char character)
{
	if (sd_lld_write(&UART_DRV, (uint8_t*)(&character), 1) != SERIAL_MSG_OK)
		stop_err(2);

	while ((&UART_DRV)->tx_busy == SPC5_LIN_TX_BUSY) { ; }; //block until transmission is complete
}
