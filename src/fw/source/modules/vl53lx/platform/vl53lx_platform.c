
// SPDX-License-Identifier: GPL-2.0+ OR BSD-3-Clause
/******************************************************************************
 * Copyright (c) 2020, STMicroelectronics - All Rights Reserved

 This file is part of VL53LX and is dual licensed,
 either GPL-2.0+
 or 'BSD 3-clause "New" or "Revised" License' , at your option.
 ******************************************************************************
 */



#include <stdio.h>
#include <stdint.h>
#include <string.h>

#ifdef _MSC_VER
#define snprintf _snprintf
#endif

#include "include/vl53lx_platform.h"
#include "include/vl53lx_platform_log.h"

#define  VL53LX_I2C_CHUNK_SIZE  32

/*******************************************************************************
* Local functions
*******************************************************************************/
VL53LX_Error VL53LX_ResolveError (i2c_result_t i2c_result)
{
	switch(i2c_result)
	{
		case I2C_ERROR_INVALID_ARGUMENT:
			return VL53LX_ERROR_CONTROL_INTERFACE;
		case I2C_NO_ERROR:
			return VL53LX_ERROR_NONE;
		default:
			return VL53LX_ERROR_UNDEFINED;
	}
}




VL53LX_Error VL53LX_WriteMulti(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint8_t      *pdata,
	uint32_t      count)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint32_t     position       = 0;
	uint32_t     data_size      = 0;

	for(position=0; position<count; position+=VL53LX_I2C_CHUNK_SIZE)
	{
		if(count > VL53LX_I2C_CHUNK_SIZE)
		{
			if((position + VL53LX_I2C_CHUNK_SIZE) > count)
			{
				data_size = count - position;
			}
			else
			{
				data_size = VL53LX_I2C_CHUNK_SIZE;
			}
		}
		else
			data_size = count;

		i2c_result_t i2c_status;
		i2c_status = i2c_lld_write16(&I2C_DRV, pdev->i2c_slave_address, index + position, pdata + position, data_size);
		if (i2c_status != I2C_NO_ERROR)
		{
			status = VL53LX_ResolveError(i2c_status);
			break;
		}
	}

	return status;
}


VL53LX_Error VL53LX_ReadMulti(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint8_t      *pdata,
	uint32_t      count)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint32_t     position       = 0;
	uint32_t     data_size      = 0;

	for(position=0; position<count; position+=VL53LX_I2C_CHUNK_SIZE)
	{
		if(count > VL53LX_I2C_CHUNK_SIZE)
		{
			if((position + VL53LX_I2C_CHUNK_SIZE) > count)
			{
				data_size = count - position;
			}
			else
			{
				data_size = VL53LX_I2C_CHUNK_SIZE;
			}
		}
		else
			data_size = count;

		i2c_result_t i2c_status;
		/* Uncomment if SPC55Studio libraries v1.17 and lower are used (to bypass the I2C bug)
		uint8_t addr_h = (index + position) >> 8;
		uint8_t addr_l = (index + position) & 0xFF;
		i2c_status = i2c_lld_write(&I2C_DRV, pdev->i2c_slave_address, addr_h, &addr_l, 1U);
		*/
		i2c_status = i2c_lld_read16(&I2C_DRV, pdev->i2c_slave_address, index + position, pdata + position, data_size);
		if (i2c_status != I2C_NO_ERROR)
		{
			status = VL53LX_ResolveError(i2c_status);
			break;
		}
	}

	return status;
}


VL53LX_Error VL53LX_WrByte(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint8_t       VL53LX_p_003)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint8_t  buffer[1];


	buffer[0] = (uint8_t)(VL53LX_p_003);

	status = VL53LX_WriteMulti(pdev, index, buffer, 1);

	return status;
}


VL53LX_Error VL53LX_WrWord(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint16_t      VL53LX_p_003)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint8_t  buffer[2];


	buffer[0] = (uint8_t)(VL53LX_p_003 >> 8);
	buffer[1] = (uint8_t)(VL53LX_p_003 &  0x00FF);

	status = VL53LX_WriteMulti(pdev, index, buffer, VL53LX_BYTES_PER_WORD);

	return status;
}


VL53LX_Error VL53LX_WrDWord(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint32_t      VL53LX_p_003)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint8_t  buffer[4];


	buffer[0] = (uint8_t) (VL53LX_p_003 >> 24);
	buffer[1] = (uint8_t)((VL53LX_p_003 &  0x00FF0000) >> 16);
	buffer[2] = (uint8_t)((VL53LX_p_003 &  0x0000FF00) >> 8);
	buffer[3] = (uint8_t) (VL53LX_p_003 &  0x000000FF);

	status = VL53LX_WriteMulti(pdev, index, buffer, VL53LX_BYTES_PER_DWORD);

	return status;
}


VL53LX_Error VL53LX_RdByte(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint8_t      *pdata)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint8_t  buffer[1];

	status = VL53LX_ReadMulti(pdev, index, buffer, 1);

	*pdata = buffer[0];

	return status;
}


VL53LX_Error VL53LX_RdWord(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint16_t     *pdata)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint8_t  buffer[2];

	status = VL53LX_ReadMulti(
					pdev,
					index,
					buffer,
					VL53LX_BYTES_PER_WORD);

	*pdata = (uint16_t)(((uint16_t)(buffer[0])<<8) + (uint16_t)buffer[1]);

	return status;
}


VL53LX_Error VL53LX_RdDWord(
	VL53LX_Dev_t *pdev,
	uint16_t      index,
	uint32_t     *pdata)
{
	VL53LX_Error status = VL53LX_ERROR_NONE;
	uint8_t  buffer[4];

	status = VL53LX_ReadMulti(
					pdev,
					index,
					buffer,
					VL53LX_BYTES_PER_DWORD);

	*pdata = ((uint32_t)buffer[0]<<24) + ((uint32_t)buffer[1]<<16) + ((uint32_t)buffer[2]<<8) + (uint32_t)buffer[3];

	return status;
}



VL53LX_Error VL53LX_WaitUs(
	VL53LX_Dev_t *pdev,
	uint32_t       wait_us)
{
	VL53LX_Error status         = VL53LX_ERROR_NONE;
	SUPPRESS_UNUSED_WARNING(pdev);
	delay_us(wait_us);
	return status;
}


VL53LX_Error VL53LX_WaitMs(
	VL53LX_Dev_t *pdev,
	uint32_t       wait_ms)
{
	VL53LX_Error status = VL53LX_ERROR_NONE;
	SUPPRESS_UNUSED_WARNING(pdev);
	delay_ms(wait_ms);
	return status;
}

VL53LX_Error VL53LX_GetTickCount(
		VL53LX_Dev_t *pdev,
		uint32_t *ptick_count_ms)
{
	VL53LX_Error status  = VL53LX_ERROR_NONE;
	SUPPRESS_UNUSED_WARNING(pdev);

	*ptick_count_ms = time_ms();
	return status;

}


VL53LX_Error VL53LX_WaitValueMaskEx(
	VL53LX_Dev_t *pdev,
	uint32_t      timeout_ms,
	uint16_t      index,
	uint8_t       value,
	uint8_t       mask,
	uint32_t      poll_delay_ms)
{

	VL53LX_Error status         = VL53LX_ERROR_NONE;
	uint32_t     start_time_ms   = 0;
	uint32_t     current_time_ms = 0;
	uint8_t      byte_value      = 0;
	uint8_t      found           = 0;

	VL53LX_GetTickCount(pdev, &start_time_ms);
	pdev->new_data_ready_poll_duration_ms = 0;

	while ((status == VL53LX_ERROR_NONE) &&
		   (pdev->new_data_ready_poll_duration_ms < timeout_ms) &&
		   (found == 0))
	{
		status = VL53LX_RdByte(
						pdev,
						index,
						&byte_value);

		if ((byte_value & mask) == value)
		{
			found = 1;
		}

		VL53LX_GetTickCount(pdev, &current_time_ms);
		pdev->new_data_ready_poll_duration_ms = current_time_ms - start_time_ms;

		//poll delay
		delay_ms(poll_delay_ms);
	}

	if (found == 0 && status == VL53LX_ERROR_NONE)
		status = VL53LX_ERROR_TIME_OUT;

	return status;
}


