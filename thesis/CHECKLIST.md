# Pre-submission checklist

This is the list of actions and checks that needs to be done before final submission.

* abbreviations
* thank you note
* attachments
* **check if CD content fits the description**
* **check if assignment is up-to-date**
* remove todo notes
* check spaces after each \textohm
* right angle lines in all tikz diagrams
* formatting of part/net names
* VL53L4 vs VL53LA int text
* spaces between citations
* emphasis of names and special terms
* **rename FreeCAD parts**
* check abbreviations
* **replace proprietary LA ULD in repo**
* check firmware settings (crystal etc.)
* triple check PCB renders and layers 
    - in barrel jack labels