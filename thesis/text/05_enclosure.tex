\chapter{Enclosure design and assembly}
\label{h:enclosure}

\begin{figure}
    \centering
    \caption{3D render of the enclosure}
    \label{img:enclosure-render}
    \includegraphics[width=0.9\textwidth]{img/enclosure-render}
\end{figure}

The design of an enclosure was a balancing act between esthetics and customizability. It needed to look seamless for demonstrations, with each component tightly integrated, but it was also important to ensure that a small modification of the sensor placement does not mean that the entire enclosure needs to be manufactured again. The original plan was to use FDM 3D printing, which is relatively cheap and fast, but making large prints is still very time consuming and most of all inefficient, as the prototype versions of the model usually end up as waste that is hard to recycle. To solve this, the key parts of the sensor housing are modular, small, and quick to print. In addition, a great deal of effort was put into saving material and reducing print time by eliminating any redundancy in the design.

Most of the parts were designed in FreeCAD, which is a free and open-source graphical CAD software. The use of a graphical CAD was helpful because the mounting spot for the PCB had to be precise, with all connectors easily accessible through well-placed cutouts. Therefore, the workflow required the import of a PCB 3D model with all components, which could then be used as a visual reference during modeling, and all clearances could be verified before manufacturing. The customizability aspect of the enclosure stems mainly from the fact that FreeCAD allows for parameterization of parts, not from the ability to adjust parts after printing. Important dimensions or radiuses exist as a variable that can be referenced and used inside formulas. 

However, the use of a parameterization is to a large extent hindered by a bug known as \textit{topological naming problem} present in FreeCAD version 0.20, which was the latest at the time. When the placement of some geometry depends on a feature of another geometry and this parent geometry gets modified to such an extent that the number of faces and vertices changes, the design will most likely break because all the features of the parent will get internally renamed and links with child geometries will break. For example, this can manifest if the variable that controls the offset for the M3 nut slots on the lid is modified so that the slots will be completely inside the lid. This would be useful because the 3D printing of the lid could be stopped before the layer that covers the slot is printed, and the nuts could be placed into slots and then covered by the remaining layers. From an aesthetic point of view, the enclosure would look nicer and the assembly would be easier, since there would be no need to keep track of every nut. Basically, this could be a differentiating factor between the final and experimental versions of the lid. Changes like these will hopefully be possible in later versions, but for now, it is important to manually check that every feature is placed correctly with every parameter update.

\begin{wrapfigure}{R}{0.5\textwidth}
    \centering
    \caption{Sensor holder}
    \label{img:sensorholder}
    \includegraphics[width=0.5\textwidth]{img/sensorholder}
\end{wrapfigure}

Five different components are included in the FreeCAD project file: base, base with PCB mount, base lid, side cover, and light diffuser. All of them encase the glass pane, which serves as the detection surface. The 3D renderings and the final assembly are shown in Figures~\ref{img:enclosure-render} and \ref{img:enclosure}. While the parameterization of the above-mentioned components would cause merely unpleasant inconveniences due to the bugs in FreeCAD, designing a parametric sensor holder in this software would be nearly impossible. The final design of a sensor holder is shown in Figure~\ref{img:sensorholder} and is made of two interlocking parts that have a PCB with a sensor securely attached between them. The idea is to place this holder in a slot found on the lid, which will define the position and direction of the sensor. The bottom of the front part then rests on the glass pane, whereas the back part extends below the glass into a cavity in the base. This cavity serves as a path for cables that provide power and data to the sensors. It is larger than the holder to provide wiggle room for different lid configurations, so only a lid needs to be reprinted to test different sensor positions. Because the only way to remove or insert the sensor from the holder is to slide the front part down, the sensor is secure as long as the whole housing is in place, pressed against the glass.

The difficult part was implementing an easy way to change the tilt of the sensor and its distance from the glass surface. This would require a lot of sketching on angled planes, which is problematic in FreeCAD, especially when the position of the said plane would shift depending on the user parameters. Therefore, it was easier to move the sensor holder design to OpenSCAD, an alternative free and open-source CAD software that translates text files written in a custom scripting language into 3D models. Parameterization is a key feature of OpenSCAD and generally causes no problems, or at least the problems it causes can be easily solved, and it is the responsibility of the designer to make sure everything works as it should. The user is only presented with a list of parameters, which can be easily changed without any understanding of the underlying script, and the correct 3D model is generated based on the input values. Each sensor holder is also marked with two most important parameters - sensor tilt and sensor offset from the glass. They are printed on top of the holder so that each version can easily be identified.

\newpage

\section{Printing recommendations}

\begin{description}
    \item[Base] (with or without PCB slot) should be printed vertically, as it was designed in a way that allows printing without supports in this specific orientation. The cutout for a sensor holder has been chamfered and round holes for screws cause no issues. Even the CAN bus connector is printable because the right edge can be bridged easily, at least as long as there is a bottom edge that can serve as a bridging point. Hence, the current value of the base height should not be decreased because it leaves a minimal bottom edge that can be printed.
    \item[Lid] is printed in its natural orientation. For variant with buried nut slots, the print may be paused so that the nuts can be inserted and permanently embedded into the lid.
    \item[Side covers] must be printed with supports below the dovetail lock.
    \item[Light diffuser] should be printed in its natural orientation and with the smallest possible infill. The orientation and amount of filling affect how the light will look, and this arrangement worked best. Supports are not necessary.
    \item[Sensor holder] is printed in two parts. The dovetails will probably need a sanding because the front and back parts are printed in different orientations.
\end{description}

\section{Assembly instructions}

The assembly is carried out in the following order:

\begin{enumerate}
    \item Insert the PCB into its corresponding slot at the bottom of the correct base. On the inner side of the wall with the USB-C connector cutout, there is a shallow ridge into which the board slides. This ensures that even though there is no screw in the corner near the USB-C connector, the bord will be firmly fixed in its place.
    \item Secure the board with a single M3 screw placed in the hole marked M2 on the PCB. It is in the corner opposite a USB-C connector and the nut is placed from above into a hexagonal slot. The screw should be of the correct length to avoid any contact with the bottom of the glass surface.
    \item Press-fit side covers into dovetail slots on the upper part of the base. Ensure that the hole for a light diffuser is placed on the side where the PCB is located.
    \item Slide the glass pane into a railing formed by the two side covers.
    \item Press-fit the side covers into an opposing base (without the PCB slot). This will restrict the glass pane from moving. 
    \item Guide sensor PCBs from below the glass into a cavity intended for a sensor holder.
    \item Press-fit lids on both sides without losing the sensors. They should be accessible through the sensor holder slot on the lid.
    \item Guide each sensor PCB through the back part of the sensor holder and then press the sensor into the front part. Carefully slide the front upward against the back so that the dovetail locks fall into a place. 
    \item Push sensor holders into their respective slots and attach both lids with 8 M3 screws and nuts.
    \item Attach the light diffuser and press the sensor cables into the cable guides on the side covers if necessary.
\end{enumerate}

\begin{figure}[hb]
    \centering
    \caption{Photos of the fully assembled enclosure}
    \label{img:enclosure}
    \begin{subfigure}[c]{\textwidth}
        \includegraphics[width=\textwidth]{img/enclosure-a.JPG}
    \end{subfigure}
    \begin{subfigure}[c]{\textwidth}
        \includegraphics[width=\textwidth]{img/enclosure-b.JPG}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/enclosure-c.JPG}
    \end{subfigure}
    \hfill
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/enclosure-d.JPG}
    \end{subfigure}
\end{figure}