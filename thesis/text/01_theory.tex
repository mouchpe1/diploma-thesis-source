\chapter{Depth sensing technology}

\begin{chapterabstract}
	The ability to accurately judge distance from an object or even calculate a full depth map of a scene has many useful applications in the automotive industry, smartphones, or cameras. This chapter summarizes different approaches to solving this task using optical sensors.
\end{chapterabstract}

\section{Cameras}
Output from a typical camera sensor can be successfully utilized to detect objects and assess distances. Even a single camera with the help of a machine learning can not only detect if an object is present in the scene, but can also determine the type of object, recognize specific people and understand complex situations. However, a lot of processing is needed to obtain this information, and in practice a powerful GPU is mandatory. This comes at the cost of higher power consumption and a more complex system in general. When it is only useful to know if something enters the scene and detailed identification is not necessary, the detection of objects by their motion is a much less demanding alternative and can even be implemented into the CMOS sensor itself~\cite{Ohta2020SmartCMOS}. This can be great for security cameras, where it is useful to filter out uninteresting footage when the scene remains static. 

In either case, critical information will be missing, that is, the distance between the camera and the detected objects. There is only so much that can be extracted from a single image, and although a neural network can estimate distance from cues such as line angles and perspective, this task is inherently not deterministic, and the overall scale of objects is difficult to judge~\cite{Eigen2014SingleImageDepth}. For example, an image with a toy car could be ambiguous because the neural network expects a car to have a certain scale. The relative positions between the toy car and other objects in the photo can still be guessed, but the absolute distance from the camera would probably be incorrect.

For distance measurement and depth mapping, the better approach is to use \emph{stereoscopy}, which is two cameras that each capture a different point of view, analogous to our eyes that can naturally perceive depth with a similar arrangement. If the pitch of the cameras is known, the distance of an object that can be identified in both images is calculated by a relatively simple triangulation~\cite{Giancola2018}. Problems arise when there are not enough unique features in the scene that can be reliably matched between perspectives, so a projector may be added to project easily distinguishable patterns that will help the algorithm; this is known as \emph{active stereoscopy}~\cite{Giancola2018}. Another technique called \emph{structured light} also employs the projector, but this time only one camera is necessary. The difference is that the pattern is something regular, for example, a grid of dots, and when it gets projected onto an object, the distortion of the pattern is captured by a camera and can be used to determine the distance in each point~\cite{Zanuttigh2016}. In either case, the algorithms that perform the calculations are very demanding, so neither stereoscopy nor structured light are suitable for low-power real-time applications. 

\section{Time-of-Flight}

The idea of Time-of-Flight systems is very simple: a signal is sent at the target, and the distance can be determined from the time it takes for it to reflect back to a detector. RADAR and SONAR systems that have been around for a long time work on this principle and utilize radio and sound signals, respectively. However, the focus of this thesis is primarily on the optical relatives and the underlying technology. Passive components like simple cameras rely only on the light that is already in the scene, and therefore are very sensitive to ambient light and cannot work reliably in the dark without an additional illuminator. Bright light sources such as direct sunlight can easily saturate the sensor and, because of the limited dynamic range, objects might be completely lost in the image. In contrast, the optical Time-of-Flight systems are active because they only rely on light that they actively produce. They can work in complete darkness or in strong ambient light, even though there are still some limitations as well.

Every optical Time-of-Flight sensor has two components: an emitter and a receiver/photodetector. Today, the emmiter is usually a \emph{vertical-cavity surface-emmiting laser} (VCSEL) with a laser beam that is directed perpendicularly to the surface of a chip. Compared to an older \emph{edge-emmiting laser} (EEL), it is among other things more efficient at low power, the laser beam diverges more slowly, and most importantly, it is easier and cheaper to manufacture, as it can be tested right on the wafer, thus the production is more efficient~\cite{Michalzik2013}. 

The emmiter typically generates infrared light that is invisible to the human eye, but the exact wavelength depends on a number of factors. One of them is \emph{quantum efficiency} (QE), a measurement of the effectiveness with which photons can be converted to electrons; a higher QE means that the device is more sensitive to incoming light. In the visible spectrum, the QE of some cameras can exceed 90 \%, but reaching similar figures is generally more difficult as the wavelength increases~\cite{PrincetonQE}. The decreased sensitivity that comes with usage of an infrared light is compensated for by the better resistance to ambient light, thanks to a significant absorption of certain infrared wavelengths of solar radiation by the elements in the atmosphere. This relationship between quantum efficiency and solar radiation absorption is shown in Figure~\ref{img:qe}. The three most common VCSEL wavelengths, all of which lie in the dips caused by atmospheric water vapor, are the following:

\begin{itemize}
    \item 850~nm -- has a faint red glow and is useful in night vision applications, as the image quality and range are usually better than for higher wavelengths.
    \item 905~nm -- used in automotive industry, has better detection of wet or water-based objects than 940~nm~\cite{Wichmann2022LidarWeather}.
    \item 940~nm -- common in multiple consumer applications
\end{itemize}

\begin{figure}[htb]
    \centering
    \caption{Relationship between a typical QE curve and solar radiation}
    \label{img:qe}
    \hspace*{2pt}
    \begin{subfigure}[c][6cm][b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{img/qe2.png}
        \subcaption{Quantum efficiency in CCD sensors~\cite{PrincetonQE}}
    \end{subfigure}
    \hfill
    \begin{subfigure}[c][6cm][b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/solar}
        \subcaption{Solar radiation absorption~\cite{wiki:solar}}
    \end{subfigure}
    \hspace*{2pt}
\end{figure}

Higher wavelengths, like 1340~nm, belong to an even more advantageous region of Figure~\ref{img:qe}, where solar radiation is almost completely blocked and thus ambient light is less of a concern. Sensors that operate in this short-wave infrared band (SWIR) exist and can provide lower noise and even better eye safety~\cite{Bamji2022}, so the power output and consequently the range can be much higher. However, they can no longer be sillicon-based, and much more expensive materials such as Germanium (Ge) or Indium Gallium Arsenide (InGaAs) need to be used instead~\cite{Bamji2022, Manda2019}. This dramatically increases cost, so this trade-off is not warranted in most common cases.

In addition to the choice of wavelength, the illumination scheme is probably the next most important factor to consider on the emmiter side of the sensor. The simplest is the \emph{uniform/flood illumination}, where the laser beam is guided through an optical diffuser to cover the entire FoV~\cite{Kostamovaara2022, Padmanabhan2019}. The resolution that can be achieved is determined by the resolution of the detector, but the maximum range is limited by the SNR at each pixel or SPAD and long distance measurement requires higher power output. \emph{Spot illumination} attempts to overcome this issue by using single or multiple focused beams~\cite{Villa2021, Kostamovaara2022, Bamji2022}. In areas where the reflected beam hits the receiver, the SNR is much better than in the previous case, but the resolution now depends on the number of laser beams~\cite{Kostamovaara2022}. Alignment is also a concern, as the position of the beam shifts with distance of a target due to a parallax effect (more drastically for closer objects), but if the illuminated pixels are identified correctly, the performance can be better for the same power output compared to flood illumination. The last option is to use \emph{scanning} of the scene by a moving laser beam, which requires a more complicated optics for steering of the beam, but the advantage is that higher resolutions can be achieved with a lesser demand on the overall resolution of the receiver~\cite{Raj2020}.

When it comes to receiving reflected infrared photons, several technologies can be used, and they all have strengths and weaknesses, so it ultimately depends on what is most important for the specific application and which type of ToF sensor is used.


\subsection{Indirect Time-of-Flight}

In iToF sensors, the time of a signal's round trip is not measured directly, but instead derived from the phase shift of the obtained signal. For this to work, the emmitter has to send a continuous wave modulated in frequency (FMCW) or amplitude (AMCW), of which the amplitude modulation is more common~\cite{Bamji2022}. Ideally, the signal would be a sine wave, but in practice the square pulses are used because the design of a driver that controls the emmiter is less complex.

The receiver is made of a photodiode, which is a simple component that produces an electrical current when exposed to light. A variant called a \emph{pinned photodiode} (PPD) is the basis of modern CMOS sensors and can also be utilized in ToF systems~\cite{Acerbi2018, Shahandashti2022}. The schematic in Figure~\ref{img:itof} illustrates a single iToF pixel, with TGMEM signals that control the storage of the energy induced by the photodiode. TGMEM switching needs to be synchronized to the same frequency as the emitted signal, so that each TGMEM captures signal in the distict part of the period. In this exact case, the period of the signal is divided by 180\textdegree -- the duty cycle for both TGMEM signals will be 50\%, but when TGMEM1 is high, TGMEM2 is low and vice versa. This setup can easily be extended to more capture nodes that divide the period into finer segments. The incoming light is integrated with the TGMEM switching until enough charge is acummulated in memory nodes, and then each node is read. The ratio of charges then determines the phase shift and, consequently, the distance to an object.

This architecture is relatively simple and easily scalable for higher resolutions, as it is based on the technology present in CMOS imagers~\cite{Acerbi2018}. Therefore, iToF sensors are very popular for face recognition, gaming, or gesture control. The downside is that achieving long distance ranging is complicated without sacrificing spatial resolution in the process~\cite{Bamji2022}. The typical range with flood illumination is a few meters; for longer distances, spot illumination is mandatory. The frequency of the emmited pulses is also a key factor, as measurements of objects that are farther than the length of the emmited wave will be ambiguous. Schemes with multiple frequencies are sometimes used to provide wrap-around detection with lower frequencies and high depth resolution with higher frequencies~\cite{Gupta2015}. Multiple frequency modulation can also help reduce the impact of multiple-path interference caused by bouncing of light from objects other than the point of interest~\cite{Bamji2022}.

\begin{figure}
    \centering
    \caption{Principle of Indirect Time-of-Flight}
    \label{img:itof}
    \begin{subfigure}[c][6cm][b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{img/pixel}
        \subcaption{Pixel architecture}
    \end{subfigure}
    \hspace{1mm}
    \begin{subfigure}[c][6cm][b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{img/tgmem}
        \subcaption{Capture of the reflected wave with TGMEM switching}
    \end{subfigure}
\end{figure}


\subsection{Direct Time-of-Flight}

Going back to the original idea of Time-of-Flight, the dToF sensor can precisely determine when the reflected pulse arrived relative to when it was emitted. The problem with sending a single pulse is that it may not reflect back, because the target is either too far or has low reflectance, and a completely unrelated pulse may also trigger incorrect detection. Therefore, in practice, a series of pulses is transmitted over some integration period, and the arrival time of incoming photons is used to build a histogram, where each bin represents the number of photons in a specific time interval~\cite{Piron2021, Seo2021}. The objects in the scene will correspond to the peaks in the histogram, and the position of the peak can be used to calculate the distance according to a simple Formula~\ref{eq:dtof}, where $c$ stands for the speed of light.

\begin{equation}
    \label{eq:dtof}
    d = \frac{t_{peak}}{2} \times c
\end{equation}
\newpage

The photodetector in dToF is typically nowadays a \emph{single-photon avalanche diode} (SPAD), which is created by biasing a diode beyond its breakdown voltage. In this state, a single photon can cause an avalanche reaction in the SPAD that increases the current flow through the diode. Because a quenching resistor is connected in series with the SPAD, the current will also increase the voltage across the resistor, which will consequently result in a drop in voltage across the SPAD~\cite{Zhang2018}. With lower voltage, the avalanche event is slowly suppressed and the SPAD is brought back to its default state. \textit{The duration of this operation, during which the SPAD is unable to detect photons, is called dead-time, and
is typically a few nanoseconds long}~\cite{Piron2021}. The current spike is easily detected and converted by a \emph{time-to-digital converter} (TDC) to a digital pulse that can be processed and counted towards a relevant histogram bin. Although the high sensitivity, fast reaction time, and low timing jitter\footnote{\textit{the uncertainty between
actual and measured photon arrival time}~\cite{Charbon2018}} of SPADs are particularly advantageous for dToF~\cite{Gyongy2022}, they can be utilized in iToF sensors as well~\cite{Buckbee2022}.

\begin{figure}
    \centering
    \caption{Illustration of a relationship between histogram and the actual scene~\cite{Gyongy2022}}
    \label{img:dtof}
    \includegraphics[width=0.7\textwidth]{img/dtof}
\end{figure}

The key metric of SPAD is the photon detection probability (PDP), which is an SPAD equivalent for quantum efficiency, i.e. the sensitivy of the sensor~\cite{Piron2021, Dutton2015Spad}. As the name implies, it would be incorrect to assume that a single photon is guaranteed to cause an avalanche event in SPAD, but there is still a tight correlation between the two. Avalanche events that are not caused by photons might also occur, and this is usually referred to as a \emph{dark count rate} (DCR)~\cite{Charbon2018}. DCR along with ambient light form a noise ground floor that is the limiting factor to a maximum range of the sensor because at some point the returning signal is too weak to stand out. On the other hand, it is also possible to saturate the sensor with a signal that is too strong and that will limit the minimum detectable range.

The captured histogram must be post-processed with suitable algorithms that correctly identify peaks belonging to target objects. The scenario shown in Figure~\ref{img:dtof} highlights how some of the objects affect the shape of the histogram. Unwanted peaks might occur due to multipath reflection or crosstalk between emitter and receiver, but if the post-processing is successful and the cause of every peak is correctly identified, challenging tasks such as identifying multiple sufficiently spaced apart objects or objects behind semi-opaque surface are more straightforward compared to iToF~\cite{Gyongy2022}. 

The downside of using SPADs is that a readout process is more challenging than that of a photodiode because TDCs are more complex, require precise timing, and have relatively high power consumption. Current SPAD sensors also have a low fill factor\footnote{measure of how much of a sensor die is dedicated to a light sensing} of at most 5\% in 2D grid arrangements without microlenses, making scalability to high-resolution pixel arrays challenging~\cite{Xue2021,Intermite2015,Villa2015}. Although this technology is continuously improving due to advances in 3D stacking processes, the only viable way to achieve high spatial resolution with SPAD dToF is to use scanning instead of flash or spot illumination, which increases cost and complexity. Therefore, dToF is still mostly used in low-resolution/long-range applications. Almost everyone is familiar with the common usage in most smartphones, where the dToF sensor detects if the phone is close to an ear during calls so that the screen can be turned off and accidental interactions can be prevented.



\section{Automotive dToF from STMicroelectronics}

The physical sensor used throughout this thesis was a prerelease engineering sample provided directly by STMicroelectronics. As such, it was not available for purchase at the time and the only relevant public source of information was the VL53L4 dToF product page. This is because the sample shares most of the important characteristics with the VL53L4, except that it is designed to obtain automotive certifications. The VL53L4 exists in two variants: VL53L4CD and VL53L4CX. The CD variant is primarily intended for proximity sensing and its maximum range is limited to 1300 mm, while LX is capable of distinguishing even multiple objects spanning up to 5 meters. Each variant has its own specific software driver, but the overall control scheme is very similar. They also come in the exact same package, and the PCB layout recommendations are the same as well. The internal architecture of the automotive dToF is likely to be closer to VL53L4CD, because some features specific to the LX could not be accessed. Properties listed in the rest of this section are common to all three variants of dToF sensors (CD, LX, and automotive), unless explicitly stated otherwise.

\begin{wrapfigure}[15]{R}{0.4\textwidth}
    \centering
    \caption{VL53L4CX package~\cite{ds:vl53l4cx}}\label{img:ld-pkg}
    \includegraphics[width=0.35\textwidth]{img/l4-pkg}
\end{wrapfigure}

The emitter is the VCSEL with wavelength 940~nm, and the receiver is an SPAD array with 18\textdegree wide FoV. The sensor contains a 32-bit embedded MCU that handles the readout of SPADs and the consequent post-processing and distance estimation. I$^2$C interface is used to send these data to a host system. I$^2$C is a very common multi-master bus that only needs two wires -- \emph{synchronous data} (SDA) and \emph{synchronous clock} (SCL) -- connected with pull-up resistors to voltage representing logic high. The master node, usually configured as open-drain output, is then able to send information by pulling these two lines low according to protocol, so that the slave node may read the data frame. Every node on the bus must have its unique address (typically specified by the manufacturer), which is sent with every data frame. The default address is \verb|0x52|, but a scenario with multiple sensors on the bus is also supported, so there must be a way for the host system to modify this address.

That is where the XSHUT pin comes in. It can be controlled by the host, and pulling it low disables the sensor firmware so that it will not respond to I$^2$C requests. When the XSHUT of each sensor is connected to the host MCU, all sensors can be disabled except one, and that will mean that the I$^2$C command that changes the address will only be applied to that particular sensor. This process is repeated until every sensor has its unique address. If this feature is not needed, the XSHUT may simply be hardwired through a pull-up resistor to the IOVDD, so that the firmware always boots automatically when the power is present. However, the XSHUT should not be left floating, to avoid leakage current.

\begin{figure}
    \centering
    \caption{VL53L4 reference schematic~\cite{ds:vl53l4cd}}
    \label{img:l4-schematic}
    \includegraphics[width=0.9\textwidth]{img/l4-schematic.pdf}
\end{figure}

With every sensor addressable, the firmware initialization procedure may be started. An optional calibration and configuration may be performed, and then the main control loop is started with a function that activates the VCSEL and starts the ranging process. When the result is ready, the GPIO1 pin is pulled low and can be used as an interrupt signal for the host MCU. After the interrupt is detected, the host MCU reads the results and clears the interrupt, which automatically starts the next range cycle. If the interrupt cannot be used, it is possible to obtain the result by polling. The functional equivalents of all these commands are implemented in both the CD and LX software drivers. The drivers, especially the CD variant, are also not very demanding, so even a low performance system can easily handle the control procedure. Because the simpler CD sensor only measures the distance to a single target, it actually has some features that can further reduce the load on the host system. For example, the interrupt can be raised only if the measured distance is above or below some threshold set by the user. There is also a possibility of specifying a range with two thresholds and waiting for a measurement that lies inside or outside this range. 

\begin{figure}
    \centering
    \caption{VL53L4 system functional description~\cite{ds:vl53l4cx}}
    \label{img:l4-funcdesc}
    \includegraphics[width=0.9\textwidth]{img/l4-funcdesc}
\end{figure}

\begin{figure}
    \centering
    \caption{VL53L4CX block diagram~\cite{ds:vl53l4cx}}
    \label{img:l4-block}
    \includegraphics[width=0.9\textwidth]{img/l4-block}
\end{figure}