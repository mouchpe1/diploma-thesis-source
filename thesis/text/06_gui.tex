\chapter{Desktop application}
\label{h:gui}

The specialized GUI takes a notable amount of time to develop, which might not always be justified, but if it is done well and provides features that simplify the controls of the device or frequent operations, it is always appreciated. Here, the main purpose is to visualize histograms from the sensors, as arrays of numbers are harder to imagine and relate to each other than a graph that represents them. This visualization could be done in some data analysis software like Matlab or Excel, but a dedicated application typically provides several advantages. It works independently and does not have to rely on any other third-party software, which is often not free and requires licensing. Moreover, functionality and appearance can be extensively customized to suit more needs than just plotting of graphs, so the application can become a complete toolkit that also handles settings for the hardware, logging, diagnostics, and so on. 

The Avalonia UI was the framework of choice, because it is licensed under the MIT license, thus completely free to use, and also because it is basically a modern version of WPF, Microsoft's library for GUI development, which has a long history and good support. However, Avalonia has the edge over plain WPF because it supports multiple desktop and even mobile platforms. The workflow is otherwise very similar, and typically the MVVM (model-view-viewmodel) architectural pattern is encouraged. This architecture isolates the graphical elements and styling (view) from the data and logic (model), so that the backend can be tested independently. The viewmodel is a class that bridges this gap and exposes data from models to views through data bindings. Views like windows and user controls are described in the XAML markup language and C\# is used for the models, viewmodels and any other functional code. 

\begin{listing}
    \caption{Example of data binding in Avalonia UI}
    \label{code:binding}
    \begin{minted}{xml}
        <!-- View.axaml -->
        <TextBlock FontSize="14" Text="{Binding SumSignal}"/>
     \end{minted}
     \vspace{-0.5cm}
    \begin{minted}{csharp}
        // ViewModel.cs
        private float _sumSignal = 0;
        public float SumSignal
        {
            get => _sumSignal;
            set => this.RaiseAndSetIfChanged(ref _sumSignal, value);
        }
     \end{minted}
 \end{listing}


An example of data binding is shown in Code Listing~\ref{code:binding}, where the TextBlock element is created in XAML and two of its properties are set: FontSize is specified with constant, but the actual Text is \textit{binded} to a property SumSignal in ViewModel with special syntax. When elementary types are concerned, as in this case, the binding converts a numerical variable to a string automatically\footnote{This is true for every class that implements ToString() method, because the Text property of TextBox is of type string.}, but a user-defined converter may also be specified to resolve conversion in cases where an implicit conversion does not exist, for example, when the target type is part of the Avalonia framework, such as colors or font styles. With property binded, it is guaranteed that whenever its value changes, the Text in TextBlock also gets updated, even though the ViewModel has no knowledge about its existence. This is due to a special setter that notifies binded elements about the change. In the GUI, the MVVM pattern was followed as closely as possible, and the data in the UI elements were always set with data bindings. It is beyond the scope of this thesis to go further into the intricacies of the source code, but knowledge of these two principles should be sufficient to understand how and from where the graphical elements get the data. 

To build the application, .NET 6.0 must be installed on the system, and then a command \verb|dotnet build| must be called from the \textit{gui} folder in the project repository. Calling the \verb|dotnet run| will build and then start the application, but the application binary is also present in the \textit{bin} directory so it can be used without any special commands. The \verb|dotnet publish| command is however recommended to create a binary that is more suitable for distribution.

\section{Board View}
Upon launch, the application will go into board view. Here, the current status of all 4 sensor ports may be observed, and active ports will have their histogram data and other information updated in real time. The currect view is always highlighted in the menu on the left that is used as a main navigation element. Three menus in the upper row of the screen are visible across all views and are used to connect to a device, view the connection status, and view the object presence evaluation, respectively.

First, the correct UART device must be selected from the list of available devices. This list can be refreshed if the device was not connected before the application was launched. When the connection is successful, the dot next to the name of the interface will turn green and the sensor data will start updating immediately. Individual bin values may also be observed interactively with a mouse hover. 


\section{Measurement view}
\label{h:gui-alg}

To start object detection, the reference scene must first be set. This can be done from any view, and the scene should be empty and without any significant background elements during this process. The algorithm will then start processing the histogram data and the current detection status will be displayed in the top right corner. The processing is done inside the application, and currently the algorithm is very basic and observes only two properties of the histogram. The first is the ratio of neighboring bins between the reference and the current scene, and the second is the bin difference compared to the reference. If the ratio is higher than 2 in any of the first four bins of the histogram, the object will be detected. Four bins are used as a rough estimate of the number of bins that are the most affected by objects within the protected area. 

If the ratio is ambiguous and does not satisfy this criterion, the signal difference must be used. There, the calculation is no longer so straightforward, and more experimentation is needed. The issue basically is that the addition of the object does not always result in a positive change in the energy, but sometimes the energy decreases compared to the reference. For now, the algorithm does not deal with these conditions and simply finds the largest peak in the area bins, and then subtracts the average value of the bins that are not peaks. In other words, it is a measure of how much the peak stands out from the noise. If this number is greater than 400, the object is detected. The measurement view then shows both of these metrics and can be used to observe and tweak the thresholds. There is also a settings view that has a single checkbox that can pass the result of the algorithm to the device so that the LED also lights up according to evaluation result. 

\begin{figure}[h]
    \centering
    \caption{Board view of the dToF GUI}
    \label{img:gui-board}
    \includegraphics[width=0.99\textwidth]{img/gui-board.png}
\end{figure}

\begin{figure}[h]
    \centering
    \caption{Measurement view of the dToF GUI}
    \label{img:gui-meas}
    \includegraphics[width=0.99\textwidth]{img/gui-meas.png}
\end{figure}



