\chapter{Hardware design}
\label{h:hw}

This chapter follows the entire process of creating a highly integrated PCB that can reliably 
control multiple Time-of-Flight sensors over I$^2$C bus. First, the electrical schematic and the reasoning 
behind the selection of certain components are described. This is followed by an explanation of the critical 
aspects of PCB layout, component placement, and routing. The closing sections are dedicated to the 
manufacturing process and assembly of the board.

Altium Designer was chosen as the PCB design software mainly because it is a de facto standard in the industry and, therefore, makes potential future collaborations easier. 
Although it comes with many great features, one big disadvantage is that it is not free and open source, and viewing design files without a valid license might be difficult. 
For that reason, detailed PDF exports and also Gerber files used for fabrication are included in the attachments. Importing to free alternatives like KiCAD or Eagle is also possible and generally good enough for reference purposes.


It is important to inform the reader that the manufactured board shown in some of the photographs
and used throughout this thesis is version 1.0 and, as such, unfortunately contains some imperfections. 
Although they were not serious to the point that the board would have to be completely discarded, 
it would be confusing to show here 1.0 schematics that include these mistakes. Therefore, all schematic 
and board-view snippets come from version 1.1. Important differences will be discussed. 

\section{Electrical schematic and component selection}

The schematic is divided into 3 sheets, each dedicated to some logical part of the board: the MCU (with some peripherals), 
power distribution and dToF sensor interface. This section follows the same structure.

\subsection{Power inputs}
\label{h:power}

\begin{figure}
    \centering
    \caption{A7985A schematic}\label{img:buck}
    \includegraphics[width=0.9\textwidth]{img/buck}
\end{figure}

\begin{table}
    \centering
    \caption{List of A7985A components}\label{tab:buck}
       \begin{tabular}{l|l|c|c}
        \tabhead{Ref} & \tabhead{Type} & \tabhead{Reference component} & \tabhead{Selected component} \\ \hline \hline
            \designator{U3} & IC & \multicolumn{2}{c}{\textit{A7985A} - STMicroelectronics} \\ \hline
            \designator{C31} & Capacitor & \makecell{\textit{20TQC8R2M}\\8.2 uF / 20~V / 20 \%\\Panasonic} & \makecell{\textit{32SEQP6R8M}\\6.8 uF / 32~V / 
            20 \%\\Panasonic} \\ \hline
            \designator{C30} & Capacitor & \multicolumn{2}{c}{220 nF} \\ \hline
            \designator{C36} & Capacitor & \makecell{\textit{TPSB3360060250}\\33 uF / 6.3~V / 20 \%\\AVX} & \makecell{\textit{10SVP33M}\\33 uF / 10~V / 20 \%\\Panasonic}\\ \hline
            \designator{L2} & Inductor & \multicolumn{2}{c}{\textit{MSS1278H-473KED} - 4.7 uH / 4.4 A - Coilcraft} \\ \hline
            \designator{D2} & Diode & \makecell{\textit{STPS120MF} - 1 A / 20 V} & \textit{STPS2L25} - 2 A / 25~V \\ \hline
            \designator{C37} & Capacitor & 68 nF & 82 nF \\ \hline
            \designator{R21} & Resistor & 1.1 k\textohm & 1 k\textohm\\ \hline
            \designator{C35} & Capacitor & \multicolumn{2}{c}{1.2 nF} \\ \hline
            \designator{C32} & Capacitor & \multicolumn{2}{c}{15 nF} \\ \hline
            \designator{R18} & Resistor & 270 \textohm & 150 \textohm \\ \hline
            \designator{R19} & Resistor & \multicolumn{2}{c}{5.1 k\textohm~/ 1 \%} \\ \hline
            \designator{R22} & Resistor & \multicolumn{2}{c}{698 \textohm~/ 1 \%} \\ \hline
        \end{tabular}
\end{table}

\begin{wrapfigure}{L}{0.40\textwidth}
    \caption{Power component hiearchy diagram}\label{img:power}
    \begin{tikzpicture}
    \definecolor{darkblue}{RGB}{3, 35, 75}
    \definecolor{lightblue}{RGB}{60, 180, 230}
    \tikzset{
        part/.style={rectangle split, rectangle split parts=2, draw=lightblue, fill=lightblue!5, thick, minimum size=7mm, text width=2.1cm,align=center},
        input/.style={rectangle split, rectangle split parts=2, draw=red!60, fill=red!5, thick, minimum size=5mm,text width=2.1cm,align=center},
        ic/.style={rectangle split, rectangle split parts=2, draw=darkblue, fill=darkblue!5, thick, minimum size=5mm,text width=2.1cm,align=center}
    }
    %Nodes
    \node[input]      (dc)                      {\textbf{DC adapter} \nodepart{two} \textit{11 - 15 V}};
    \node[ic]        (buck)       [below=0.6 cm of dc] {\textbf{A7985A} \nodepart{two} \textit{VREG 5 V}};
    \node[input]      (usb)        [right=0.4 cm of dc]                      {\textbf{USB-C} \nodepart{two} \textit{VBUS 5 V}};
    \node[part]      (jumper)        [below=0.6 cm of buck]                      {\textbf{Jumper} \nodepart{two} \textit{5 V}};
    \node[ic]        (vdd)       [below=0.6 cm of jumper] {\textbf{LD49100} \nodepart{two} \textit{VDD 3.3 V}};
    \node[ic]        (iovdd)       [below=0.6 cm of vdd] {\textbf{LD49100} \nodepart{two} \textit{IOVDD 1.8 V}};
    \node[ic]        (avdd)       [right=0.4 cm of iovdd] {\textbf{LD49100} \nodepart{two} \textit{AVDD\\2.8 V}};
    
    %Lines
    \draw[->] (dc.south) -- (buck.north);
    \draw[->] (buck.south) -- (jumper.north);
    \draw[->] (jumper.south) -- (vdd.north);
    \draw[->] (buck.south) -- (jumper.north);
    \draw[->] (vdd.south) -- (iovdd.north);
    %\draw[->,rounded corners=5pt] (usb.south) -- ([yshift=+7pt]jumper.east);
    \draw[<-,rounded corners=5pt] ([yshift=+7pt]jumper.east) -| (usb.south);
    \draw[->,rounded corners=5pt] ([yshift=7pt, xshift=1.18cm]vdd.south) -| (avdd);
    \end{tikzpicture}
\end{wrapfigure}

Designing a reliable way to power the board is essential, especially in an automotive environment. That is why the primary 12~V power input is converted to 5~V by an automotive grade DC-DC buck converter. The exact model is A7985A and it is manufactured by STMicroelectronics. It can provide up to 2~A of current, which gives us a lot of headroom. The same can be said for the input voltage, which can range anywhere from 4.5 to 38~V.

This means that even 24~V input can technically be accommodated, although the efficiency of output would not be optimal in such a case. The reason is that, in general, the rest of the components around the switching converter should be adjusted for most common operating conditions. If these conditions are defined too broadly, unnecessary spending or suboptimal performance are likely to occur. Finding the right balance is not easy, so thankfully, STMicroelectronics provides an online tool called \href{https://eds.st.com/}{eDesignSuite} that can produce an optimized reference schematic for a multitude of power management components.

In this tool, the input voltage was chosen between 11 and 15~V, so even an unregulated car battery voltage that can vary depending on the remaining power is taken into account.
On the output side, the original intention was to select a 3.3~V output voltage that would be directly supplied to the MCU and other components, but ultimately a 5~V voltage was chosen because it was needed to power a CAN bus transceiver.
Finally, the output current was set to 1 A, mainly because it is a maximum value that the LD49100 regulator can provide for the derived power rails.

The generated schematic was further tweaked and the final version is shown in Figure~\ref{img:buck}. 
One key difference is the addition of reverse polarity and overvoltage protection, consisting of the shottky diode \designator{D1} and the TVS diode \designator{TR1}, respectively (both highlighted in purple). 
The overall structure of the schematic is the same; only some of the components were swapped for alternatives. 

A detailed comparison of the reference and selected components is listed in Table~\ref{tab:buck}. The input and output capacitors had to be replaced because the suggested parts were not available at the time. 
The diode \designator{D2} was unfortunately ordered before the decision was made to use the 5~V output, so the recommended part for the 3.3~V version was kept. 
Changes in the compensation net were made to obtain nicer and more typical values, but only the resistor \designator{R21} was selected manually -- the rest was calculated automatically by the online tool to reflect this change. 
All of these modifications had a marginal effect on the expected efficiency, which decreased only by 0.7\% from 89.5\% to 88.8\%.

Another 5~V power source, selectable with a jumper, is the USB-C connector used in conjunction with the CP2102 UART bridge\footnote{In the schematic, UART bridge is labeled as CP2102N instead of CP2102. This is because CP2102N is a designation for a newer version of this UART bridge and is fully compatible with the legacy CP2102}. 
This part of the schematic (shown in Figure~\ref{img:uart}) is based on the typical connection diagram in the CP2102 datasheet~\cite{ds:cp2102}, which provides a minimal working example, as well as several optional variations, of which two were implemented. 
The first is the addition of the 4.7 uF capacitor \designator{C42}, which is required if the internal 3.3~V regulator is used to power other components. This is intended to help in a situation where the USB portion of the board is used standalone in some other application. 

The second optional addon is the \designator{ZD1} ESD diode connected to the VBUS and data lines. It protects the board to some extent against short voltage spikes, but a PTC fuse could also have been added to provide overcurrent protection. This omission should be rectified in the next version of the schematic. Unfortunately, another more serious error was made: an incorrect connection of the RX and TX lines. Figure~\ref{img:uart} already shows the correct arrangement, but the manufactured board had to be fixed by crossing the RX/TX lines with cables.

\begin{figure}[t]
    \centering
    \caption{USB-C and UART bridge schematic}\label{img:uart}
    \includegraphics[width=0.9\textwidth]{img/uart}
\end{figure}

USB-C receptacle has a reduced pin count, which means that it does not offer USB 3.0 functionality, but it is easier to solder than the full 24-pin version. Since CP2102 is a USB 2.0 device that is typically used with a microUSB connector, the lack of high-speed pins is not a problem. Replacing microUSB with USB-C is not difficult; the only thing that requires extra attention are the CC1 and CC2 pins, which advertise to an upstream port what voltage and current the device requires. Typically, these pins are pulled down with 5.1 k\textohm~on USB 2.0 devices, and that is also the case here.

The last component in this section is the automotive grade LD49100 low dropout linear regulator that can lower the input voltage with fewer components and less output noise than switching regulators. However, its efficiency depends on the difference between input and output voltages -- a higher difference means that more power is lost as heat. Therefore, A7985A is first used to reduce 12~V to 5~V as effectively as possible, so that the input of LDOs is not too distant from their output. In total, three LDOs are used. One converts 5~V to 3.3 V, which then gets converted by remaining two to 1.8 and 2.8~V to power dToF and its communication. The full power component is illustrated in Figure~\ref{img:power}.

An adjustable version of LD49100 was chosen for this board, since one part can be used multiple times, reducing the complexity of the assembly process and making adjustments to output values possible. 
The output voltage is determined by a resistor divider whose values are calculated as $V_{out} = V_{adj} (1 + R_h/R_l)$ with $V_{adj} = 0.8$ V~\cite{ds:ld49100}.
Fixed voltage variants for 1.8 and 3.3~V also exist and can be used as a drop-in replacement. If this is the case, the reference schematic for the adjustable version in Figure~\ref{img:ldo} must be changed so that $R_h$ is shorted with 0 \textohm~resistor and $R_l$ is left unpopulated~\cite{ds:ld49100}.

LD49100 also has a very useful enable feature that allows the MCU to completely turn off its output. Both IOVDD and AVDD are controlled in this way, so the connected dToF sensors can be truly reset if some serious error is detected or simply to save power. The enable pin of the VDD is connected to 5 V, so it is permanently on, but it could also have been used as a simple manual power switch instead. This option will be considered in future iterations because currently it is not possible to shutdown the system without unplugging the cable or disconnecting the jumper. Grounding of the enable pin would not stress the hypothetical switch nearly as much as breaking the current path, so a cheaper part with a lower current rating could be used.

\begin{figure}
    \centering
    \caption{Adjustable LD49100 schematic}\label{img:ldo}
    \includegraphics[width=0.5\textwidth]{img/ldo}
\end{figure}

\subsection{Sensors}

The board must be able to communicate with up to four dToF sensors connected by a cable, and thus a suitable connector is necessary. Such a connector needs to provide a secure connection, but also be reasonably compact and easy to use. Ultimately, the SignalBee DF51K 8 pin double row connector from Hirose was selected as a great compromise between size, quality, and price. It boasts a strong and clicky locking mechanism that has been proved to be capable of withstanding accidental disconnections. Contacts are organized in a standard 2 mm grid layout, so finding an alternative should not be an issue in cases where this exact connector cannot be used. 2 mm spacing might be less common than 2.54 mm, but it provides a noticable space saving without making the socket contacts too small and too hard to crimp. The only downside is that this connector does not have automotive certification.

However, connecting the sensors to the board is only a small part of the problem. The bigger part was to ensure that the MCU and the sensors could communicate with each other, because the sensors are powered by a voltage different from that of the MCU, as stated in Section~\ref{h:power}. The goal was to prevent any problems with heat or power consumption because the 3.3~V of the VDD power rail is right at the upper limit of the specified operating conditions, so both IOVDD and AVDD were lowered to their typical datasheet values\cite{ds:vl53l4cd}. As a consequence, some form of voltage-level translation is necessary. 

Two different components were used to facilitate translation between VDD and IOVDD. The first is the PCA9306 from NXP, which translates the I$^2$C bus signals. It can handle speeds up to 400 kHz, which is below the maximum of 1 MHz that the dToF sensor can support in \textit{fast mode plus} and therefore can be a limiting factor in some cases. Fortunately, in this application, I$^2$C speed is not a bottleneck, so a regular fast mode is good enough. The pull-up resistors for SDA and SCL are externally connected on each side and their values were chosen based on the \textit{VL53L4CD NUCLEO expansion board} schematic~\cite{um:l4-expansion}.

\begin{figure}[ht]
    \centering
    \caption{Connection of dToF GPIO pins}\label{img:l4-gpio}
    \hspace*{2pt}
    \begin{subfigure}[c][5.5cm][b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{img/txs0104e-arch}
        \subcaption{using a TXS0104E voltage-level translator~\cite{ds:txs0104e}}
    \end{subfigure}
    \hfill
    \begin{subfigure}[c][5.5cm][b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/l4-gpio}
        \subcaption{referenced to VDD, no translation necessary~\cite{um:l4-expansion}}
    \end{subfigure}
    \hspace*{2pt}
\end{figure}

The rest of the sensor's GPIO signals are translated by three TXS0104E 4-bit bidirectional voltage-level translators from Texas Instruments, which exist in both industrial and automotive versions.
Their main advantage is that the 10 k\textohm~pull-up resistors are already integrated at each port, so the board space occupied by these translators is compensated for by the reduced number of resistors (see Figure~\ref{img:l4-gpio}). All TXS0104E translators have their ports permanently enabled according to the datasheet~\cite{ds:txs0104e}. Additional automotive-grade ESD protection is also present on the sensor side to further improve the integrity of the signals and prevent any damage to the system.

The sensor interface can theoretically be simplified in future versions because only six wires are strictly necessary to communicate with the dToF sensor. IOVDD can be left out because all pull-ups are part of the main board and the sensor does not need it as a reference nor a power source. It was included for potential future versions of the daughter board that might have more than just a sensor itself, or a completely different sensor altogether. The same applies to IO0-IO3 signals, which could be used to control additional circuits, for example, an LED indicator that will highlight which sensor is currently ranging or a temperature sensor. If these redundancies were left out, 6 pin connectors and only two TXS0104E translators would be required.


\subsection{MCU}

\begin{wrapfigure}[13]{R}{0.6\textwidth}
    \centering
    \caption{Clock crystal and debouncing capacitors of the SPC582B}\label{img:mcu}
    \includegraphics[width=0.6\textwidth]{img/mcu}
\end{wrapfigure}

The most vital component is, without question, the SPC582B automotive MCU. It has a single 32-bit PowerPC core, 1~MB of flash for code, 64~kB for data, and multiple peripherals including UART, CAN, I$^2$C, and SPI\cite{ds:chorus}. It can be powered from 3.3 or 5~V and is generally available in three packages:

\begin{itemize}
    \item eTQFP64 with 64 pins
    \item eTQFP100 with 100 pins
    \item QFN48 with 48 pins
\end{itemize}

Since the board already contains several components in QFN packages, it would be preferable to use a compact and estetically pleasing QFN48. Regrettably, it's pin count is too low, so a larger eTQFP64 had to be selected. The upside is that the eTQFP64 can be hand-soldered, and it is easier to find soldering errors and repair them. 

Not much is needed to get the MCU up and running, namely a clock crystal, a JTAG connector, and a network of decoupling capacitors. These components were selected based on the recommendations from the ST's automotive team, whose preffered schematic contains fewer capacitors than the official minimal working example available online~\cite{an:chorushw}, and therefore takes up less board space and is easier to route. In addition to capacitors, the essential components include a 36 MHz crystal and a 10 pin JTAG connector with 50 mil pitch, which is more compact than a full-size 14 pin 100 mil version in other automotive boards, so an adapter might be necessary to connect to a debugger.

\begin{wrapfigure}[13]{L}{0.3\textwidth}
    \centering
    \caption{Button with optional debouncing}\label{img:button}
    \includegraphics[width=0.28\textwidth]{img/button}
\end{wrapfigure}

Three buttons are present on the board: a reset button and two general purpose buttons that should allow for some basic controls in cases when the board is working standalone. All of the buttons are active low and can be debounced in hardware by changing the values of capacitor and series resistor. For a reset button, that would be \designator{C19} and \designator{R8} shown in Figure~\ref{img:button}, but all buttons are wired in the same way. By default, no debouncing is used, so a series resistor is shorted and a capacitor is not populated. Unfortunately, both general purpose buttons were mistakenly connected to GPIO pins that do not support external interrupts and must be polled by the firmware to detect a press.

For visual indications, two simple 0805 LEDs are available, but they are only useful for signaling error states because they are small and when the board is mounted inside a 3D printed enclosure (refer to Chapter~\ref{h:enclosure}), they will not be visible unless the whole device is turned upside down. As a result, a much more powerful and versatile WS2182B RGB addressable LED will indicate whether the object is present inside a scene or not. The schematic shows four of these LEDs, but the intention is to provide an option to mount two of them on either front or back of the PCB, depending on how the enclosure is to be illuminated. Technically, both sides can be occupied, but they cannot be controlled separately.

This is because each side is organized as a daisy chain pair (see Figure~\ref{img:rgb}) and both pairs would be controlled by the same signal coming from pin 48 of the MCU. This pin is internally connected to an SDO signal from the SPI peripheral, which can be used to generate a series of pulses according to the required protocol. Other SPI signals are not used. Daisy chaining feature of WS2812B allows for a simpler schematic that scales well, unlike a conventional RGB LED where each color channel has to be controlled separately by a PWM signal for each LED. So, two conventional LEDs would take up 6 MCU pins, while WS2812B needs just one no matter the number of LEDs. Limiting factors are only an available power and a refresh rate of the chain. 

\begin{figure}[hb]
    \centering
    \caption{Pair of daisy-chained WS2812B RGB LEDs}
    \label{img:rgb}
    \includegraphics[width=0.8\textwidth]{img/rgb}
\end{figure}

\section{PCB design}

The form factor of the board had to be carefully considered so that the design of an accompanying enclosure was not too complicated. Therefore, the board has a rectangular shape with three M3 mounting holes. The dimensions of the rectangle, as well as the position and spacing of the mounting holes and RGB LEDs, are of nice values rounded to the nearest millimeter to make things easier, and all important connectors are placed around the edges so that they can be exposed to the user through cutouts in the walls of an enclosure. The board has four layers organized in a typical fashion, with the inner ground plane, the inner power layer, and the outer layers mostly used to route the data signals.

At first sight, the most noticeable feature is the USB interface, which is separated from the rest of the board by large grooves. Therefore, the complete removal of the USB portion of the board is an option that can be useful in certain cases. Most likely, this would occur during evaluation, when the board undergoes different measurements and even stress tests that put the system outside of normal operating conditions. So, either intentionally or accidentally, things like short circuits can happen and put anything connected to a USB port in danger. The preferable solution is to use optically isolated or a wireless, e.g. Bluetooth, UART interface that would not expose the expensive equipment to such failures. Although integration of either feature would be possible, it would require a considerable amount of board space and increase cost.

Even after the USB interface is removed, it can function standalone as any other USB to UART converter. In this mode of operation, the \designator{P1} header should be mounted, as it serves as the main interface with the same order of pins as in typical Arduino compatible UART programmer. To use the power coming from the USB interface, the \designator{P2} header with appropriate jumper must be present to select between 3.3 and 5 V; otherwise, the VDD pin is disconnected. It is important to pay attention when using the version 1.0 of the board, because it has \designator{P2} labels swapped, so moving jumper to a position labeled 5~V puts 3.3~V on the output VDD pin and vice versa. Auto-reset during firmware flashing, feature common in some generic development boards, is also supported thanks to the exposed DTR and RTS pins. CTS may also be used instead of RTS and can be selected with a solder bridge on the bottom side.


\begin{figure}
    \centering
    \caption{Barrel jack footprint comparison}
    \label{img:barreljack}
    \begin{subfigure}[c]{0.35\textwidth}
        \includegraphics[width=\textwidth]{img/jack-a}
        \subcaption{Flat pins}
    \end{subfigure}
    \hspace*{10mm}
    \begin{subfigure}[c]{0.35\textwidth}
        \includegraphics[width=\textwidth]{img/jack-b}
        \subcaption{Round pins}
    \end{subfigure}
\end{figure}

\begin{wrapfigure}{L}{0.35\textwidth}
    \centering
    \caption{Oscillator layout}
    \label{img:guardring}
    \includegraphics[width=0.35\textwidth]{img/crystal}
\end{wrapfigure}

Another distinct area belongs to a buck converter in the upper left corner of the board, where it is sorrounded by LD49100 regulators to keep all power components nearby and maintain a clear and logical partitioning of the board space. Efforts were made to minimize interference due to buck converter switching activity, which is exemplified by the removal of copper under coil \designator{L2} and the separation of the signal and the power ground. Furthermore, the copper regions and traces that carry power were appropriately sized to meet current requirements. Unfortunately, version 1.0 of the PCB also contained an inconvenient footprint for the 5.5mm x 2.1mm barrel jack meant for parts with round pins, which are less common than parts with wide flat pins. These two footprints, shown in Figure~\ref{img:barreljack}, are incompatible.

Lastly, the MCU is on the right and all of the decoupling capacitors are placed underneath it on the bottom side, as close to the power pins as possible. This was the most sensible arrangement because 0805 capacitors are relatively large and would obstruct access to the signal pins if they were to be placed next to the chip itself. On the contrary, the crystal oscillator is placed on the front side near the MCU according to the recommendations contained in the design guidelines, which state that oscillator traces should be as short as possible to optimize performance and minimize EMC susceptibility~\cite{an:chorushw}. Electrical noise isolation is even further improved thanks to a guard ring around the oscillator. Details of its implementation are shown in Figure~\ref{img:guardring}, where it can also be seen that there is no copper under the oscillator area. As a consequence, no high-speed signal traces that might cause interference are present in the layers below, and parasitic capacitance should also be reduced.

The test points that simplify diagnostics are spread across the board; to name a few the I$^2$C and CAN bus can be diagnosed on either side of their respective transcievers, GPIO signals have test pads on the bottom layer below each connector, and each power rail can also be checked. One glaring omission is the lack of test points for the power rail enable signals, as there is no other easy way to probe them and they are critical for the operation of the board.

\section{Sensor daughter board}

\begin{wrapfigure}[12]{R}{0.5\textwidth}
    \centering
    \caption{Commercial VL53L4 satellite board}
    \label{img:satel}
    \includegraphics[width=0.5\textwidth]{img/satel.JPG}
\end{wrapfigure}

The daughter board for the automotive dToF sensor was also designed from the ground up. Solutions such as SATEL-VL53L4CD with VL53L4CD dToF (Figure~\ref{img:satel}), which is an industrial equivalent, are sold by STMicroelectronics and would work with the control board without problems. However, they lack one feature that limits the flexibility of measurement setups. In some asymmetric arrangements, for example, when the sensors are on the same side of the protected area (that is, they are not in the opposing corners, as illustrated in Figure~\ref{img:sensor}), the position of the transmitter and receiver is not the same relative to their field of view. Therefore, it is likely that the sensors would behave differently even in the optimal case where there are no objects in the background, there is no manufacturing disparity, and the ambient light is consistent and uniform.


As a workaround, it would be possible to rotate the board 180\textdegree, but that would also move the solder pads to a different place, so two different enclosures would have to be designed.
As a proper solution, the custom 2-layer board shown in Figure~\ref{img:reversible} supports mounting of a dToF sensor on either side, so the transmitter and receiver positions can be selected during the soldering process. Decoupling capacitors will typically be mounted on the side opposite the sensor to make space for a cover glass. The most common cover glass model is the IR012C0-PM3D-A066 manufactured by Hornix Optical Technology and, for this reason, its footprint is highlighted on the silkscreen for reference. In cases where cover glass is not used, the capacitors may also be placed next to the sensor. Cables can be soldered on either side.

\begin{figure}[htb]
    \centering
    \caption{Simplified illustration of a difference in FoV for asymmetric arrangement}
    \label{img:sensor}
    \includegraphics[width=0.9\textwidth]{img/asymm.pdf}
\end{figure}

\begin{figure}[H]
    \centering
    \caption{Custom reversible PCB for an automotive dToF sensor}
    \label{img:reversible}
    \begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/sensorpcb-front.jpg}
        \subcaption{Front side with Hornix cover glass}
    \end{subfigure}
    \hspace*{1mm}
    \begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/sensorpcb-back.jpg}
        \subcaption{Back side with blocking capacitors and unused alternative location for the sensor}
    \end{subfigure}
%    \hspace*{1mm}
%    \begin{subfigure}[c]{0.45\textwidth}
%        \includegraphics[width=\textwidth]{img/sensorpcb-render.png}
%        \subcaption{3D render}
%    \end{subfigure}
\end{figure}


\section{Manufacturing and assembly}

The boards were manufactured in the Czech Republic by the company Pragoboard. The POOL service manufacturing process suitable for prototype boards was chosen because it is their most affordable option and no additional features were necessary. The core material and the copper thickness could not be adjusted, but since the application does not rely on high-frequency signals and is not particularly demanding when it comes to power requirements, the default 18 \textmu m copper foil in the outer layers and 35 \textmu m in the inner layers (in the case of a 4-layer main PCB) did not pose a limitation. ITEQ-158 and ISOLA IS400 core materials were used for the sensor and the main board, respectively, and the total width of the PCB is equal to 1.5 mm in both cases. Matching stainless steel stencils were also produced for the precise application of solder paste.

Multiple ICs that are not easy to solder by hand were used, for example, ESD protection on sensor communication lines or the CP2102 UART transciever. So, from the start, the idea was to apply solder paste with the help of a stencil and then assemble the boards using a semi-automatic component placer. Since it is usually viable to apply solder paste only on one side, at least unless a dedicated stencil holder machine is available, most of the mandatory components were placed on the front side of the PCB during the design phase. The component placer allows the user to position each component gently and precisely, without smearing the applied solder paste, a feat that is very difficult to achieve if the components are placed by hand using tweezers. Smearing of a solder paste is dangerous because small solder balls can form outside of the individual pads, and they can later move and cause short circuits. The low-melt Sn42Bi58 solder alloy was used because there is a lower risk of damaging sensitive components or the board itself by excessive heat, especially when it is necessary to fix soldering errors with a hot air gun. 


\begin{figure}
    \centering
    \caption{Fix of the RX/TX wiring error}
    \label{img:rxtx}
    \begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/rxtx-a.jpg}
        \subcaption{The first option is to cut RX/TX traces on the bridge between UART module and the rest of the board. Then they can be reconnected at the two UART pin headers in the correct order. }
    \end{subfigure}
    \hspace*{1mm}
    \begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/rxtx-b.jpg}
        \subcaption{Alternatively, to avoid cutting into the PCB, resistors \designator{R1} and \designator{R2} that act as a failsafe against a short circuit can be removed, and their pads can be used instead of the \designator{P1} header.}
    \end{subfigure}
\end{figure}


With solder paste and components present, the boards were first baked at the appropriate temperatures in the reflow solder oven, and then some components such as connectors and pin headers had to be soldered separately by hand. All of the shortcomings of the first prototype that were critical to a board's function also had to be addressed. The barrel jack pins had to be filed to fit into insufficiently sized holes and the wrongly connected RX and TX signals of the UART transciever had to be crossed with the wires using one of the methods shown in Figure~\ref{img:rxtx}. 

The finished board was visually checked for soldering errors before being powered on and then carefully tested to verify that each component worked as expected. An issue that manifested during testing was that the jumper that selects between USB and 12~V power source causes an undesirable behavior when the board is powered with USB and jumper is either not present at all or is set to a 12~V input without any power adapter connected. In that situation, because the RX and TX pins of the UART transciever are pulled high, the voltage passes through the UART pins of the MCU to a VDD power rail and because it is the only voltage source at that point, it is used to power everything connected to the VDD. However, these pins are not meant to provide power and their voltage is also less than the 3.3~V expected for a VDD. Fortunately, neither the board nor the CP2102 were damaged, but this undefined behavior is still not desirable and should be fixed in later versions. 

Ideally, power would be automatically sourced from the 12~V supply if it is connected. One way to do this is to directly check the presence of the voltage on the positive pin. Most likely an operational amplifier that makes a comparison with some known reference voltage will be used for this purpose. The result can then be connected to a gate of a PMOS transistor or a dedicated load switch IC that will block USB power if a 12~V power adapter is used. Alternatively, the third pin of the barrel jack behaves like a mechanical switch that is normally closed and connected to a ground, but upon insertion of the plug it is opened. Therefore, it can also act as a control signal that switches power sources, with the difference that the board will not react to disconnection of the power adapter from the wall outlet. In either case, automatic supply switching would fix the aforementioned issue because the user would not have to manually reposition the jumper every time the USB becomes the sole power source. Not only would the behavior be deterministic, but user comfort would also be improved, as there is one less action that needs to be done when using the board.

\begin{figure}[H]
    \centering
    \caption{Assembled dToF Control Board 1.0}
    \label{img:mainpcb}
    \includegraphics[width=0.9\textwidth]{img/mainpcb.jpg}
\end{figure}

\begin{figure}[H]
    \centering
    \caption{3D render of the dToF Control Board 1.1}
    \label{img:mainpcb-render}
    \includegraphics[width=0.9\textwidth]{img/mainpcb-render.png}
\end{figure}