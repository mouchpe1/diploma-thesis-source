\chapter{Design and implementation of firmware}
\label{h:fw}

There is not much community support for automotive MCUs like SPC582B; therefore, the best resource is the datasheet, guidelines, and tools provided directly by the manufacturer. For that reason, SPC5Studio 6.0, an official and free Eclipse-based IDE with a hardware configuration wizard and other useful features, was chosen to develop the firmware. Conveniently, many examples of projects are included with simple demonstrations of how to use different peripherals, timers, and other hardware features. The hardware-specific code is distributed and updated directly through the IDE, but once it is downloaded, the project can be built using a generated Makefile without any other software but the compiler compatible with the PowerPC architecture.

The first step to create a new project is as simple as choosing the exact MCU model in the creation wizard, in this case SPC582B60E1. Different aspects of the MCU are set through \textit{components}, so only the required functionality is included in the source code. The firmware source depends on the following components:

\begin{description}
        \item[SPC582Bxx Platform Component RLA] Specifies the exact model of the MCU and is created by default
        \item[SPC582Bxx Init Package Component RLA] Dependency of other essential\\components
        \item[SPC582Bxx Board Initialization Component RLA] Allows changing the board\\name and assignment and settings of pins
        \item[SPC582Bxx Clock Component RLA] Specifies oscillator parameters and clock\\sources for different peripherals
        \item[SPC582Bxx IRQ Component RLA] Specifies handling and priority of external and internal interrupts
        \item[SPC582Bxx OSAL Component RLA] Sets system timer parameters
        \item[SPC582Bxx Low Level Drivers Component RLA] Important component used to\\configure communication peripherals, system timers, and DMA channels
        \item[Eeprom Emulation Driver] Used for EEPROM emulation in flash for persistent storage
        \item[Flash Driver] Dependency of the \emph{Eeprom Emulation Driver}
\end{description}

Each component presents itself as a list of curated options that allow for granular configuration of the system even without the knowledge of internal registers. This is especially helpful when defining the parameters of individual pins, because the interface (shown in Figure~\ref{img:pins}) displays the MCU package, where the position of each pin can be quickly compared to the PCB layout, and its parameters are set by clicking directly on its physical position. Making changes to the rest of the system was similarly straightforward. The clock tree configuration did not need adjustments at all, because the default value for the crystal is 40 MHz and matches the physical component. The same goes for the system timer and EEPROM emulation. Changes in the \textit{Low Level Drivers} and \textit{IRQ} components will be discussed in more detail in the relevant sections. 

\begin{figure}
    \centering
    \caption{SPC5Studio PinMap editor}
    \label{img:pins}
    \includegraphics[width=0.8\textwidth]{img/pins.png}
\end{figure}


The component settings are stored in the \textit{configuration.xml} file placed in the project root. Before starting the build process, SPC5Studio checks for changes in this file, and it will generate the \textit{components} folder that contains assembly and C source files with the necessary interfaces and configurations. In cases where some functionality is missing or does not work as expected, the generated components can be modified by the user. The SPC5Studio will update the \textit{patch.xml} file accordingly so that any changes are remembered and reapplied during component regeneration. Components were not changed in this project, so compatibility issues with future versions of SPC5tudio are less likely to occur. The starting point of the firmware is in the \textit{main.c} file that calls the main application loop. All user-defined code and external libraries are stored in the \textit{source} folder and organized according to Table~\ref{tab:fw}.

\begin{table}
\centering
\caption{Firmware source code structure}
\label{tab:fw}
 \begin{tabular}{c l} 
 \multicolumn{2}{c}{\tabhead{Main API}} \\
 \tabemph{platform} & Frequently reused platform-specific code and platform initialization \\
 \tabemph{app} & Main application loop, interrupt handling and shared global structs \\ 
 \tabemph{sensor} & Sensor interface, abstracts the access to LA and LX drivers\\
 \tabemph{meas} & Functions that process sensor data and determine object presence \\
 \tabemph{msg} & \makecell[l]{Implementation of simple message protocol for recieving commands\\from GUI over UART} \\
 \tabemph{cmd} & \makecell[l]{Defines functionality for valid command bytes accepted by\\the message protocol} \\
 \multicolumn{2}{c}{\tabhead{Additional modules}} \\
 \tabemph{vl53l4} & VL53L4 Ultra Lite driver \\
 \tabemph{vl53lx} & VL53LX Bare driver with histogram readout \\
 \tabemph{argb} & WS2812B Addressable RGB LED driver \\
 \tabemph{printf} & MIT licensed printf/sprintf implementation for embedded systems~\cite{printf} \\
 \end{tabular}
\end{table}

\section{Platform specific code}

The \textit{platform} header file mainly contains common macros and constants used throughout the rest of the application, such as the number of available sensor ports and peripheral IDs. Platform-specific functions and function-like macros provide access to some peripherals, and the idea behind this was to simplify the porting of the firmware to a different platform by making the \textit{platform.h} and \textit{platform.c} only files that need to be changed. However, this goal proved to be difficult to achieve because, for example, the reading from the UART peripheral works asynchronously and \mintinline{C}{uart_read(*buffer, len)} only sends a request to wait for the arrival of a specific number of bytes and quits immediately. When the full message is received in the buffer, the interrupt is fired, and it is the responsibility of the application to process it. Therefore, it is challenging to contain this kind of functionality in a single function. Thankfully, adapting simpler functionality, like blocking delay that waits for a certain amount of microseconds/milliseconds, or readout of the current value of system timer should be more straightforward. 

\section{Application loop}

The first function called by the \mintinline{C}{main()} is the \mintinline{C}{platform_init()} that initializes components provided by the SPC5Studio -- this includes the UART, I$^2$C and SPI peripherals, timers, and EEPROM emulation drivers. It also enables interrupts and turns on the power rails for the sensors by setting the EN pin of the IOVDD and AVDD LDOs. The \mintinline{C}{app_run()} is called next and it performs initialization of the application settings and, most importantly, finds which sensors are connected and assigns at most two (in order based on their port number) to available measurement slots. Port numbers are visible on the PCB and can also be determined by checking the boot message that lists which ports are occupied. The number of measurement slots can be changed by increasing the \mintinline{C}{SENSOR_MEAS_CNT} macro. If more sensors are connected, they will not be scheduled for ranging. The application will not continue if no sensor is connected.

\begin{table}
    \centering
    \caption{Interrupt table}
    \label{tab:irq}
     \begin{tabular}{c c c} 
     \tabhead{IRQ source} & \tabhead{Pin (eTQFP64)} & \tabhead{Handler}\\
     EIRQ 0 & 3 & \verb|dtof0_irq_handler| \\
     EIRQ 10 & 5 & \verb|dtof1_irq_handler| \\
     EIRQ 8 & 14 & \verb|dtof2_irq_handler| \\
     EIRQ 13 & 25 & \verb|dtof3_irq_handler| \\
     UART RX & 51 & \verb|uart_rx_handler| \\
     PIT0 Ch1 (1 Hz) & -- & \verb|wd_irq_handler| \\
     PIT0 Ch2 (12 Hz) & -- & \verb|btn_irq_handler| \\
     \end{tabular}
\end{table}

The command that starts ranging is sent to the first available sensor, and the main loop that waits for any of the interrupts described in Table~\ref{tab:irq} is entered. One of the interrupts will come from the sensor after it completes the ranging and the only action inside the interrupt handler is storing the sensor's port number to the \mintinline{C}{dtof_irq_id} variable. When it is determined in the main loop that \mintinline{C}{dtof_irq_id} contains valid port nummber, these actions are performed:

\begin{enumerate}
    \item Ranging for the sensor that caused the interrupt is stopped.
    \item Ranging is started for the next available slot.
    \item Results are evaluated with \mintinline{C}{meas_eval_obstacle()}, unless the evaluation is done externally through the GUI.
    \item Indication light is changed to red if an object is present and green if the scene is empty.
    \item \mintinline{C}{dtof_irq_id} is set to -1, and the watchdog is reset.
\end{enumerate}

Interrupt handlers for events generated by timers and processed UART read requests also set just one variable that is then regularly checked in the main loop. This ensures that all actions are performed sequentially in correct order, and handlers are short and have as little performance impact as possible. One timer is used as a watchdog that can be used as a safeguard in situations where the ranging interrupt does not come in an expected time window, possibly because the sensor is disconnected or the cable is damaged. The watchdog value is reset with each successful sensor readout.

Other timer polls buttons, because GPIO pins to which they are connected unfortunatelly do not support interrupts. With each tick of the timer, the current button value is shifted into an integer variable that represents a short history of pin states. If the pressed state is present for a sufficient number of ticks, the action can be performed. This is done as a debouncing countermeasure, a phenomenon where due to mechanical imperfections a button might oscillate between values during state transitions, and this can cause unwanted press detections if only an immediate state is considered. Currently, buttons are used to pause/resume ranging for each sensor slot, but a more fitting use case will probably be assigned in the future.

Board settings, all data captured by sensors, and evaluation data are stored in the following global variables: 
\begin{description}
    \item[\mintinline{C}{Board_t board;}] Contains an array of structs that represent each sensor port. Another array represents the measurement slots and is expected to contain pointers to elements in the previous array. The number of slots assigned is also stored here.
    \item[\mintinline{C}{Measurement_t meas;}]  Contains raw data captured from sensors, as well as data processed by the detection algorithm.
    \item[\mintinline{C}{Setting_t settings;}]  Contains board settings and parameters for the evaluation algorithm. This struct is designed to be saved into flash blocks that emulate EEPROM persistent storage.
\end{description}

The evaluation algorithm contained within the firmware is very simple, as it only compares the distance reported directly by the sensor to a fixed threshold stored in the \verb|settings| variable. If any sensor detects something closer to this threshold value, the object will be detected. This is not ideal because histogram knowledge is not used in any way, but it made sense to delegate the experimental detection algorithm to a desktop application because it is easier to debug and a lot more tools and libraries are available. If a reliable algorithm is ever developed, it will be ported directly to a firmware. For now, only basic functions that calculate different parameters of the histogram are implemented.

\section{Sensor interface}

The first attempts to control the dToF sensor were done with a relatively simple VL53L4CD ULD driver, which was already tested with a VL53L4 satellite board and was supposed to work with the automotive variant as well. This driver is divided into API functions and platform functions. The API includes functions for reading the result, clearing the interrupt, and also for calibration and configuration. It is a well-designed and compact library, so it was initially used directly within the main application. The programmer only needs to implement platform functions that read and write data via the I$^2$C bus. The measured data are stored into a struct where two parameters are especially important - distance in millimeters and signal intensity. These numbers are aggregated by the firmware inside the sensor and do not provide a complete picture of the scene. As the distance of an object from the sensor increased, these two numbers became more and more ambiguous without all the underyling data used in their computation.

Interestingly, it was discovered that the driver intended for VL53L4CX dToF, which uses the histogram to distinguish multiple objects over larger distances, also works with the automotive variant. It is not clear whether the performance is the same as with a proper VL53L4CX, but it was still possible to obtain individual histogram bins along with aggregated information about distance and signal intensity. However, this driver is much more complicated and is structured differently than the VL53L4CD ULD driver, so to allow usage of both in the application, a universal interface was created that abstracts both of these drivers, and it can be decided before compilation which one should be used. 

Each sensor is represented by a \mintinline{c}{struct} in Listing~\ref{code:sensor}. Some information is common between the CD and LX drivers, such as the need to know where the XSHUT pin is connected and what is the port number of the sensor. The difference is that the LX driver uses a custom object that represents the I$^2$C address of the device, whereas the CD driver simply stores the address as a byte. When it comes to ranging parameters contained within the \verb|params| member, the LX also needs additional information about the preffered distance range, but only \verb|VL53LX_DISTANCEMODE_MEDIUM| and \verb|VL53LX_DISTANCEMODE_SHORT| can be used. This is probably the main limitation of the automotive dToF, as the \verb|VL53LX_DISTANCEMODE_LONG| seems to be exclusive to a genuine VL53L4CX. Nevertheless, both drivers need to know the timing budget and the inter-measurement period. The timing budget specifies the duration of active ranging, when the VCSEL is active, and the valid values are in an interval from 0 to 200 ms. Longer ranging will result in better accuracy and longer maximum distance, while shorter ranging saves power~\cite{um:uld-manual}. The inter-measurement period specifies the time between starts of two consecutive rangings. When it is 0, the VCSEL is always active, but it can also be as high as 5 seconds. Larger values will reduce power consumption because VCSEL is active less frequently and also because the sensor goes to sleep mode if the timing budget ends and there is still time left until next ranging~\cite{um:uld-manual}. Lastly, a \verb|SensorState_t| is an \mintinline{c}{enum} type with four different states for when the sensor is disconnected, connected but idle, connected and ranging, or in an invalid state.

\begin{listing}
    \caption{Wrapper representing a status of dToF sensor}
    \label{code:sensor}
    \begin{minted}{c}
typedef struct
{
    uint8_t id;///<Internal ID of the sensor (port number)
    SensorState_t state; ///<Current sensor state

    struct
    {
        #ifdef SENSOR_USE_LXDRIVER
            VL53LX_Dev_t lx;
        #endif
        uint8_t addr; ///<Current I2C address
    } dev;

    uint16_t xshut_pin; ///<XSHUT pin number
    uint16_t xshut_port; ///<XSHUT port number
    SensorRangingParams_t params; ///<Ranging parameters
} Sensor_t;
     \end{minted}
 \end{listing}

\subsection{Functions}

The sensor API was designed according to the common dToF ranging procedure shown in Figure~\ref{img:sensorapi}. All functions require a pointer to a sensor wrapper \verb|Sensor_t| as the first parameter.

\begin{figure}[H]
    \centering
    \caption{Sensor API workflow}
    \label{img:sensorapi}
    \includegraphics[width=0.8\textwidth]{img/sensorapi}
\end{figure}

\begin{itemize}[style=nextline]
    \item \mintinline[breaklines, breakafter=dev,]{c}{void sensor_init ( Sensor_t* sensor, uint8_t id, uint8_t dev,} \\
        \mintinline{c}{                   uint16_t xshut_pin, uint16_t xshut_port,}\\
        \mintinline{c}{                   SensorRangingParams_t* params);}
    \begin{itemize}
        \item Initializes the sensor wrapper with values passed as parameters and guarantees correct behavior regardless of the backend driver. It is not recommended to initialize \verb|sensor| by assigning values to individual members.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_connect ( Sensor_t* sensor_array, const int cnt );}
    \begin{itemize}
        \item Here, the array of sensors and the number of sensors in the array are expected. Consider that one of the sensors needs to be initialized and that all of the sensors have the same I$^2$C address after boot. The \verb|sensor_connect| then must be able to disable the rest of the uninitialized sensors so that the command that changes the I$^C$C address is accepted only by a single sensor. When this function ends, all initialized sensors will have a desired address, their state will be set to \verb|SENSOR_STATE_IDLE|, and ranging parameters will be applied.  
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_disable ( Sensor_t* sensor );}
    \begin{itemize}
        \item Disables the sensor with an XSHUT pin set to low and waits 50 ms.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_enable ( Sensor_t* sensor );}
    \begin{itemize}
        \item Enables the sensor with an XSHUT pin set to high and waits 50 ms to give it time to enter SW standby.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_reset ( Sensor_t* sensor );}
    \begin{itemize}
        \item The shorthand for \verb|sensor_disable| and \verb|sensor_enable| called in succession
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_start_ranging ( Sensor_t* sensor );}
    \begin{itemize}
        \item Starts ranging and sets the sensor state to \verb|SENSOR_STATE_RANGING|
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_stop_ranging ( Sensor_t* sensor );}
    \begin{itemize}
        \item Stops ranging and sets the sensor state to \verb|SENSOR_STATE_IDLE|
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_toggle_ranging ( Sensor_t* sensor );}
    \begin{itemize}
        \item Ranging is stopped if the sensor state is \verb|SENSOR_STATE_RANGING| and started otherwise.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{SensorRet_t sensor_read_result ( Sensor_t* sensor, SensorResult_t* result);}
    \begin{itemize}
        \item Reads the measured data into a \verb|result| parameter. The return type is an \mintinline{c}{enum} with error states. If the data are not ready or are not valid, the \verb|SENSOR_RET_ERR| value is returned. The \verb|SENSOR_RET_OK| is returned otherwise. This can be useful if an interrupt cannot be used and polling is preferable.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{void sensor_clear_interrupt ( Sensor_t* sensor );}
    \begin{itemize}
        \item Clears the interrupt and resumes ranging.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{uint16_t sensor_id ( Sensor_t* sensor );}
    \begin{itemize}
        \item If the sensor is connected and working, this function should always return \verb|0xEBAA|, even before any initialization is performed.
    \end{itemize} 
    \item \mintinline[breaklines]{c}{uint8_t sensor_isconnected ( Sensor_t* sensor );}
    \begin{itemize}
        \item Checks if the sensor is connected and if the initialization with \verb|sensor_connect| was performed
    \end{itemize} 
\end{itemize}

\subsection{Result type}

\begin{listing}[h]
    \caption{Type representing a sensor data}
    \label{code:sensorresult}
    \begin{minted}{c}
typedef struct
{
	int32_t max; ///<Maximum value in histogram
	int32_t sum; ///<Sum of histogram values
	int32_t bins; ///<Number of used bins
	int32_t hist[SENSOR_HIST_SIZE]; ///<Histogram data
} SceneData_t;

typedef struct
{
	uint32_t distance_mm; ///<Distance
	uint32_t signal_rate_kcps; ///<Signal rate
	uint32_t ambient_rate_kcps; ///<Ambient rate
	uint32_t number_of_spad; ///<Number of SPADs
	uint32_t sigma_mm; ///<Sigma (standard deviation)
	SceneData_t scene; ///<Histogram of the scene
} SensorResult_t;
     \end{minted}
 \end{listing}

The data from sensors are stored into a \verb|SensorResult_t| type shown in Code Listing~\ref{code:sensorresult}. The distance, signal rate, and ambient rate are self-explanatory, but both drivers also report two other interesting metrics. The first is the number of SPADs that were used for the measurement. This number will change with the intensity of the signal and closer targets will require less active SPADs. Sigma refers to a typical notation of standard deviation and its purpose is to describe how precise is the measured distance. Lower values mean that the confidence in a measured value is higher.

The histogram is part of the \verb|scene| member that also stores a number of bins and simple metrics that can be easily calculated during the histogram readout from the LX driver and could be useful for the detection algorithm. For now, only a maximum value and summation of bins are calculated, but in the future this might change. If the CD driver is used, the number of bins is always zero, indicating that the histogram is not available. 

\section{Message protocol and command interface}

To communicate with some other device via the UART, some protocol must be agreed upon. The most influential choice is whether to convert data to strings and send them as ASCII characters or whether they should be transmitted in their binary form. The advantage of the first approach is that the communication is easier to read and understand by the human, even if all they have is a data dump captured by a logic analyzer or an oscilloscope. The downside is that the conversion to characters creates a lot of redundancy and is harder to parse by a machine on the receiving end. Moreover, if data are sent with as little overhead as possible in binary form, faster communication speeds can be achieved. In practice, both approaches are used in this project. The seldom debug and error messages are transmitted as strings because they must be understandable to the user under any circumstances. The compact implementation of \verb|printf| is used for this purpose, because it is a useful and familiar interface for C programmers. Every \verb|printf| call blocks until the full string is transmitted so that fast subsequent calls do not result in data loss due to buffer rewrites. 

Because of the way the UART driver provided by SPCStudio is implemented, receiving the data into the application has more caveats and the binary approach made more sense. It was already mentioned that to read data, a request to UART peripheral is made and an interrupt is invoked when the transfer is complete. The key part of the request is the number of bytes to read, which means that at any point, the size of the incoming message must be known in advance. This is a frustrating requirement for any text-based protocol, so the decision was made to control the firmware exclusively through a graphical application that will handle the communication in binary form.

\begin{figure}
    \caption{Structure of the message frame}
    \centering
    \label{img:frame}
    \begin{tikzpicture}
    \definecolor{darkblue}{RGB}{3, 35, 75}
    \definecolor{lightblue}{RGB}{60, 180, 230}
    \definecolor{darkgreen}{RGB}{0, 128, 0}
    \tikzset{
        frame/.style={rectangle split, rectangle split parts=2, draw=lightblue, fill=lightblue!5, thick,align=center},
        header/.style={rectangle split, rectangle split parts=2, draw=darkgreen, fill=darkgreen!5, thick,align=center},
        body/.style={rectangle split, rectangle split parts=2, draw=red!60, fill=red!5, thick,align=center}
    }
    %Nodes
    \node[frame] (header) {\strut\textbf{Header start} \nodepart{two} \strut\verb|0x2A|};
    \node[header]  (cmd)       [right=0 cm of header] {\strut\textbf{Command} \nodepart{two} \strut 1 byte};
    \node[header]  (size)       [right=0 cm of cmd] {\strut\textbf{Data size} \nodepart{two} \strut 1 byte};
    \node[frame]  (end1)       [right=0 cm of size] {\strut\textbf{Stop} \nodepart{two} \strut\verb|0x2D|};
    \node[frame]  (datas)       [right=0 cm of end1] {\strut\textbf{Data start} \nodepart{two} \strut\verb|0x2B|};
    \node[body]  (data)       [right=0 cm of datas] {\strut\textbf{Data} \nodepart{two} \strut\textit{Data size} bytes};
    \node[frame]  (end2)       [right=0 cm of data] {\strut\textbf{Stop} \nodepart{two} \strut\verb|0x2D|};

    \end{tikzpicture}
\end{figure}

Figure~\ref{img:frame} shows the structure of a single frame according to a simple binary protocol that was implemented for the purpose of this application. Every frame is split into a header and data. Each part separately must contain a correct start and end byte; otherwise, the message is deemed invalid. The header and data need a different start byte, but the end byte is the same for both. The header contains two bytes of useful information: the command byte and the data length. If the target device supports the required command, the header will be acknowledged with a \verb|0x41| byte. The response with a \verb|0x45| byte will be used if the command is not recognized. The data portion of the message may only be sent after the valid acknowledge byte is received to ensure that the next UART read request has already started and that the data will not be lost. The maximum data length is limited to 255 bytes, which is good enough to transport a full histogram for a single sensor, but in hindsight, it would probably have been better to use at least 16 bit integer to allow larger messages. 

When the full message is received, the device will send a response in the same format. The header will contain the command byte of the incoming message, and the data length will correspond to the size of the data produced by the command. The only difference is that there is no waiting for acknowledge after the header and the data are sent immediately, because the GUI buffers incomming data and the confirmation is not necessary. 

Commands themselves are defined as functions that share a prototype from Code Listing~\ref{code:cmd}. One reason for this is to separate them from the parsing logic so that the same command function can be used with a different protocol or even peripheral. This would be important if in the future the CAN bus was used as another input channel. Therefore, each command only requires a pointer to the input data, the size of the input, and a pointer to the buffer where the command output will be stored. The size of the output is passed to the caller through a return value. Each command assumes that the output buffer is allocated and is sufficiently large. In practice, the \mintinline{c}{uint8_t msg_out_buffer[MSG_MAX_LEN]} buffer that can accommodate the largest 255-byte message is always used by the message parser that calls the command function. 

\begin{table}
    \centering
    \caption{Table of implemented commands}
    \label{tab:cmd}
     \begin{tabular}{c c l p{4cm}} 
     \tabhead{Name} & \makecell[c]{\tabhead{Command}\\\tabhead{byte}} & \makecell[c]{\tabhead{Params (bytes)}} & \makecell[c]{\tabhead{Response (bytes)}} \\
     \hline \hline
     \verb|cmd_read_sensor| & 'A' \verb|0x41| & 
        \begin{tabular}{c @{\hspace*{\the\fontdimen2\font\space }} l}
            \textit{1:} & Port number \\ 
        \end{tabular}
     & \makecell[cl]{
     \textit{4:} Distance (mm)
     \\\textit{4:} Ambient rate (kcp/s)
     \\\textit{4:} Signal rate (kcp/s)
     \\\textit{4:} Number of SPADs
     \\\textit{4:} Sigma (mm)
     }\\
     \multicolumn{4}{ p{0.8\textwidth} }{Reads the last captured measurement. Items in the response are encoded as \mintinline{c}{uint32_t}. If the port is not active, the response will be \textit{Empty}.}\\
     \hline
     \verb|cmd_set_evalmode| & 'D' \verb|0x44| & 
     \begin{tabular}{c @{\hspace*{\the\fontdimen2\font\space }} l}
            \textit{1:} & \verb|0x0| onboard \\ & \verb|0x1| remote 
        \end{tabular}
     & \makecell[cl]{
     \textit{Empty}
     }\\
     \multicolumn{4}{l}{Sets the evaluation mode.}\\
     \hline
     \verb|cmd_set_eval| & 'E' \verb|0x45| & 
        \begin{tabular}{c @{\hspace*{\the\fontdimen2\font\space }} l}
            \textit{1:} & \verb|0x0| empty \\ & \verb|0x1| object \\ & \verb|0x2| fake object 
        \end{tabular}
     & \makecell[cl]{
     \textit{Empty}
     }\\
     \multicolumn{4}{l}{Sends remote evaluation result. Has no effect if the evaluation mode is set to \textit{onboard}.}\\
     \hline
     \verb|cmd_read_hist| & 'H' \verb|0x48| & 
     \begin{tabular}{c @{\hspace*{\the\fontdimen2\font\space }} l}
            \textit{1:} & Port number \\ 
        \end{tabular}
     & \makecell[cl]{
     \textit{96:} 24 histogram bins
     }\\
     \multicolumn{4}{ p{0.9\textwidth} }{Reads the last captured histogram. Each bin is encoded as \mintinline{c}{uint32_t}. If the port is not active or the histogram is not available, the response will be \textit{Empty}.}\\
     \hline
     \verb|cmd_set_dist| & 'S' \verb|0x53| & 
     \begin{tabular}{c @{\hspace*{\the\fontdimen2\font\space }} l}
            \textit{1:} & \verb|0x0| read \\ & \verb|0x1| write \\
            \textit{2:} & Distance (mm) \\
            & only if \textit{write}\\
        \end{tabular}
     & \makecell[cl]{
     \textit{2:} Distance (mm)\\
     \\
     or\\
     \\
     \textit{Empty} if \textit{write}
     }\\
     \multicolumn{4}{ p{0.9\textwidth} }{Can be used to either read the current distance treshold for the \textit{onboard} algorithm, or to set a new one and store it into persistent memory. Distance is encoded as \mintinline{c}{uint16_t}.}\\
     \hline
     \end{tabular}
\end{table}

\begin{listing}[b]
    \caption{Command prototype}
    \label{code:cmd}
    \begin{minted}{c}
    typedef uint8_t (*CommandFunc_t)(uint8_t* in, const uint8_t in_len, 
                                        uint8_t* out);
     \end{minted}
 \end{listing}

Another advantage of having commands that share the same prototype is that they can be easily accessed by function pointers. The pointer to every command is stored in the \mintinline{c}{cmd_links} array and every position in this command table corresponds to a single command byte. Therefore, the message parser simply calls the function stored in the correct position when the full message is received. This process should be faster than the comparison of command bytes, and the code is also easier to maintain because, to add a new command, the programmer simply writes the function with a correct prototype and adds its pointer to a free position in the command table. The index in the command table is not the same as the command byte, and an offset is applied so that the first position matches the capital 'A' according to ASCII table. This means that every command byte is a printable character, which can simplify debugging. The full list of commands currently implemented is in Table~\ref{tab:cmd}.

\section{ARGB LED}

\begin{figure}[hb]
    \hfill
    \begin{minipage}[t]{.4\linewidth}
      \centering
      \caption{WS2812B protocol}
      \label{img:ws2812b}
      \includegraphics[width=0.6\textwidth]{img/ws2812b}
    \end{minipage}
    \hspace*{2mm}
    \begin{minipage}[t]{.5\linewidth}
      \centering
      \captionof{table}{WS2182B timing requirements}
      \label{tab:ws2812-time}
      \begin{tabular}{c c c} 
        \tabhead{Interval} & \tabhead{Period} & \tabhead{Tolerance}\\
        TxH + TxL & 1.25 {\textmu}s & $\pm600$ ns \\
        T0H & 0.4 {\textmu}s & $\pm150$ ns \\
        T0L & 0.85 {\textmu}s & $\pm150$ ns \\
        T1H & 0.8 {\textmu}s & $\pm150$ ns \\
        T1L & 0.45 {\textmu}s & $\pm150$ ns \\
        TReset & $\ge50$ {\textmu}s & --- \\
        \end{tabular}
    \end{minipage}
    \hfill
  \end{figure}

The WS2812B LED accepts information about color as a series of bits encoded according to Figure~\ref{img:ws2812b}. Each bit has a fixed period and always starts with high level and then goes to low level. If the duration of high level is longer than the duration of low level, the bit has value 1, and if it is shorter, the bit is 0. The exact ratio of high to low is specified in the datasheet for each bit value, as well as the timing tolerance. Each LED requires 24 uninterrupted bits that carry 8-bit brightness values for green, red, and blue colors (in this exact order). Pulling the input line low for at least 50 {\textmu}s activates the color as the received bits are loaded into a driving circuit.

To demonstrate the full control sequence, consider a configuration with two daisy-chained LEDs. The input buffer of WS2812B acts like a shift register, and after the first 24-bit frame is received and another is sent in succession (without a reset signal in between them), it will start shifting the received bits to the next LED in chain. Therefore, the first 24 bits will end up in the second LED, and at that point, the reset signal may be used to apply the color and finish the sequence. As a consequence, even when only one LED is required to change color, the color of all preceding LEDs in the chain must also be rebroadcast, and so the MCU must store the current status of the daisy chain in a buffer.

The protocol could be implemented with software control of the GPIO pin, but the more optimal and less resource-intensive way was to utilize the SPI peripheral. SPI traditionally requires four signals: clock (SCK), data in (SIN), data out (SOUT), and chip select (CS). The signal that is connected to the data input of the LED is the SOUT -- SIN is of no use because no data is coming from the LED and CS is only useful in real SPI communication where there is a need to specify target device. The data sent over the SPI are organized into n-bit frames and the duration of each bit is determined by the frequency of the SCK. 

The idea is to set the clock frequency so that the duration of a single frame is the same as the duration of one bit in the LED protocol. Then the data inside the frame can be set so that it begins with a series of ones followed by zeros. The number of ones in the frame will control the length of the high pulse and thus determine whether the LED receives one or zero bit. The SPI was configured to use 8-bit frames, because they are convenient to work with, and the clock frequency was set so that one frame has a period of 1.6 ns, which is within the specifications of WS2812B. As a consequence of these settings, a single bit inside the SPI frame controls the value of SOUT for 0.2 ns. The optimal frame values were chosen to be as close as possible to the T0H and T1H values in the datasheet: \verb|0xf8| frame for 1 and \verb|0xc0| for 0. 

Unfortunately, the SPI approach had one problem -- the SOUT line is pulled high by default. Naturally, it was expected that it can be controlled with zero frames, but this did not work because even if two zero frames were sent in succession, a short voltage spike always appeared on the SOUT. Its value was roughly half of the VDD and this was enough to be detected by the LED as a positive edge. This meant that it was impossible to create a long enough reset signal. This behavior seems to be a software or hardware bug, but it was fortunately possible to circumvent it thanks to the ability of each GPIO pin to invert the output value. The frames must also be inverted to correct for inversion at output, but now, when the SPI is idle, the SOUT will be low and it just must be ensured with a blocking wait that the next transaction will not start until the reset period ends. The measurements from the oscilloscope are shown in Figure~\ref{img:rgb-osc} and highlight the problematic behavior of the SPI peripheral. It was also verified that the timings are correct and in accordance with the SPI settings. 

\begin{figure}
    \centering
    \caption{Oscilloscope captures of the WS2812B data line}
    \label{img:rgb-osc}
    \begin{subfigure}[t]{0.9\textwidth}
        \includegraphics[width=\textwidth]{img/spi-bug.png}
        \subcaption{Reset signal generated by a series of zero frames. Brief voltage spikes that interrupt the reset sequence may be observed. }
    \end{subfigure}

    \vspace*{10mm}

    \begin{subfigure}[t]{0.9\textwidth}
        \includegraphics[width=\textwidth]{img/spi-inv.png}
        \subcaption{Valid reset sequence that was obtained after the output pin values were inverted. The reset duration was measured to be 63.6 {\textmu}s between two subsequent color changes.}
    \end{subfigure}
\end{figure}

In the end, the LED control was fully operational. A code was divided into an API and platform functions that control the SPI. Some colors were named and are part of the \mintinline{c}{enum Color_t} type. Only the first 24 bits are used, and the order of colors is red-green-blue, which is the typical order in which the colors are specified in HTML and other common desktop software. The correct order for the WS2812B is achieved later in a platform function that fills the new color of the LED into an SPI buffer.

\begin{listing}
    \caption{Procedure that changes one color in the ARGB buffer}
    \label{code:argb-buffer}
    \begin{minted}{c}
    void _argb_set (const uint8_t num, const Color_t color)
    {
        unsigned long color_cp = color;
        int idx = (ARGB_NUM_LEDS - num - 1) * ARGB_FRAME_SIZE 
                    + ARGB_FRAME_SIZE - 1;

        for (int color = 2; color >= 0; color--) //Starts from LSB
        {
            int color_offset = (2 - argb_order[color]) * 8;
            for (int i = color_offset; i < color_offset + 8; i++)
            {
                if (color_cp & 1) argb_led[idx - i] = ARGB_HIGH;
                else argb_led[idx - i] = ARGB_LOW;
                color_cp >>= 1;
            }
        }
    }
     \end{minted}
 \end{listing}
