\chapter{Testing and evaluation}

With the full setup working, it first was verified that the sensors do not interfere with each other. This was done with the help of two IR LEDs connected to oscilloscope probes and placed near each sensor. Both halves of the area were isolated and the VCSEL activation times were captured. The results show that there is no crosstalk and that only one of the VCSELs is active at a time, which is an expected behavior. The pictures of the measurement results the setup with IR diode are shown in Figures~\ref{img:xtalk} and \ref{img:xtalk-ir}, respectively.

\begin{figure}[hb]
    \centering
    \caption{VCSEL activation sequence}
    \label{img:xtalk}
    \includegraphics[width=0.9\textwidth]{img/xtalk}
\end{figure}

\begin{figure}
    \centering
    \caption{IR diode placed in front of a sensor}
    \label{img:xtalk-ir}
    \includegraphics[width=0.7\textwidth]{img/xtalk-ir}
\end{figure}

The object detection capabilities of the device were tested with the algorithm described in Section~\ref{h:gui-alg}. Therefore, the calculations were performed externally in the desktop application, because that version of the algorithm was more sofisticated than the simple distance threshold approach implemented in the firmware of the MCU. It also better represented the current direction for future development. The list of objects used during the evaluation was specified in the Chapter~\ref{h:requirements}. It must only be added that the black and white cubes that represent the opposite ends of the reflectivity spectrum were 3D printed and both had dimensions $10 \times 10 \times 15$~mm. In addition, the parking ticket was emulated with a piece of white office paper.

Each item was placed in distinct spots in the area (depending on its size), and it was noted whether the item was reliably detected or not. When detection was not convincing and the state frequently changed to an empty scene, the position was counted as a failure. The experiment was carried out in a professional SpectraLight~QC light box with illumination set to TL84 standardized light with a color temperature of 4100 K. This light was set because it has a low amount of IR component, and therefore the effect of ambient light should be minimized. The enclosure was configured so that the center of the sensors would be 5.7 mm from the glass and 11 mm horizontally from the closest sidewall. This was the lowest possible placement achievable with the current sensor holder design. It was chosen because the lower positions are naturally more resistant to false detections of objects behind the scene. Slight 1.2\textdegree~tilt towards the glass surface was applied for the same reason and the horizontal angle was set to 4\textdegree~inwards.

The results are shown in Figure~\ref{img:eval} where each image represents the 
map of a physical area protected by sensors. The area was divided into $10 \times 10$ mm squares according to the size of the black and white test cubes. The cubes were tested in each square, but other larger and less uniform objects usually took more than one square at a time, and their placement was less precise. The credit card could not even fully fit inside the area, so it was tested partially inserted with one side extending upward and also lying on the side walls. Fortunately, it was not necessary to figure out how to convert these positions to squares in the diagram because the credit card and other large objects were always detected regardless of the orientation. Of the base set of objects, only the coin was incorrectly detected in corners that are in a blind spot of the nearest sensor and furthest from the sensor on the opposite side. The reason is that the selected 1~cent is very small and simply does not stand out enough in the signal returned by the nearby wall. 

\begin{figure}[ht]
    \centering
    \caption{SpectraLight QC Lightbox}
    \label{img:lightbox}
    \includegraphics[width=0.7\textwidth]{img/lightbox}
\end{figure}

The coverage of the black cube was even worse than that of a coin, even though it was physically larger. Its reflectivity was so low that, in some positions, the sensor did not even capture any difference compared to an empty scene. This was a troubling discovery because if some dark objects do not affect the histogram in any way, then it is simply not possible to detect them using this setup. On the other hand, the coverage of the white cube is almost 100\% which emphasizes that the volume of the object is less important than the material and color. Another interesting behavior was that sometimes the histogram bins actually went down with the black cube present. The change was significant and consistent, but it is debatable whether the machine could correctly recognize it.
Especially when other, less apparent issues also need to be considered by the algorithm.

One of them is the dust build-up that happens naturally in almost any environment. Unfortunately, even a small amount of dust results in a histogram that could easily be interpreted as belonging to one of the smaller objects. The only assumption that can be made about the dust is that it is uniform and that it should theoretically affect the histogram in some predictable way. However, implementation of any form of dust correction would probably result in an overall worse object detection because the signature of smaller objects shrinks as the dust increases, and any additional filtering is unlikely to make it stand out. There is also a glaring issue with the original premise, because any clean-up of the dust will disturb its uniformity and the system will be very frustrating for the user, since any imperfectly cleaned spot would probably be detected as an object.

The next issue that is not apparent from the results is the detection of fake objects. In some cases, this could be considered a feature because it can provide an early warning to a driver that something is likely to fall somewhere that is not supposed to be. But for this to work, the system must be able to distinguish between this and the situation where the object already violates the rules. However, the problem is that the histogram produced by the sensor is not fine enough and, as a result, the entire protected area is covered only by four bins, with the last bin inconveniently affected by objects behind the frame as well. Maybe, if the dimensions of the protected area were more in line with the spacing of the bins, the boundary could be more easily isolated. The biggest obstacle to this solution will likely be the everpresent multipath reflections.

Another problematic variable is temperature, because in an automotive environment, the system has to be reliable across a wide temperature range. During the experiment in the light box, the temperature was not controlled, but later the system was placed in a temperature chamber and the signal rate was observed. The results in Figure~\ref{img:temp} show that the sensor output changes drastically with temperature. This is a known fact, and the dToF sensor even has a temperature calibration feature that should solve this problem. According to the manual, temperature only adds a predictable offset to the signal that can be isolated, but this means that temperature must be monitored so that the calibration runs at the right time~\cite{um:uld-manual}. Furthermore, it must be tested how the calibration affects the object detection algorithm. Currently, the assumption is that changes caused by a small temperature drift may be difficult to detect and that the detection sensitivity will have to be reduced to avoid false positives. Even during the object detection test, the histogram of one of the sensors changed over time and a new reference for an empty scene had to be set frequently. This cannot be attributed solely to temperature, but it is definitely one of the likely causes.

\begin{figure}[t]
    \centering
    \caption{Signal rate dependency on temperature}
    \label{img:temp}
    \includegraphics[width=0.8\textwidth]{img/temp.pdf}
\end{figure}

Finally, it is apparent from both object detection and temperature testing that each sensor behaves differently under the same conditions. This production spread is significant and was the main reason why the approach was to measure a reference and detect changes to it. It would be possible to calibrate the system during production in a similar matter, but after that it needs to function without any intervention under most conditions. The differences between sensors also make it difficult to take advantage of symmetric arrangments where two or more sensors share a part of the FoV, and the comparison between their outputs could be another useful indicator of object presence. 


\begin{figure}[tb]
    \centering
    \caption{Coverage testing for the histogram algorithm implemented in a GUI application}\label{img:eval}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-parkingticket.pdf}
        %\subcaption{Parking ticket}
    \end{subfigure}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-card.pdf}
        %\subcaption{Credit card}
    \end{subfigure}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-pen.pdf}
    \end{subfigure}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-coin.pdf}
    \end{subfigure}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-blackcube.pdf}
    \end{subfigure}
    \begin{subfigure}[c]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/eval-whitecube.pdf}
    \end{subfigure}
\end{figure}

\begin{figure}[htb]
    \centering
    \caption{Photos of test objects}
    \label{img:evalp}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-parkingticket.JPG}
        \subcaption{Parking ticket (emulated with a white paper)}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-card.JPG}
        \subcaption{Credit card}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-pen.JPG}
        \subcaption{Pen}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-coin.JPG}
        \subcaption{Coin}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-whitecube.JPG}
        \subcaption{White cube}
    \end{subfigure}
    \begin{subfigure}[c]{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/evalp-blackcube.JPG}
        \subcaption{Black cube}
    \end{subfigure}
\end{figure}