var struct_sensor__t =
[
    [ "addr", "struct_sensor__t.html#af5105c72a61e60d3b44670de20e0c3fb", null ],
    [ "dev", "struct_sensor__t.html#a6c3b2d92814052906aa7ab7207932719", null ],
    [ "id", "struct_sensor__t.html#a1e6927fa1486224044e568f9c370519b", null ],
    [ "lx", "struct_sensor__t.html#a3c2ef78b22c76d5e91ec08f32b93ed5b", null ],
    [ "params", "struct_sensor__t.html#affea76778ed25a5678caa2dae865ec67", null ],
    [ "state", "struct_sensor__t.html#a12a5481f6a024c944a702c69a4efa00b", null ],
    [ "xshut_pin", "struct_sensor__t.html#a34734b4ed98ccb156bb87443bc720ad1", null ],
    [ "xshut_port", "struct_sensor__t.html#a8128ad237527401e106bbc943ca07600", null ]
];