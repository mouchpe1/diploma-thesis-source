var annotated_dup =
[
    [ "Board_t", "struct_board__t.html", "struct_board__t" ],
    [ "EvalResult_t", "struct_eval_result__t.html", "struct_eval_result__t" ],
    [ "Measurement_t", "struct_measurement__t.html", "struct_measurement__t" ],
    [ "SceneData_t", "struct_scene_data__t.html", "struct_scene_data__t" ],
    [ "Sensor_t", "struct_sensor__t.html", "struct_sensor__t" ],
    [ "SensorRangingParams_t", "struct_sensor_ranging_params__t.html", "struct_sensor_ranging_params__t" ],
    [ "SensorResult_t", "struct_sensor_result__t.html", "struct_sensor_result__t" ],
    [ "Setting_t", "struct_setting__t.html", "struct_setting__t" ],
    [ "VL53L4CD_ResultsData_t", "struct_v_l53_l4_c_d___results_data__t.html", "struct_v_l53_l4_c_d___results_data__t" ],
    [ "VL53L4CD_Version_t", "struct_v_l53_l4_c_d___version__t.html", "struct_v_l53_l4_c_d___version__t" ]
];