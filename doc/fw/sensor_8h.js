var sensor_8h =
[
    [ "SceneData_t", "struct_scene_data__t.html", "struct_scene_data__t" ],
    [ "SensorResult_t", "struct_sensor_result__t.html", "struct_sensor_result__t" ],
    [ "SensorRangingParams_t", "struct_sensor_ranging_params__t.html", "struct_sensor_ranging_params__t" ],
    [ "Sensor_t", "struct_sensor__t.html", "struct_sensor__t" ],
    [ "SENSOR_HIST_AMBIENT", "sensor_8h.html#ac2122e1eb8e90d182236bf63f19546e8", null ],
    [ "SENSOR_HIST_SIZE", "sensor_8h.html#ab21b1eced3cf6ea89fc96be89c9d24e3", null ],
    [ "SENSOR_I2C_ADDRESS", "sensor_8h.html#a2e976105f0b127c43dea3134d3ebdc12", null ],
    [ "SENSOR_USE_LXDRIVER", "sensor_8h.html#a556b18f968f5a5baa6b227998e3ff75d", null ],
    [ "SensorRet_t", "sensor_8h.html#aaf6077aa54a0d949c03c9be2f08886da", [
      [ "SENSOR_RET_OK", "sensor_8h.html#aaf6077aa54a0d949c03c9be2f08886daada2f4ef460fcff44085d81ade2120056", null ],
      [ "SENSOR_RET_ERR", "sensor_8h.html#aaf6077aa54a0d949c03c9be2f08886daaa0dd3486db378d96889b9a0e9902aa76", null ]
    ] ],
    [ "SensorState_t", "sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09", [
      [ "SENSOR_STATE_DISCONNECTED", "sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09aa96aa505c5ab8bb7f598c56da275581f", null ],
      [ "SENSOR_STATE_IDLE", "sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09afcebd4e5455b500926aa210b31a5cd84", null ],
      [ "SENSOR_STATE_RANGING", "sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09a5ca4fec37d244f678c68d48fd676919e", null ],
      [ "SENSOR_STATE_ERROR", "sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09a926a047d10a2309d2e7ea68e318a2d9a", null ]
    ] ],
    [ "sensor_clear_interrupt", "sensor_8h.html#aab576d8a16b618cb00b9c74a03265380", null ],
    [ "sensor_connect", "sensor_8h.html#a0b8b35fa2cb1fb04e91f761afb020113", null ],
    [ "sensor_disable", "sensor_8h.html#a435e169c2d2e8113bb67e6e6ecb6de1c", null ],
    [ "sensor_enable", "sensor_8h.html#a8db346230bea5ca8a1dfc41166f06504", null ],
    [ "sensor_id", "sensor_8h.html#a31ac4e6c92dfedce3f9d886f2c7527f7", null ],
    [ "sensor_init", "sensor_8h.html#a147e4fcffd0e331b6765a5b1765373e2", null ],
    [ "sensor_isconnected", "sensor_8h.html#abaa4bd2791eb72d4faad57fdadd75d70", null ],
    [ "sensor_read_result", "sensor_8h.html#ac95ffd1cf2ed6357578083bec13b6902", null ],
    [ "sensor_reset", "sensor_8h.html#ac4bdea3c8aaebbd9f3047551dd8c44e2", null ],
    [ "sensor_start_ranging", "sensor_8h.html#aa1271250e7a48b0bbfc939cad93c1902", null ],
    [ "sensor_stop_ranging", "sensor_8h.html#a8d8652ac9c672116a21b7c18894ca01a", null ],
    [ "sensor_toggle_ranging", "sensor_8h.html#a8216725c264fea7fbc874181c0f951f1", null ]
];