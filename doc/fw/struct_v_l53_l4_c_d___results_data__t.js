var struct_v_l53_l4_c_d___results_data__t =
[
    [ "ambient_per_spad_kcps", "struct_v_l53_l4_c_d___results_data__t.html#a9498c581619854f81322310e4f67936b", null ],
    [ "ambient_rate_kcps", "struct_v_l53_l4_c_d___results_data__t.html#a8eee8daf28772b64f79f850b7537613e", null ],
    [ "distance_mm", "struct_v_l53_l4_c_d___results_data__t.html#a780fee760b6a8c05845a55634fd1d7d0", null ],
    [ "number_of_spad", "struct_v_l53_l4_c_d___results_data__t.html#aacc249e9719508a2813a02169f0715fb", null ],
    [ "range_status", "struct_v_l53_l4_c_d___results_data__t.html#ad4afdf20896b44c79318589612dd9aa5", null ],
    [ "sigma_mm", "struct_v_l53_l4_c_d___results_data__t.html#a84836df409b965d315060fa80d1abb5c", null ],
    [ "signal_per_spad_kcps", "struct_v_l53_l4_c_d___results_data__t.html#a4536ad17588b66adc9cfaa95df4efd77", null ],
    [ "signal_rate_kcps", "struct_v_l53_l4_c_d___results_data__t.html#aaf2805dfb6224bc09a13d2ef1e8d5fb7", null ]
];