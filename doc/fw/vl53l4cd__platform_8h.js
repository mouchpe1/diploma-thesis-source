var vl53l4cd__platform_8h =
[
    [ "Dev_t", "vl53l4cd__platform_8h.html#a4720fc2317be91590dfc25d169dc1c64", null ],
    [ "VL53L4CD_Error", "vl53l4cd__platform_8h.html#a155a3d8f2816a8af1ae29f1c0052ace4", null ],
    [ "VL53L4CD_RdByte", "vl53l4cd__platform_8h.html#a070eeb8d36b24e64a155d11e166837c8", null ],
    [ "VL53L4CD_RdDWord", "vl53l4cd__platform_8h.html#adaf3e6bb326f74c79f5085fb3fc4048f", null ],
    [ "VL53L4CD_RdWord", "vl53l4cd__platform_8h.html#abf795668944172026086755030b415be", null ],
    [ "VL53L4CD_WrByte", "vl53l4cd__platform_8h.html#a7368feb012679711d625d34d6474c3bc", null ],
    [ "VL53L4CD_WrDWord", "vl53l4cd__platform_8h.html#ad3740d5e210bc8ca60f7b9b613266b3d", null ],
    [ "VL53L4CD_WrWord", "vl53l4cd__platform_8h.html#a39a80551a0de082fd44e728314382049", null ],
    [ "WaitMs", "vl53l4cd__platform_8h.html#a007775a4f6a9bebeb536d9b59d72ef12", null ]
];