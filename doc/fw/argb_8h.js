var argb_8h =
[
    [ "Color_t", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fd", [
      [ "COLOR_BLACK", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda2a9daf215a30f1c539ead18c66380fc1", null ],
      [ "COLOR_RED", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda592503b9434c1e751a92f3fc536d7950", null ],
      [ "COLOR_GREEN", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdacfa9d8bbffc418447ed826f286abca02", null ],
      [ "COLOR_BLUE", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda1340428efccb140dcbdb71aa6176f696", null ],
      [ "COLOR_PURPLE", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda278276a8155620ed5e155c80d35316cc", null ],
      [ "COLOR_YELLOW", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdab03862907066c68204ee9df1ee04aa29", null ],
      [ "COLOR_CHARTREUSE", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdaec51a468b36d94482fef8e32f579c538", null ],
      [ "COLOR_PINK", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda1b93c096fbb90304018ed9977dc1dadc", null ],
      [ "COLOR_HOTPINK", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdae1e5d3716cbf2ed97062af7c3380eb1e", null ],
      [ "COLOR_AQUA", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda3fbd5be8e43fa2c705c409c188b33bc9", null ],
      [ "COLOR_NAVY", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdacaed5fc877f6ff2fc2ea905b8753c118", null ],
      [ "COLOR_DARKCYAN", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda7f2bf085b70769d2cc5029dcc640c954", null ],
      [ "COLOR_DARKGREEN", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdacd9ad0935617fa571ae6dd998e626959", null ],
      [ "COLOR_BLUEVIOLET", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdaa0ae13299dfaee588c40919cb1e3b703", null ],
      [ "COLOR_AMETHYST", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fda887842aa50b12f4ff51052d505393113", null ],
      [ "COLOR_WHITE", "argb_8h.html#aac08c9c4b63f4dfa095c03fe6ecca1fdad47b4c240a0109970bb2a7fe3a07d3ec", null ]
    ] ],
    [ "_argb_get", "argb_8h.html#a485b41c32f0a9be8168c37734181c12c", null ],
    [ "_argb_set", "argb_8h.html#aa6a6695b8e2f3d358fb495b8ef2af213", null ],
    [ "_argb_update", "argb_8h.html#a4880477b615793fd1a4a6d24a4c7854d", null ],
    [ "argb_clear", "argb_8h.html#a1bc5f6679a09c256827bb8e66d8fa5b0", null ],
    [ "argb_set", "argb_8h.html#a6578e867859e1a85ff67c91aa01054b7", null ],
    [ "argb_toggle", "argb_8h.html#a11bde4bf6f682bf7eff0173cb4217ae0", null ],
    [ "argb_toggle2", "argb_8h.html#aa54844953168c3f5ebdee8289630d925", null ]
];