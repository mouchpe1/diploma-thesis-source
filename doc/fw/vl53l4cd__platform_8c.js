var vl53l4cd__platform_8c =
[
    [ "VL53L4CD_RdByte", "vl53l4cd__platform_8c.html#a3524203554f65b6c7be00b82e6d81476", null ],
    [ "VL53L4CD_RdDWord", "vl53l4cd__platform_8c.html#a086cc59f8115fc38599da19121046aa0", null ],
    [ "VL53L4CD_RdWord", "vl53l4cd__platform_8c.html#ac72fa06e19cc68072975f1cc5ffc676b", null ],
    [ "VL53L4CD_ResolveError", "vl53l4cd__platform_8c.html#a60c2ee0b2aa1cf3de26e0dbfeb5692b9", null ],
    [ "VL53L4CD_WrByte", "vl53l4cd__platform_8c.html#a211276a84b5a95a4032f3b0a7bd92018", null ],
    [ "VL53L4CD_WrDWord", "vl53l4cd__platform_8c.html#ae20e77b3e028e8126cccc34cbe94af31", null ],
    [ "VL53L4CD_WrWord", "vl53l4cd__platform_8c.html#a509eea4827b4438c8c013f3feb620c72", null ],
    [ "WaitMs", "vl53l4cd__platform_8c.html#a007775a4f6a9bebeb536d9b59d72ef12", null ]
];