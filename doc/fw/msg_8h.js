var msg_8h =
[
    [ "MSG_BYTE_DATA", "msg_8h.html#a16190ba15879bacfe158f425f885f047", null ],
    [ "MSG_BYTE_END", "msg_8h.html#a200aa4adca013c607f2b8400410d9d84", null ],
    [ "MSG_BYTE_HEAD", "msg_8h.html#a1c1a499d78e467d4442e24472d0474ec", null ],
    [ "MSG_MAX_LEN", "msg_8h.html#a2ad9de7ccad1dc3102460a8090f17ed0", null ],
    [ "MSG_RESPONSE_ACK", "msg_8h.html#ac6d4c0bcf0434d4ba224b1b1656806fd", null ],
    [ "MSG_RESPONSE_ERR", "msg_8h.html#aa0a061e08028b7f425af2c9d1004b5b2", null ],
    [ "MessageState_t", "msg_8h.html#a188e8b9e482737663b047932cf1dcb43", [
      [ "MSG_EXPECT_HEADER", "msg_8h.html#a188e8b9e482737663b047932cf1dcb43a6a64f84df3e7b5d3b590363c98166b56", null ],
      [ "MSG_EXPECT_DATA", "msg_8h.html#a188e8b9e482737663b047932cf1dcb43ab0309afe475c84958af09f86a47b70c4", null ],
      [ "MSG_RECEIVED_DATA", "msg_8h.html#a188e8b9e482737663b047932cf1dcb43afe17de777680c42364a16e0ee3de231e", null ]
    ] ],
    [ "msg_get_opcode", "msg_8h.html#adf1be2d6da713b553baa905a2c176b4e", null ],
    [ "msg_get_state", "msg_8h.html#a4022e697eaf32430acda6aedd59298c3", null ],
    [ "msg_read", "msg_8h.html#a25ad1ac086f8d7bd6c6cd73c2b655ecf", null ],
    [ "msg_start", "msg_8h.html#a701ffeff793261a605a4090e7c085a2d", null ],
    [ "msg_write_boot", "msg_8h.html#a67ddd1183ced59286b4321d8d1575514", null ]
];