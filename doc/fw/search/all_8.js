var searchData=
[
  ['last_5fscene_0',['last_scene',['../struct_eval_result__t.html#a736abe965775cee023ff9dd7831c77d6',1,'EvalResult_t']]],
  ['ledb_5fclear_1',['LEDB_CLEAR',['../platform_8h.html#a277aaf01eb5af45f10a024402821b458',1,'platform.h']]],
  ['ledb_5fset_2',['LEDB_SET',['../platform_8h.html#a3ea8e7c4e9ea21f7a687bcdf6f399796',1,'platform.h']]],
  ['ledb_5ftoggle_3',['LEDB_TOGGLE',['../platform_8h.html#a96d06118d9e57a4e8de462e2f8f48974',1,'platform.h']]],
  ['ledg_5fclear_4',['LEDG_CLEAR',['../platform_8h.html#a365fdaeeafdea210d854d48ae530e446',1,'platform.h']]],
  ['ledg_5fset_5',['LEDG_SET',['../platform_8h.html#af55146c2e7f3f4b83da18513e387094c',1,'platform.h']]],
  ['ledg_5ftoggle_6',['LEDG_TOGGLE',['../platform_8h.html#a327a9731a1c2ebfb3dcc44ba23cc2d03',1,'platform.h']]],
  ['lx_7',['lx',['../struct_sensor__t.html#a3c2ef78b22c76d5e91ec08f32b93ed5b',1,'Sensor_t']]]
];
