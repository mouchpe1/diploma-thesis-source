var searchData=
[
  ['sensor_5fret_5ferr_0',['SENSOR_RET_ERR',['../sensor_8h.html#aaf6077aa54a0d949c03c9be2f08886daaa0dd3486db378d96889b9a0e9902aa76',1,'sensor.h']]],
  ['sensor_5fret_5fok_1',['SENSOR_RET_OK',['../sensor_8h.html#aaf6077aa54a0d949c03c9be2f08886daada2f4ef460fcff44085d81ade2120056',1,'sensor.h']]],
  ['sensor_5fstate_5fdisconnected_2',['SENSOR_STATE_DISCONNECTED',['../sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09aa96aa505c5ab8bb7f598c56da275581f',1,'sensor.h']]],
  ['sensor_5fstate_5ferror_3',['SENSOR_STATE_ERROR',['../sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09a926a047d10a2309d2e7ea68e318a2d9a',1,'sensor.h']]],
  ['sensor_5fstate_5fidle_4',['SENSOR_STATE_IDLE',['../sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09afcebd4e5455b500926aa210b31a5cd84',1,'sensor.h']]],
  ['sensor_5fstate_5franging_5',['SENSOR_STATE_RANGING',['../sensor_8h.html#ad49bbbfff6ef00946797ac1e6a5d5c09a5ca4fec37d244f678c68d48fd676919e',1,'sensor.h']]]
];
