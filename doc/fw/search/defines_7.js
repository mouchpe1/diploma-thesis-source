var searchData=
[
  ['sensor_5fhist_5fambient_0',['SENSOR_HIST_AMBIENT',['../sensor_8h.html#ac2122e1eb8e90d182236bf63f19546e8',1,'sensor.h']]],
  ['sensor_5fhist_5fsize_1',['SENSOR_HIST_SIZE',['../sensor_8h.html#ab21b1eced3cf6ea89fc96be89c9d24e3',1,'sensor.h']]],
  ['sensor_5fi2c_5faddress_2',['SENSOR_I2C_ADDRESS',['../sensor_8h.html#a2e976105f0b127c43dea3134d3ebdc12',1,'sensor.h']]],
  ['sensor_5fmeas_5fcnt_3',['SENSOR_MEAS_CNT',['../platform_8h.html#aacb59453aad10adac309ef23893923e5',1,'platform.h']]],
  ['sensor_5ftotal_5fcnt_4',['SENSOR_TOTAL_CNT',['../platform_8h.html#a6f4e2ac62b05f185111b9e9abfc3f89e',1,'platform.h']]],
  ['sensor_5fuse_5flxdriver_5',['SENSOR_USE_LXDRIVER',['../sensor_8h.html#a556b18f968f5a5baa6b227998e3ff75d',1,'sensor.h']]],
  ['settings_5fid_6',['SETTINGS_ID',['../platform_8h.html#ae2da0afbbaeb81a07fa2b26ed3e981f9',1,'platform.h']]]
];
