var searchData=
[
  ['addr_0',['addr',['../struct_sensor__t.html#af5105c72a61e60d3b44670de20e0c3fb',1,'Sensor_t']]],
  ['ambient_5fper_5fspad_5fkcps_1',['ambient_per_spad_kcps',['../struct_v_l53_l4_c_d___results_data__t.html#a9498c581619854f81322310e4f67936b',1,'VL53L4CD_ResultsData_t']]],
  ['ambient_5frate_5fkcps_2',['ambient_rate_kcps',['../struct_sensor_result__t.html#acf1aaccdc3acc53b684d3a59820b1a58',1,'SensorResult_t::ambient_rate_kcps()'],['../struct_v_l53_l4_c_d___results_data__t.html#a8eee8daf28772b64f79f850b7537613e',1,'VL53L4CD_ResultsData_t::ambient_rate_kcps()']]],
  ['argb_5forder_3',['argb_order',['../argb__platform_8c.html#a97b33191290780b5c1d794bc91cf2d0e',1,'argb_platform.c']]],
  ['assigned_4',['assigned',['../struct_board__t.html#a3840318e503baa04bcfd33462f3d8273',1,'Board_t']]]
];
