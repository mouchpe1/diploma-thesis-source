var searchData=
[
  ['scene_0',['scene',['../struct_sensor_result__t.html#aec26144f5799f6424d3274eb95b271e7',1,'SensorResult_t']]],
  ['sensor_5fautoupdate_1',['sensor_autoupdate',['../struct_setting__t.html#a8ecb3b6a5083cfd30fa4eb069fcf605a',1,'Setting_t']]],
  ['sensor_5flx_5fdata_5fready_2',['sensor_lx_data_ready',['../sensor__lx_8c.html#ad09abc4f2db4039f406906eb9e8b055f',1,'sensor_lx.c']]],
  ['sensors_3',['sensors',['../struct_board__t.html#a088d54f0883eb36a73ed42b54fad690b',1,'Board_t']]],
  ['sensors_5fmeas_4',['sensors_meas',['../struct_board__t.html#a9e5bf8b42aa4fa15054c0324151c6ab4',1,'Board_t']]],
  ['settings_5',['settings',['../app_8c.html#ad607f665a5f4cf845406522d89b5effd',1,'settings():&#160;app.c'],['../cmd_8h.html#ad607f665a5f4cf845406522d89b5effd',1,'settings():&#160;app.c'],['../meas_8h.html#ad607f665a5f4cf845406522d89b5effd',1,'settings():&#160;app.c']]],
  ['sigma_5fmm_6',['sigma_mm',['../struct_sensor_result__t.html#aeb09813977cb12b0b7f0039f111db3bf',1,'SensorResult_t::sigma_mm()'],['../struct_v_l53_l4_c_d___results_data__t.html#a84836df409b965d315060fa80d1abb5c',1,'VL53L4CD_ResultsData_t::sigma_mm()']]],
  ['signal_5fper_5fspad_5fkcps_7',['signal_per_spad_kcps',['../struct_v_l53_l4_c_d___results_data__t.html#a4536ad17588b66adc9cfaa95df4efd77',1,'VL53L4CD_ResultsData_t']]],
  ['signal_5frate_5fkcps_8',['signal_rate_kcps',['../struct_sensor_result__t.html#a9efea8d79af868b90551de5bfadb189b',1,'SensorResult_t::signal_rate_kcps()'],['../struct_v_l53_l4_c_d___results_data__t.html#aaf2805dfb6224bc09a13d2ef1e8d5fb7',1,'VL53L4CD_ResultsData_t::signal_rate_kcps()']]],
  ['state_9',['state',['../struct_sensor__t.html#a12a5481f6a024c944a702c69a4efa00b',1,'Sensor_t']]],
  ['sum_10',['sum',['../struct_scene_data__t.html#abc644de8cf67ff88da87e9cc16310188',1,'SceneData_t']]]
];
