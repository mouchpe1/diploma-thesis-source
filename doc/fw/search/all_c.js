var searchData=
[
  ['params_0',['params',['../struct_sensor__t.html#affea76778ed25a5678caa2dae865ec67',1,'Sensor_t']]],
  ['period_5fms_1',['period_ms',['../struct_sensor_ranging_params__t.html#a1b97d8976db265530afd6538b08969cd',1,'SensorRangingParams_t']]],
  ['platform_2ec_2',['platform.c',['../platform_8c.html',1,'']]],
  ['platform_2eh_3',['platform.h',['../platform_8h.html',1,'']]],
  ['platform_5finit_4',['platform_init',['../platform_8h.html#a760229807662fde444020422a81e937c',1,'platform_init(void):&#160;platform.c'],['../platform_8c.html#a390c450e83ddc7807da2e9f0a894d8d1',1,'platform_init():&#160;platform.c']]],
  ['print_5ferr_5',['print_err',['../platform_8h.html#a32daed9741f86df9a728d94065d69ca7',1,'print_err(const char *file, uint8_t line):&#160;platform.c'],['../platform_8c.html#a32daed9741f86df9a728d94065d69ca7',1,'print_err(const char *file, uint8_t line):&#160;platform.c']]]
];
