var searchData=
[
  ['uart_5fdrv_0',['UART_DRV',['../platform_8h.html#a37dce7389f36fd7f0dbfa3dc065954d6',1,'platform.h']]],
  ['uart_5firq_5fcalls_1',['uart_irq_calls',['../app_8c.html#a7e27b50bc07a39196ce62138f8c4015a',1,'app.c']]],
  ['uart_5fread_2',['uart_read',['../platform_8h.html#a41cbd9ccf29915c6b88d77828c3cf02c',1,'uart_read(uint8_t *buffer, uint16_t len):&#160;platform.c'],['../platform_8c.html#a41cbd9ccf29915c6b88d77828c3cf02c',1,'uart_read(uint8_t *buffer, uint16_t len):&#160;platform.c']]],
  ['uart_5frx_5fhandler_3',['uart_rx_handler',['../app_8c.html#a2a1623a52623c1d904b94c4c9ddd354a',1,'uart_rx_handler(SerialDriver *sdp):&#160;app.c'],['../platform_8h.html#a2a1623a52623c1d904b94c4c9ddd354a',1,'uart_rx_handler(SerialDriver *sdp):&#160;app.c']]],
  ['uart_5fwrite_4',['uart_write',['../platform_8h.html#acb183ff24d0e0fbc97be82b8bae4095f',1,'uart_write(uint8_t *buffer, uint16_t len):&#160;platform.c'],['../platform_8c.html#acb183ff24d0e0fbc97be82b8bae4095f',1,'uart_write(uint8_t *buffer, uint16_t len):&#160;platform.c']]]
];
