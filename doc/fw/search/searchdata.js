var indexSectionsWithContent =
{
  0: "_abcdehilmnoprstuvwx",
  1: "bemsv",
  2: "acmpsv",
  3: "_abcdmpstuvw",
  4: "abcdehilmnoprsuwx",
  5: "cdv",
  6: "cms",
  7: "cms",
  8: "abdilmrstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

