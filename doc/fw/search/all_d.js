var searchData=
[
  ['range_5fstatus_0',['range_status',['../struct_v_l53_l4_c_d___results_data__t.html#ad4afdf20896b44c79318589612dd9aa5',1,'VL53L4CD_ResultsData_t']]],
  ['ranging_5factive_1',['RANGING_ACTIVE',['../app_8c.html#a8712f42d6543c7db1ef2ea73e513d190',1,'app.c']]],
  ['ranging_5fidle_2',['RANGING_IDLE',['../app_8c.html#a22b93c4f491e7d91ffa31c6d77d2256f',1,'app.c']]],
  ['ranging_5fms_3',['ranging_ms',['../struct_sensor_ranging_params__t.html#a4fc24c10cd99c2b2ab5434483c90a0a8',1,'SensorRangingParams_t']]],
  ['raw_4',['raw',['../struct_measurement__t.html#a118187bc9998f12b8bdfc4179648fd55',1,'Measurement_t']]],
  ['remote_5feval_5',['remote_eval',['../struct_setting__t.html#a88f17fe08ef643f248e36813ac37699e',1,'Setting_t']]],
  ['revision_6',['revision',['../struct_v_l53_l4_c_d___version__t.html#a7593f6425fdea53d846cc5e89f3727db',1,'VL53L4CD_Version_t']]],
  ['rgb_5fdrv_7',['RGB_DRV',['../platform_8h.html#aa605afa836d5f403b26ef3e1779677a2',1,'platform.h']]]
];
