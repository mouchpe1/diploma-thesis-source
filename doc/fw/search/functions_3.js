var searchData=
[
  ['cmd_5ffetch_0',['cmd_fetch',['../cmd_8c.html#ae63f827228a8d43ba6b27f90b0f07664',1,'cmd_fetch(uint8_t id):&#160;cmd.c'],['../cmd_8h.html#ae63f827228a8d43ba6b27f90b0f07664',1,'cmd_fetch(uint8_t id):&#160;cmd.c']]],
  ['cmd_5fread_5fhist_1',['cmd_read_hist',['../cmd_8c.html#a37db621db1512facd889dd6d38be6947',1,'cmd_read_hist(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c'],['../cmd_8h.html#a37db621db1512facd889dd6d38be6947',1,'cmd_read_hist(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c']]],
  ['cmd_5fread_5fsensor_2',['cmd_read_sensor',['../cmd_8c.html#a65c964863e519b1daec490b0d3cceb0d',1,'cmd_read_sensor(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c'],['../cmd_8h.html#a65c964863e519b1daec490b0d3cceb0d',1,'cmd_read_sensor(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c']]],
  ['cmd_5fset_5fdist_3',['cmd_set_dist',['../cmd_8c.html#ab5eb8c1a94ef2b45b7cbbea310527ee7',1,'cmd_set_dist(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c'],['../cmd_8h.html#ab5eb8c1a94ef2b45b7cbbea310527ee7',1,'cmd_set_dist(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c']]],
  ['cmd_5fset_5feval_4',['cmd_set_eval',['../cmd_8c.html#a8c431dd03534c7621664cafa2a268951',1,'cmd_set_eval(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c'],['../cmd_8h.html#a8c431dd03534c7621664cafa2a268951',1,'cmd_set_eval(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c']]],
  ['cmd_5fset_5fevalmode_5',['cmd_set_evalmode',['../cmd_8c.html#aab15985101f51782c12c078e40d1950f',1,'cmd_set_evalmode(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c'],['../cmd_8h.html#aab15985101f51782c12c078e40d1950f',1,'cmd_set_evalmode(uint8_t *in, const uint8_t in_len, uint8_t *out):&#160;cmd.c']]]
];
