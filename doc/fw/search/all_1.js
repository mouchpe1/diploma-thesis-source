var searchData=
[
  ['addr_0',['addr',['../struct_sensor__t.html#af5105c72a61e60d3b44670de20e0c3fb',1,'Sensor_t']]],
  ['ambient_5fper_5fspad_5fkcps_1',['ambient_per_spad_kcps',['../struct_v_l53_l4_c_d___results_data__t.html#a9498c581619854f81322310e4f67936b',1,'VL53L4CD_ResultsData_t']]],
  ['ambient_5frate_5fkcps_2',['ambient_rate_kcps',['../struct_sensor_result__t.html#acf1aaccdc3acc53b684d3a59820b1a58',1,'SensorResult_t::ambient_rate_kcps()'],['../struct_v_l53_l4_c_d___results_data__t.html#a8eee8daf28772b64f79f850b7537613e',1,'VL53L4CD_ResultsData_t::ambient_rate_kcps()']]],
  ['app_2ec_3',['app.c',['../app_8c.html',1,'']]],
  ['app_2eh_4',['app.h',['../app_8h.html',1,'']]],
  ['app_5frun_5',['app_run',['../app_8c.html#a653e34abe7953d99fe3c105e285d5a7b',1,'app_run():&#160;app.c'],['../app_8h.html#af5b220761df34e786386b8f5cb8b59f9',1,'app_run(void):&#160;app.c']]],
  ['app_5fsensor_5fassign_6',['app_sensor_assign',['../app_8h.html#a9b05c3c5891d9f25265ba90f03507f00',1,'app_sensor_assign(void):&#160;app.c'],['../app_8c.html#a9b05c3c5891d9f25265ba90f03507f00',1,'app_sensor_assign(void):&#160;app.c']]],
  ['app_5fsensor_5fnext_7',['app_sensor_next',['../app_8c.html#a89e43e2bb69a14f06d2d862f774ade69',1,'app.c']]],
  ['app_5fsensor_5fread_8',['app_sensor_read',['../app_8h.html#a63951ba645a71d23e27562796fc89ac5',1,'app.h']]],
  ['app_5fsettings_5finit_9',['app_settings_init',['../app_8c.html#a83eee4cd452a86619e94d2fc6de79d4d',1,'app_settings_init(void):&#160;app.c'],['../app_8h.html#a83eee4cd452a86619e94d2fc6de79d4d',1,'app_settings_init(void):&#160;app.c']]],
  ['app_5fsettings_5fload_10',['app_settings_load',['../app_8h.html#a513169eabc00d446363642dd56786ab6',1,'app_settings_load(void):&#160;app.c'],['../app_8c.html#a513169eabc00d446363642dd56786ab6',1,'app_settings_load(void):&#160;app.c']]],
  ['app_5ftypes_2eh_11',['app_types.h',['../app__types_8h.html',1,'']]],
  ['argb_2ec_12',['argb.c',['../argb_8c.html',1,'']]],
  ['argb_2eh_13',['argb.h',['../argb_8h.html',1,'']]],
  ['argb_5fclear_14',['argb_clear',['../argb_8c.html#a1bc5f6679a09c256827bb8e66d8fa5b0',1,'argb_clear(int num):&#160;argb.c'],['../argb_8h.html#a1bc5f6679a09c256827bb8e66d8fa5b0',1,'argb_clear(int num):&#160;argb.c']]],
  ['argb_5fframe_5fsize_15',['ARGB_FRAME_SIZE',['../argb__platform_8c.html#acb2c421c451af7ed5f178214c276e3c8',1,'argb_platform.c']]],
  ['argb_5fhigh_16',['ARGB_HIGH',['../argb__platform_8c.html#ae598df045dc81e1d03e6c4c7c955debb',1,'argb_platform.c']]],
  ['argb_5finverted_17',['ARGB_INVERTED',['../argb__platform_8c.html#a6ea8f56b485186b22a5ea2455261cdfa',1,'argb_platform.c']]],
  ['argb_5fled_5fsize_18',['ARGB_LED_SIZE',['../argb__platform_8c.html#ab4fd889895e128008ac780208ff496be',1,'argb_platform.c']]],
  ['argb_5flow_19',['ARGB_LOW',['../argb__platform_8c.html#a39605ba3a1db0396212fb60df007279d',1,'argb_platform.c']]],
  ['argb_5fnum_5fleds_20',['ARGB_NUM_LEDS',['../argb__platform_8c.html#a2d892c8b4e74a5d1daa75cee34e31d1b',1,'argb_platform.c']]],
  ['argb_5forder_21',['argb_order',['../argb__platform_8c.html#a97b33191290780b5c1d794bc91cf2d0e',1,'argb_platform.c']]],
  ['argb_5fplatform_2ec_22',['argb_platform.c',['../argb__platform_8c.html',1,'']]],
  ['argb_5freset_5fsize_23',['ARGB_RESET_SIZE',['../argb__platform_8c.html#a42cd0ec38a0ebb23e354998ad6518c37',1,'argb_platform.c']]],
  ['argb_5fset_24',['argb_set',['../argb_8c.html#a6578e867859e1a85ff67c91aa01054b7',1,'argb_set(int num, Color_t color):&#160;argb.c'],['../argb_8h.html#a6578e867859e1a85ff67c91aa01054b7',1,'argb_set(int num, Color_t color):&#160;argb.c']]],
  ['argb_5ftoggle_25',['argb_toggle',['../argb_8c.html#a11bde4bf6f682bf7eff0173cb4217ae0',1,'argb_toggle(int num, Color_t color):&#160;argb.c'],['../argb_8h.html#a11bde4bf6f682bf7eff0173cb4217ae0',1,'argb_toggle(int num, Color_t color):&#160;argb.c']]],
  ['argb_5ftoggle2_26',['argb_toggle2',['../argb_8c.html#aa54844953168c3f5ebdee8289630d925',1,'argb_toggle2(int num, Color_t color_a, Color_t color_b):&#160;argb.c'],['../argb_8h.html#aa54844953168c3f5ebdee8289630d925',1,'argb_toggle2(int num, Color_t color_a, Color_t color_b):&#160;argb.c']]],
  ['argb_5fuse_5fcolorbuffer_27',['ARGB_USE_COLORBUFFER',['../argb__platform_8c.html#ae1da772c8626eae46aa9edff0d2c0839',1,'argb_platform.c']]],
  ['assert_28',['assert',['../platform_8h.html#a620dbe10e64d50924945fa13bf19b6b5',1,'platform.h']]],
  ['assigned_29',['assigned',['../struct_board__t.html#a3840318e503baa04bcfd33462f3d8273',1,'Board_t']]]
];
