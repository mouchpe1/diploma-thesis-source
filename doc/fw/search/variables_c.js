var searchData=
[
  ['range_5fstatus_0',['range_status',['../struct_v_l53_l4_c_d___results_data__t.html#ad4afdf20896b44c79318589612dd9aa5',1,'VL53L4CD_ResultsData_t']]],
  ['ranging_5fms_1',['ranging_ms',['../struct_sensor_ranging_params__t.html#a4fc24c10cd99c2b2ab5434483c90a0a8',1,'SensorRangingParams_t']]],
  ['raw_2',['raw',['../struct_measurement__t.html#a118187bc9998f12b8bdfc4179648fd55',1,'Measurement_t']]],
  ['remote_5feval_3',['remote_eval',['../struct_setting__t.html#a88f17fe08ef643f248e36813ac37699e',1,'Setting_t']]],
  ['revision_4',['revision',['../struct_v_l53_l4_c_d___version__t.html#a7593f6425fdea53d846cc5e89f3727db',1,'VL53L4CD_Version_t']]]
];
