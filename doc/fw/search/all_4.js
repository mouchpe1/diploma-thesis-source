var searchData=
[
  ['delay_5fms_0',['delay_ms',['../platform_8h.html#a389926e6490f1d3db0f0896e8b0f5f2e',1,'platform.h']]],
  ['delay_5fus_1',['delay_us',['../platform_8h.html#af4f937c251678e98e4c41b4d58f232a5',1,'platform.h']]],
  ['dev_2',['dev',['../struct_sensor__t.html#a6c3b2d92814052906aa7ab7207932719',1,'Sensor_t']]],
  ['dev_5ft_3',['Dev_t',['../vl53l4cd__platform_8h.html#a4720fc2317be91590dfc25d169dc1c64',1,'vl53l4cd_platform.h']]],
  ['distance_5fmm_4',['distance_mm',['../struct_sensor_result__t.html#a9aa6b73a05ba9e263b9ff4716023178c',1,'SensorResult_t::distance_mm()'],['../struct_v_l53_l4_c_d___results_data__t.html#a780fee760b6a8c05845a55634fd1d7d0',1,'VL53L4CD_ResultsData_t::distance_mm()']]],
  ['dtof0_5firq_5fhandler_5',['dtof0_irq_handler',['../app_8c.html#a5e96e665f91582bc384f84eaa570a6a1',1,'app.c']]],
  ['dtof1_5firq_5fhandler_6',['dtof1_irq_handler',['../app_8c.html#aa54dd1a9605f1719220760e369aa6196',1,'app.c']]],
  ['dtof2_5firq_5fhandler_7',['dtof2_irq_handler',['../app_8c.html#acef26251a7cd17b3d4ad5e514b7e2643',1,'app.c']]],
  ['dtof3_5firq_5fhandler_8',['dtof3_irq_handler',['../app_8c.html#ae373d1bc46816452130924ad13c42a3a',1,'app.c']]],
  ['dtof_5firq_5fid_9',['dtof_irq_id',['../app_8c.html#a563e9396f4d4bc7d436407ac4cffdeb1',1,'app.c']]]
];
