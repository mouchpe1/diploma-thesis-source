var cmd_8h =
[
    [ "CommandFunc_t", "cmd_8h.html#a827ce2e2f3a4fc114b5f382baeffe724", null ],
    [ "cmd_fetch", "cmd_8h.html#ae63f827228a8d43ba6b27f90b0f07664", null ],
    [ "cmd_read_hist", "cmd_8h.html#a37db621db1512facd889dd6d38be6947", null ],
    [ "cmd_read_sensor", "cmd_8h.html#a65c964863e519b1daec490b0d3cceb0d", null ],
    [ "cmd_set_dist", "cmd_8h.html#ab5eb8c1a94ef2b45b7cbbea310527ee7", null ],
    [ "cmd_set_eval", "cmd_8h.html#a8c431dd03534c7621664cafa2a268951", null ],
    [ "cmd_set_evalmode", "cmd_8h.html#aab15985101f51782c12c078e40d1950f", null ],
    [ "board", "cmd_8h.html#aadd9694310193cbbfb23471e00300f05", null ],
    [ "meas", "cmd_8h.html#ac01c37d03d12e34b394dfbd9e3bc432c", null ],
    [ "settings", "cmd_8h.html#ad607f665a5f4cf845406522d89b5effd", null ]
];