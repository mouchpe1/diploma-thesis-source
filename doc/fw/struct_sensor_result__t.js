var struct_sensor_result__t =
[
    [ "ambient_rate_kcps", "struct_sensor_result__t.html#acf1aaccdc3acc53b684d3a59820b1a58", null ],
    [ "distance_mm", "struct_sensor_result__t.html#a9aa6b73a05ba9e263b9ff4716023178c", null ],
    [ "number_of_spad", "struct_sensor_result__t.html#a3946821c5ed8cdaacada44694af9c089", null ],
    [ "scene", "struct_sensor_result__t.html#aec26144f5799f6424d3274eb95b271e7", null ],
    [ "sigma_mm", "struct_sensor_result__t.html#aeb09813977cb12b0b7f0039f111db3bf", null ],
    [ "signal_rate_kcps", "struct_sensor_result__t.html#a9efea8d79af868b90551de5bfadb189b", null ]
];